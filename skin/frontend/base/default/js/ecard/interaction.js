jQuery.noConflict();

jQuery( document ).ready(function( $ ) {

	jQuery(".ecard-index-displaycard .open-text").click(function(event) {

			jQuery(".ecard-index-displaycard .container").addClass('active');

	})


	var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

    //Function to convert hex format to a rgb color
    function rgb2hex(rgb) {
     rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
     return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

    function hex(x) {
      return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }

    var cardSent = false;

    jQuery.ajaxSetup({
        beforeSend:function(){
            jQuery.loader();
        },
        complete:function(){
            jQuery.loader('close');
        }
    });
    if(!document.URL.match(/displaycard/g) && !document.URL.match(/downloadgift/g)) {
        $(window).on('beforeunload', function(){
            if (!cardSent && loggedIn == 1) return 'Do you want to leave this page and lose your changes?';
        });
    }
    $(document).ready(function(){
        var prefix = "";

        var step = 0;
        var contentForm = "";
        var contentData = "";
        var defaultFontSize = '18px';
        var buttonChecked = '';

        if (typeof window.fromNameField !== 'undefined') {
            var fromNameField = window.fromNameField;
        }
        if (typeof window.fromEmailField !== 'undefined') {
            var fromEmailField = window.fromEmailField;
        }
        if (typeof window.toNameField !== 'undefined') {
            var toNameField = window.toNameField;
        }
        if (typeof window.toEmailField !== 'undefined') {
            var toEmailField = window.toEmailField;
        }

        if (typeof window.cardSize !== 'undefined') {
            var cardSize = window.cardSize;
        } else {
            var cardSize = 2;
        }

        if (typeof window.cardNumber !== 'undefined') {
            var cardNumber = window.cardNumber;
        } else {
            var cardNumber = 1;
        }

        if (typeof window.isOpeningCard !== 'undefined') {
            var isOpeningCard = window.isOpeningCard;
        } else {
            var isOpeningCard = false;
        }

        if (typeof window.fontFamily !== 'undefined') {
            var fontFamily = window.fontFamily;
        } else {
             var fontFamily = "Somebody";
        }

        if (typeof window.fontSize !== 'undefined') {
            var fontSize = window.fontSize;
        } else {
             var fontSize = defaultFontSize;
        }

        if (typeof window.fontWeight !== 'undefined') {
            var fontWeight = window.fontWeight;
        } else {
            var fontWeight = 'bold';
        }

        if (typeof window.fontStyle !== 'undefined') {
            var fontStyle = window.fontStyle;
        } else {
            var fontStyle = 'normal';
        }

        if (typeof window.fontColor !== 'undefined') {
            var fontColor = window.fontColor;
        } else {
            var fontColor = 'rgb(0,0,0)';
        }

        if (typeof window.textAlign !== 'undefined') {
            var textAlign = window.textAlign;
        } else {
            var textAlign = 'left';
        }

        if (typeof window.cardBooks !== 'undefined') {
            var cardBooks = window.cardBooks;
        } else {
            var cardBooks = new Array();
        }
        if (typeof window.itemId !== 'undefined') {
            var magentoItemId = window.itemId;
        } else {
            var cardBooks = new Array();
        }

        if (typeof window.cardText !== 'undefined') {
            var cardText = window.cardText;
        } else {
            var cardText = 'Your message here.';
            $.get('/cardText.txt', function(content) {
                cardText = content;
            });
        }

        var bookSkus = {
            book1: ["vol-one-any", "vol-one-epub", "vol-one-mobi", "ebook-volume-1"],
            book2: ["vol-two-any", "vol-two-epub", "vol-two-mobi"],
            book3: ["vol-three-any", "vol-three-epub", "vol-three-mobi"],
            book4: ["vol-four-any", "vol-four-epub", "vol-four-mobi"],
            book5: ["vol-five-any", "vol-five-epub", "vol-five-mobi"],
            book6: ["vol-six-any", "vol-six-epub", "vol-six-mobi"],
            book7: ["vol-seven-any", "vol-seven-epub", "vol-seven-mobi" ],
            bookAll: ["all-ebooks-any", "all-ebooks-epub", "all-ebooks-mobi"]
        };
        var booksAdded = {
            book1: 0,
            book2: 0,
            book3: 0,
            book4: 0,
            book5: 0,
            book6: 0,
            book7: 0,
            bookAll: 0
        };

        var books = {
                book1: 0, //if book1 = 0, then we should add a book
                book2: 0,
                book3: 0,
                book4: 0,
                book5: 0,
                book6: 0,
                book7: 0,
                bookAll: 0
            };

        var fontFamilyButton = fontFamily;
        var fontSizeButton = fontSize;
        var fontColorButton = rgb2hex(fontColor);

        if(fontWeight == "bold") {
            var fontWeightButton = true;
        } else {
            var fontWeightButton = false;
        }

        if(fontStyle == "italic") {
            var fontStyleButton = true;
        } else {
            var fontStyleButton = false;
        }

        if(textAlign == "left") {
            var textAlignLeftButton = true;
        } else {
            var textAlignLeftButton = false;
        }

        if(textAlign == "center") {
            var textAlignCenterButton = true;
        } else {
            var textAlignCenterButton = false;
        }

        if(textAlign == "right") {
            var textAlignRightButton = true;
        } else {
            var textAlignRightButton = false;
        }

        if(textAlign == "justify") {
            var textAlignJustifyButton = true;
        } else {
            var textAlignJustifyButton = false;
        }

            // Back button
        $('#cardAppDiv').on('click', '#backButton', function() {
            //do stuff
            displayStep(step-1);
        })

            // Next button
        $('#cardAppDiv').on('click', '#nextButton', function() {
            displayStep(step+1);
        });

            // Text button
        $('#cardAppDiv').on('click', '#textback_text', function() {
            displayStep(2);
        });

            // Back button
        $('#cardAppDiv').on('click', '#textback_back', function() {
            displayStep(3);
        });

            // Go from Step 0 to Step 1
        $('#cardAppDiv').on('click', '.cardImage', function() {
            var previewPath = $(this).attr("src");
            cardNumber = previewPath.slice(previewPath.indexOf("card_")+5,previewPath.indexOf("."));
            cardSize = previewPath.slice(previewPath.indexOf("size_")+5,previewPath.indexOf("_card"));
            if(loggedIn == 0) {
                window.location.replace(prefix + '/default/ecard/index/login');
                throw "stop execution";
            }
//            if(typeof ebookCounter != 'undefined' && ebookCounter != 0) {
//                displayStep(1);
//            } else {
//                $('<div><p>Ecards are for e-books purchases only, please select an ebook!</p><p><a href="ebooks.html">Goto the ebooks page now!</a></p></div>').dialog();
//            }
            displayStep(1);
        });

            // Go from Step 1 to Step 2
        $('#cardAppDiv').on('click', '#envelopeAndCard', function() {
            if (step == 1) displayStep(2);
        });

        if(typeof window.editCard !== 'undefined' && window.editCard == true) {
            var editCard = true;
            displayStep(1);
        } else {
            var editCard = false;
        }

        function displayStep(goToStep){
            switch(goToStep) {

                // -------------------- STEP 0 --------------------
                case 0:
                    step = 0;
                    window.location.reload();
                    break;

                // -------------------- STEP 1 --------------------
                case 1:
                    cardSent = false;
                    if (step >= 1) {
                        $("#textTools").css("display","none");
                        $("#fontsDiv").css("display","none");
                        $(".openingChoice").css("display","none");
                        $("#envelopeAndCard").css("display","block");
                        $("#cardText").attr("readonly","readonly");
                        $("#cardText").css("cursor","pointer");
                        $("#backButton").css('display','none');
                        $("#returnButtonLink").css('display','inline');
                        step = 1;
                        break;
                    }

                    $("#cardAppDiv").empty();
                    $("#cardAppDiv").addClass('cardAppDivWithStyle');

                                    var step1Text = "";
                                    step1Text += "<div id='designDiv'>";
                                    step1Text += "<img id='topbar' src='/images/design/top_design.png'>";
                                    step1Text += "<img id='textback_text' src='/images/design/second_top_textback_text.png'>";
                                    step1Text += "<img id='textback_back' src='/images/design/second_top_textback_back.png' style='opacity:0.5'>";
                                    step1Text += "<div id='ecard-bar'>";
                                    step1Text += "<a href='javascript:void(0);' id='nextButton'>NEXT</a>";
                                    step1Text += "<a href='javascript:void(0);' style='display:none;' id='backButton'>BACK</a>";
                                    step1Text += "<a id='returnButtonLink' href='" + prefix + "/default/ecard.html'>RETURN TO CARD SELECTION</a>";
                                    step1Text += "</div>";
                                    step1Text += "</div>";

                                    step1Text += "<div id='contentDiv'>";
                                    //step1Text += textEditor();
                                    step1Text += "</div>";

                                    $("#cardAppDiv").append(step1Text);
									$("#contentDiv").css('backgroundImage','url(/images/cards_size_'+cardSize+'/card_'+cardNumber+'/background.jpg)');

                                    //checkIfOpenCard();
                                    //textEditorInteractions();

                    var dir = "/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_open_left.png";
                                    $.ajax({
                        url: dir,
                                            type:'HEAD',
                                            error: function(){
                                                    isOpeningCard = false;
                                                    $('#contentDiv').append(textEditor());
                                                    textEditorInteractions();
                                            //autoResizeText($('#cardText'));
                                                    //alert("no!");
                                            },
                        success: function (data) {
                            isOpeningCard = true;
                                                    $('#contentDiv').append(textEditor());
                                                    textEditorInteractions();
                                            //autoResizeText($('#cardText'));
                        }
                                    });

                    step = 1;

                    break;

                // -------------------- STEP 2 --------------------
                case 2:
                    if (step >= 2) {
                        $("#textback_text").css("opacity","1");
                        $("#textback_back").css("opacity","0.5");

                        $("#contentDiv").empty();
                        $("#contentDiv").append(textEditor());
                        textEditorInteractions();
                        //checkIfOpenCard();
                        loadTextEdition();
                    }
                    $('.openingChoice').css('display', 'block');
                    $('#envelopeAndCard').css('display', 'none');
                    $("#textTools").css("display","block");

                    $("#fontsDiv").css("display","block");
                    $("#cardText").removeAttr("readonly");
                    $("#cardText").css("cursor","auto");
                    $("#cardTextDiv").css("cursor","default");
                    $("#backButton").css('display','inline');
                    $("#returnButtonLink").css('display','none');
                    step = 2;

                    break;

                // -------------------- STEP 3 --------------------
                case 3:
                    if (step <= 3) {
                        $("#textback_text").css("opacity","0.5");
                        $("#textback_back").css("opacity","1");
                        saveTextEdition();
                    }
                    else {
                        $("#topbar").attr("src","/images/design/top_design.png");
                        $("#textback_text").css("visibility","visible");
                        $("#textback_back").css("visibility","visible");
                        $("#textback_text").show(500);
                        $("#textback_back").show(500);
                    }

                    $("#contentDiv").empty();
                    $("#contentDiv").append(booksEditor());

                    booksEditorInteractions();
                    loadBooks('cardBackSize'+cardSize);
                    step = 3;

                    break;

                // -------------------- STEP 4 --------------------
                case 4:
                    $("#topbar").attr("src","/images/design/top_preview.png");
                    $("#textback_text").css("visibility","hidden");
                    $("#textback_back").css("visibility","hidden");
                    if (step >= 4) {
                        $("#nextButton").show();
                        $(".toSendDate").datepicker( "destroy" );
                        contentForm = $("#contentDiv").html();
                        contentData = $("#emailForm").serialize();
                    }

                    $("#contentDiv").empty();
                    $("#contentDiv").append(cardPreview());

                    cardPreviewInteractions();
                    step = 4;

                    break;
                // -------------------- STEP 5 --------------------
                case 5:
                    $("#topbar").attr("src","/images/design/top_delivery.png");
                    $("#nextButton").hide();

                    $("#contentDiv").empty();
                    if(contentForm.length != 0) {
                        $('#contentDiv').append(contentForm);
                        $("#emailForm").unserializeForm(contentData);
                        $(".toSendDate").datepicker({ minDate: 0 });
                        prepareStep5();
                        $('.close').unbind('click').click(function(){
                            $(this).parent().remove();
                            if($('div.people p').length == 8) {
                                $('a.addPerson').show();
                            }
                            unduplicateCart();
                            return false;
                        });

                        break;
                    } else {
                        $("#contentDiv").append(sendCardForm());
                    }
                    $(".toSendDate").datepicker({ minDate: 0 });
                    prepareStep5();
                    break;
                default:
                    break;
            }
        }

        function prepareStep5() {
                    $.ajax({
                      dataType: "json",
                      type: "GET",
                      url: prefix + "/default/ecard/index/getCartContents",
                      success: function(returnValue) {
                          $('.cartWrapper').html(buildCartTable(returnValue));
                      }
                    });

                    $(function(){
                        $('#people').hide();
                    });

                    $(".addPerson").click(function(){
                        $('#people').show();
                        //add
                        for(var count = 1; count <= 100; count++) {
                            if($('textarea[name=toNameField'+ count + ']').length == 0) {
                                $('div.people').append("<p class='person-block'><label class='person-to'>To: <textarea name='toNameField"+count+"' class='textField toNameField' placeholder='Recipient\'s name' wrap='off' rows='1' cols='24'></textarea></label><label class='person-email'> E-Mail: <textarea name='toEmailField"+count+"' class='textField toEmailField' placeholder='recipient@address.com' wrap='off' rows='1' cols='30'></textarea></label><label class='person-schedule'> Schedule Sending: <input type='text' name='toSendDate"+count+"' class='textField toSendDate' placeholder='(click)'></label><a href='#' class='close'>X</a></p>");
                                break;
                            }
                        }
                        $('.close').unbind('click').click(function(){
                            $(this).parent().remove();
                            if($('div.people p').length == 99) {
                                $('a.addPerson').show();
                            }
                            if($('div.people p').length == '') {
                                $('#people').hide();
                            }
                            unduplicateCart();
                            return false;
                        });
                        if($("div.people p").length == 1) {
                            $('a.removePerson').show();
                        }
                        if($("div.people p").length == 100) {
                            $('a.addPerson').hide();
                        }
                        $(".toSendDate").datepicker({ minDate: 0 });

                        duplicateCart();
                        return false;
                    });
//                    $(".removePerson").click(function(){
//                        $('div.people p').last().remove();
//                        if($('div.people p').length == 0) {
//                            $('a.removePerson').hide();
//                        }
//                        if($('div.people p').length == 8) {
//                            $('a.addPerson').show();
//                        }
//                        count--;
//                        unduplicateCart();
//                        return false;
//                    });

                    sendCardInteractions();
                    step = 5;
        }

        function duplicateCart() {
            var skusToAdd = [];
            var booksToAdd = [];
            getUnlinkedBookCounts();
            $.each(cardBooks, function(i, value){
                var index = value.indexOf('book_');
                var book = value.substr(index, 6).replace("_","");
                booksToAdd[booksToAdd.length] = book;
            });

            //get the skus
            $.each(booksToAdd, function(index, book) {
                //if(books[book] == 0) {
                    skusToAdd[skusToAdd.length] = bookSkus[book][0];
                    booksAdded[book] ++;
                /*} else {
                    books[book] --;
                    booksAdded[book] ++;
                }*/
            });
            $.ajax({
              dataType: "json",
              async: false,
              type: "POST",
              data: { skus: skusToAdd },
              url: prefix + "/default/ecard/index/addSkusToCart",
              success: function(returnValue) {
                  console.log(returnValue);
                  $('.cartWrapper').html(buildCartTable(returnValue));
              }
            });
        }

        function unduplicateCart() {
            getUnlinkedBookCounts();
            var skusToRemove = [];
            var booksToRemove = [];
            $.each(cardBooks, function(i, value){
                var index = value.indexOf('book_');
                var book = value.substr(index, 6).replace("_","");
                booksToRemove[booksToRemove.length] = book;
            });

            $.each(booksToRemove, function(index, book) {
                if(booksAdded[book] > 0) {
                    skusToRemove[skusToRemove.length] = bookSkus[book][0];
                    booksAdded[book] --;
                } else {
                    // do nothing because it was in the cart already
                }
            });

            $.ajax({
              dataType: "json",
              async: false,
              type: "POST",
              data: { skus: skusToRemove },
              url: prefix + "/default/ecard/index/removeSkusFromCart",
              success: function(returnValue) {
                  $('.cartWrapper').html(buildCartTable(returnValue));
              }
            });
        }

        function buildCartTable(cartObject) {
            var cartTable = "";
            cartTable += "<p class='subtotal'>Subtotal: $"+Number(cartObject.totals['subtotal']).toFixed(2)+"</p>";
            cartTable += "<div id='cart-table'><table>";
            cartTable += "<tr><th>Item</th><th>Quantity</th></tr>"

            $.each(cartObject['items'], function(value, item){
                cartTable += "<tr>";
                cartTable += "<td>- " + item['name'] + "</td>";
                cartTable += "<td>" + item['qty'] + "</td>";
                cartTable += "</tr>";
            });

            cartTable += "<tr><td></td><td></td></tr>";
            //console.log(cartObject);

            cartTable += "</table></div>";
            return cartTable;
        }

        // -------------------- STEP 1 & 2 --------------------
        function textEditor(){
            var step1Text = "";
            step1Text += "<div id='textTools' style='display:none'>";
            step1Text += "<div class='font-format'>";
			step1Text += "<div class='font-title'><h3>Format your Message</h3></div>";
            step1Text += "<div class='font-changes'>";
            step1Text += "<div id='spinnerDiv'  title='Font Size'><input id='spinner' name='value' value='"+parseInt(fontSize)+"' style='color:#fff;'></div>";
            step1Text += "<div id='colorDiv'  title='Color'><input id='color' type='text' name='color' value= "+fontColorButton+" /></div>";
            step1Text += "<div id='format'>";
            step1Text += "<input type='checkbox' id='check1' "+getButton(fontWeightButton,'button')+"><label for='check1' title='Bold'><span style='font-weight:bold;color:#000;'>B</span></label>";
            step1Text += "<input type='checkbox' id='check2' "+getButton(fontStyleButton,'button')+"><label for='check2' title='Italic'><span style='font-style:italic;color:#fff;'>i</span></label>";
            step1Text += "</div>";
            step1Text += "<div id='align'>";
            step1Text += "<input type='radio' id='radio1' name='radio' "+getButton(textAlignLeftButton,'button')+"><label for='radio1'><img src='/images/design/text_tools_align_left.png'  title='Text align left' class='align'></label>";
            step1Text += "<input type='radio' id='radio2' name='radio' "+getButton(textAlignCenterButton,'button')+"><label for='radio2'><img src='/images/design/text_tools_align_center.png' title='Text align center' class='align'></label>";
            step1Text += "<input type='radio' id='radio3' name='radio' "+getButton(textAlignRightButton,'button')+"><label for='radio3'><img src='/images/design/text_tools_align_right.png' title='Text align right'  class='align'></label>";
            step1Text += "<input type='radio' id='radio4' name='radio'  "+getButton(textAlignJustifyButton,'button')+"><label for='radio4'><img src='/images/design/text_tools_align_justify.png' title='Text align justify' class='align'></label>";
            step1Text += "</div>";
            step1Text += "<button id='fit'>Fit to card</button>";
            step1Text += "</div>";
            step1Text += "</div>";
            step1Text += "</div>";

			step1Text += "<div id='fontsDiv' style='display:none'>";
			step1Text += "<div class='header-buttons font'><h3>Select a Font</h3></div>";
			step1Text += "<div class='somediv'>";
            step1Text += "<div id='Somebody' class='fonts "+getButton('Somebody','font')+"' title='Somebody You Used To Know'>Somebody</div>";
            step1Text += "<div id='Tempus' class='fonts "+getButton('Tempus','font')+"' title='Tempus'>Tempus</div>";
            step1Text += "<div id='Brooklyn' class='fonts "+getButton('Brooklyn','font')+"' title='Brooklyn'>Brooklyn</div>";
            step1Text += "<div id='Claudette' class='fonts "+getButton('Claudette','font')+"' title='Claudette Aimele Chocolat'>Claudette</div>";
            step1Text += "<div id='Baskerville' class='fonts "+getButton('Baskerville','font')+"' title='Baskerville'>Baskerville</div>";
            step1Text += "<div id='Myriad' class='fonts "+getButton('Myriad','font')+"' title='Myriad'>Myriad</div>";
			step1Text += "</div>";
			step1Text += "<div class='world-count'>Number of symbols should not exceed 750</div>";
			step1Text += "</div>";

            step1Text += "<div id='envelopeAndCard'>";
//            step1Text += "<button id='previewButton' class='editButton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'><span class='ui-button-text'>Click to edit the card</span></button>";
            step1Text += "<div id='envelopeClosed'>";
            step1Text += "<img id='envelopeImgTop' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_top_back.png'>";
            step1Text += "<img id='envelopeBack' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_back.png'>";
            step1Text += "</div>";
            step1Text += "<img id='cardFrontTop' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_top.png'>";
            step1Text += "<img id='cardFrontBottom' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_bottom.png'>";
            step1Text += "</div>";

//            step1Text += "<div id='cardFrontDiv' class='cardFrontAnimated3'>";

//            step1Text += "</div>";


                if (isOpeningCard) {
					step1Text += "<div id='cardTextDiv' class='openingChoice open-crt-holder' style='display:none'>";
					step1Text += "<div id='envelope'>";
					step1Text += "<img class='envelopeTopBack' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_top_back.png'>";
					step1Text += "<img class='envelopeBack' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_back.png'>";
					step1Text += "</div>";
                    step1Text += "<img class='cardOpenSize"+cardSize+"' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_open_left.png'>";
                    step1Text += "<img class='cardOpenSize"+cardSize+"' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_open_right.png'>";
                    step1Text += "<textarea id='cardText' maxlength='750' class='cardTextSize"+cardSize+"Opening' readonly></textarea>";
                    //if (cardSize <= 1) defaultFontSize = 11;
					step1Text += " </div>";
                }
                else {
					step1Text += "<div id='cardTextDiv' class='openingChoice single-crt-holder' style='display:none'>";
					step1Text += "<div id='envelope'>";
					step1Text += "<img class='envelopeTopBack' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_top_back.png'>";
					step1Text += "<img class='envelopeBack' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_back.png'>";
					step1Text += "</div>";
                    if (cardSize <= 1) {
                        step1Text += "<img class='cardFront' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front.png'>";
                    }
                    else {
                        step1Text += "<img class='cardFrontTop' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_top.png'>";
                        step1Text += "<img class='cardFrontBottom' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_bottom.png'>";
                    }
                    step1Text += "<textarea id='cardText' maxlength='750' class='cardTextSize"+cardSize+"NonOpening' readonly></textarea>";
					step1Text += " </div>";
                }


            return step1Text;
        }

        /*function checkIfOpenCard(){
            var dir = "/_images/cards_size_"+cardSize+"/card_"+cardNumber+"/";
            var cardPart = "card_open";
            var isOpeningCard = false;
            $.ajax({
                //This will retrieve the contents of the folder if the folder is configured as 'browsable'
                url: dir,
                success: function (data) {
                    //List all jpg file names in the page
                    $(data).find("a:contains(" + cardPart + ")").each(function () {
                        var fileName = this.href.slice(this.href.indexOf(cardPart));
                        var className = fileName.slice(0,fileName.indexOf('.'));
                        $(".cardFrontTop").remove();
                        $(".cardFrontBottom").remove();
                        $('#cardText').before($("<img class="+className+" src=" + dir + fileName + "></img>"))
                        .addClass('cardTextSize'+cardSize+'Opening')
                        .removeClass('cardTextSize'+cardSize+'NonOpening');
                    });
                }
            });
        }*/

        function textEditorInteractions(){
            $( "#spinner" ).spinner();

            $('#color').colorPicker({showHexField: false});
            $( "#format" ).buttonset();
            $( "#align" ).buttonset();
            $( "#fit" ).button({
                text: false,
                icons: {
                    primary: "ui-icon-arrow-4-diag"
                }
            });
            $('#cardText').val(cardText);
            $('#cardText').css('font-size', fontSize);
            $('#cardText').css("text-align", textAlign);
            $('#cardText').css('font-family', fontFamily);
            $( "#spinner" ).spinner( "value", parseInt(fontSizeButton) );
            //$( "#spinner" ).spinner( "value", parseInt(fontSizeButton) );

                    /*, function() {
                $(this).css('font-size', defaultFontSize);
                $( "#spinner" ).spinner( "value", defaultFontSize );
                alert(defaultFontSize);
                //autoResizeText($('#cardText'));

                $(window).resize(function() {
                    autoResizeText($('#cardText'));
                });
            });*/


            /*$(window).resize(function() {
                autoResizeText($('#cardText'));
            });*/

            $( "#spinner" ).keypress(function(event){
                if (event.which == 13) {
                    //alert( $( "#spinner" ).spinner( "value" ));
                    //alert($('#cardText').attr("rows"));
                    var fontSize = $( "#spinner" ).spinner("value");
                    if (fontSize <= 0) $( "#spinner" ).spinner("value", "1")
                    $('#cardText').css('font-size',$( "#spinner" ).spinner("value"));
                    autoResizeText($('#cardText'));
                }
            });
            $( "#spinnerDiv" ).click(function(){
                //alert( $( "#spinner" ).spinner( "value" ));
                var fontSize = $( "#spinner" ).spinner("value");
                if (fontSize <= 0) $( "#spinner" ).spinner("value", "1")
                $('#cardText').css('font-size',$( "#spinner" ).spinner("value"));
                autoResizeText($('#cardText'));
            });

            $('#color').change(function(){
                $('#cardText').css("color",$('#color').val());
            });
            /*$( "#colorButton" ).click(function() {
                $( "#color" ).trigger( "click" );
            });*/
            $( "#check1" ).click(function(){
                //alert($('#cardText').css("font-weight"));
                if ($('#cardText').css("font-weight") == "bold" || $('#cardText').css("font-weight") >= 600)
                    $('#cardText').css("font-weight","normal");
                else {
                    $('#cardText').css("font-weight","bold");
                    autoResizeText($('#cardText'));
                }
            });

            $( "#check2" ).click(function(){
                if ($('#cardText').css("font-style") == "italic")
                    $('#cardText').css("font-style","normal");
                else $('#cardText').css("font-style","italic");
            });
            $( "#radio1" ).click(function(){
                $('#cardText').css("text-align","left");
            });
            $( "#radio2" ).click(function(){
                $('#cardText').css("text-align","center");
            });
            $( "#radio3" ).click(function(){
                $('#cardText').css("text-align","right");
            });
            $( "#radio4" ).click(function(){
                $('#cardText').css("text-align","justify");
            });
            $( "#fit" ).click(function() {
                $('#cardText').css('font-size', '100px');
                autoResizeText($('#cardText'));
            });



            $( "#Somebody" ).click(function(){
                $('#cardText').css("fontFamily",$(this).css("fontFamily"))
                    .css('font-size', '20px')
                    .css('font-weight', 'bold')
                    .css("letter-spacing", "2px");
            });
            $( "#Tempus" ).click(function(){
                $('#cardText').css("fontFamily",$(this).css("fontFamily"))
                    .css('font-size', '18px')
                    .css('font-weight', 'bold')
                    .css("letter-spacing", "0");
            });
            $( "#Brooklyn" ).click(function(){
                $('#cardText').css("fontFamily",$(this).css("fontFamily"))
                    .css('font-size', '18px')
                    .css('font-weight', 'bold')
                    .css("letter-spacing", "0");
            });
            $( "#Claudette" ).click(function(){
                $('#cardText').css("fontFamily",$(this).css("fontFamily"))
                    .css('font-size', '24px')
                    .css('font-weight', 'normal')
                    .css("letter-spacing", "2px");
            });
            $( "#Baskerville" ).click(function(){
                $('#cardText').css("fontFamily",$(this).css("fontFamily"))
                    .css('font-size', '18px')
                    .css('font-weight', 'normal')
                    .css("letter-spacing", "0");
            });
            $( "#Myriad" ).click(function(){
                $('#cardText').css("fontFamily",$(this).css("fontFamily"))
                    .css('font-size', '18px')
                    .css('font-weight', 'normal')
                    .css("letter-spacing", "0");
            });

            $('#cardText').keypress(function(){
                while ($(this).prop('scrollHeight') > $(this).prop('clientHeight')) {
                    $(this).css('font-size', '-=1');
                }
            });

            $("#cardAppDiv").on("click", ".fonts", function(){
                autoResizeText($('#cardText'));
                if ($(this).hasClass('fontsHovered')) {
                    $(this).addClass('fontsClickedHovered');
                    $(this).removeClass('fontsHovered');
                    $( ".fonts" ).each(function(){
                        if ($(this).hasClass('fontsClicked')) {
                            $(this).addClass('fontsOriginal')
                            $(this).removeClass('fontsClicked');
                        }
                    });
                }
            })
            .on("mouseenter", ".fonts",
            function(){
                if ($(this).hasClass('fontsClicked')) {
                    $(this).addClass('fontsClickedHovered');
                    $(this).removeClass('fontsClicked');
                }
                else {
                    $(this).addClass('fontsHovered');
                    $(this).removeClass('fontsOriginal');
                }
             })

            .on("mouseleave", ".fonts",
                 function() {
                    if ($(this).hasClass('fontsClickedHovered')) {
                        $(this).addClass('fontsClicked');
                        $(this).removeClass('fontsClickedHovered');
                    }
                    else {
                        $(this).addClass('fontsOriginal');
                        $(this).removeClass('fontsHovered');
                    }
                 }
            );

        }

        function saveTextEdition() {
            cardText = $('#cardText').val();
            fontFamily = $('#cardText').css('fontFamily');
            fontSize = $('#cardText').css('fontSize');
            fontWeight = $('#cardText').css('fontWeight');
            fontStyle = $('#cardText').css('fontStyle');
            fontColor = $('#cardText').css('color');
            textAlign = $('#cardText').css('textAlign');

            fontFamilyButton = fontFamily;
            fontSizeButton = fontSize;
            fontColorButton = rgb2hex(fontColor);
            fontWeightButton = $( "#check1" ).is(':checked');
            fontStyleButton = $( "#check2" ).is(':checked');
            textAlignLeftButton = $( "#radio1" ).is(':checked');
            textAlignCenterButton = $( "#radio2" ).is(':checked');
            textAlignRightButton = $( "#radio3" ).is(':checked');
            textAlignJustifyButton = $( "#radio4" ).is(':checked');
            //alert(fontWeightButton);
        }

        function loadTextEdition() {
            $('#cardText').val(cardText);
            $('#cardText').css('fontFamily',fontFamily);
            $('#cardText').css('fontSize',fontSize);
            $('#cardText').css('fontWeight',fontWeight);
            $('#cardText').css('fontStyle',fontStyle);
            $('#cardText').css('color',fontColor);
            $('#cardText').css('textAlign',textAlign);
        }

        function autoResizeText(thisText){

            while (thisText.prop('scrollHeight') > thisText.prop('clientHeight')) {
                thisText.css('font-size', '-=1');
            }
            $( "#spinner" ).spinner( "value", parseInt(thisText.css('font-size')) );

            return thisText.css('font-size');
        }

        function getButton(buttonProperty, buttonType) {
            if (buttonType == 'font') {
                if (fontFamilyButton.search(buttonProperty) >= 0) return 'fontsClicked';
                else return 'fontsOriginal';
            }
            else if (buttonType == 'button') {
                if (buttonProperty) return 'checked';
                else return '';
            }
            else if (buttonType == 'book') {
                if (cardBooks.indexOf(buttonProperty) >= 0) return 'booksClicked';
                else return 'booksOriginal';
            }
        }

        // -------------------- STEP 3 --------------------
        function booksEditor(){
            var step3Text = "";

            step3Text += "<div id='booksListDiv'>";
			step3Text += "<p class='booksList-description'>Scroll down to select Volumes</p>";
            step3Text += "<div id='booksList'>";
            step3Text += "</div>";
            step3Text += "<p id='booksInstruction'>Click to add the image of your selected eBook(s) to the back of your card. Click again to remove it.</p>";
            step3Text += " </div>";

            step3Text += "<div id='cardBackDiv'>";
            step3Text += "<img id='cardBackSize"+cardSize+"' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_back.png' width='50%;'>";
            step3Text += "<div id='booksPrintSize"+cardSize+"'></div>";
            step3Text += "</div>";

            return step3Text;
        }

        var booksChecked = 0;
        function booksEditorInteractions(){
            $("#booksList").on("click", ".books", function(){
                $.loader();
                var bookUrl = $(this).attr('src');
                var index = bookUrl.indexOf('book_');
                var book = bookUrl.substr(index, 6).replace("_","");
                var books = getUnlinkedBookCounts();
                var _this = this;
                if ($(this).hasClass('booksClickedHovered')) {
                    if(booksAdded[book] != 0) {
                        $.loader("close");
                        $( '<div id="dialog-confirm" title="Remove eBook from Cart?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will remove an eBook from your cart and decrease your order total.</p></div>' ).dialog({
                          resizable: false,
                          height:240,
                          modal: true,
                          buttons: {
                            "Yes, remove it from my cart": function() {
                                $( this ).dialog( "close" );
                                $.loader();
                                removeBooksFromCart(book);
                                booksAdded[book] --;
                                $(_this).removeClass('booksHovered');
                                $(_this).removeClass('booksClicked');
                                $(_this).addClass('booksOriginal');
                                refreshBooksDisplay($(_this).attr('src'), false);
                                $.loader("close");
                            },
                            "No, cancel this action": function() {
                              $( this ).dialog( "close" );
                            }
                          }
                        });
                    } else {
                        books[book]++;
                        $(this).addClass('booksHovered');
                        $(this).removeClass('booksClickedHovered');
                        refreshBooksDisplay($(this).attr('src'), false);
                        $.loader("close");
                    }


                }
                else {
                    if(books[book] == 0 ) {
                        $.loader("close");
                        $( '<div id="dialog-confirm" title="Add eBook to Cart?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This eBook is not available in your current cart or has been linked with another eCard.<br>Would you like to add this eBook to your cart?<br>Adding to your cart will increase your order total.</p></div>' ).dialog({
                          resizable: false,
                          height:340,
                          modal: true,
                          buttons: {
                            "Yes, add it to my cart": function() {
                                $( this ).dialog( "close" );
                                $.loader();
                                addBooksToCart(book);
                                booksAdded[book] ++;
                                $(_this).addClass('booksClickedHovered');
                                $(_this).removeClass('booksHovered');
                                refreshBooksDisplay($(_this).attr('src'), true);
                                $.loader("close");
                            },
                            "No, I don't want it": function() {
                              $( this ).dialog( "close" );
                            }
                          }
                        });
                    } else {
                        //alert(books[book]);
                        addBooksToCart(book);
                        booksAdded[book] ++;
                        $(this).addClass('booksClickedHovered');
                        $(this).removeClass('booksHovered');
                        refreshBooksDisplay($(this).attr('src'), true);
                        $.loader("close");

                    }

                }
            })
            .on("mouseenter", ".books",
            function(){
                if ($(this).hasClass('booksClicked')) {
                    $(this).addClass('booksClickedHovered');
                    $(this).removeClass('booksClicked');
                }
                else {
                    $(this).addClass('booksHovered');
                    $(this).removeClass('booksOriginal');
                }
             })
            .on("mouseleave", ".books",
                 function() {
                    if ($(this).hasClass('booksClickedHovered')) {
                        $(this).addClass('booksClicked');
                        $(this).removeClass('booksClickedHovered');
                    }
                    else {
                        $(this).addClass('booksOriginal');
                        $(this).removeClass('booksHovered');
                    }
                 }
            );
            var dir = "/images/books/";
            var cardPart = "book_";
            $.ajax({
                //This will retrieve the contents of the folder if the folder is configured as 'browsable'
                url: dir,
                success: function (data) {
                    //List all jpg file names in the page
                    var i = 1;
                    $(data).find("a:contains(" + cardPart + ")").each(function () {
                        var filename = this.href.slice(this.href.indexOf(cardPart));
                        $("#booksList").append($("<img class='books "+getButton(dir+filename,'book')+"' src=" + dir + filename + "></img><p class='book-label'>Volume " + i + "</p>"));
                        //$("#booksPrint").append($("<img class='booksOnCard' src=" + dir + filename + "></img>"));
                        i++;
                    });
                },
                            error: function() {
                                    for (var i = 1; i <= 7; i++) {
                        var filename = 'book_'+i+'.png';
                            $("#booksList").append($("<img class='books "+getButton(dir+filename,'book')+"' src=" + dir + filename + "></img><p class='book-label'>Volume " + i + "</p>"));
                                    }
                            }
            });
        }

        function addBooksToCart(book){
            var cat = bookSkus[book][0];

            $.ajax({
              dataType: "json",
              async: false,
              type: "POST",
              data: { sku: bookSkus[book][0]},
              url: prefix + "/default/ecard/index/addSkuToCart",
              success: function(returnValue) {
                  if(returnValue == false) {
                      alert("error adding to cart")
                  }
              }
            });
        }

        function removeBooksFromCart(book) {
            $.ajax({
              dataType: "json",
              async: false,
              type: "POST",
              data: { sku: bookSkus[book][0]},
              url: prefix + "/default/ecard/index/removeSkuFromCart",
              success: function(returnValue) {
                  if(returnValue == false) {
                      alert("error removing from cart")
                  }
              }
            });
        }

        var unlinkedBookCounts = false;

        function getUnlinkedBookCounts(useCache){
            useCache = typeof useCache !== 'undefined' ? useCache : true; //set default

            if(useCache && unlinkedBookCounts != false) {
                return unlinkedBookCounts;
            }



            var shoppingCart = null
            $.ajax({
              dataType: "json",
              async: false,
              url: prefix + "/default/ecard/index/getCartContents",
              success: function(shoppingCart) {
                if(typeof shoppingCart.items != "undefined") {
                    $.each(shoppingCart.items, function(key, item){
                        if(typeof item.sku != "undefined") {
                            $.each(bookSkus, function(k, skus){
                                $.each(skus, function(index, sku){
                                    if(item.sku == sku) {
                                        books[k] = item.qty;
                                    }
                                });
                            });
                        }
                    });
                }

                //figure out ecards
                if(typeof shoppingCart.ecards != "undefined") {
                    $.each(shoppingCart.ecards.created, function(key, value) {
                        var _skus = value.split(",");
                        $.each(_skus, function(_index, _sku){
                            $.each(bookSkus, function(k, skus){
                                $.each(skus, function(index, sku){
                                    if(_sku == sku) {
                                        books[k] --;
                                        if(books[k] < 0) {
                                            books[k] = 0;
                                        }
                                    }
                                });
                            });
                        });
                    });
                 }
              }
            });

            if(step)

            unlinkedBookCounts = books
            console.log(books);
            return books;
        }

        function refreshBooksDisplay(imgClicked, moreBooks){
            if(!moreBooks) {
                booksChecked--;
                $(".booksOnCard").each(function(){
                    if ($(this).attr("src") == imgClicked) {
                        $(this).remove();
                        cardBooks.splice(cardBooks.indexOf(imgClicked), 1);
                    }
                });
            }
            else {
                booksChecked++;
                $("#booksPrintSize"+cardSize).append($("<img class='booksOnCard' src="+imgClicked+"></img>"));
                cardBooks.push(imgClicked);
            }
            resizeBooks(booksChecked, 'cardBackSize'+cardSize);
        }

        function loadBooks(thisImage) {
            for (var i = 0; i < cardBooks.length; i++) {
                $("#booksPrintSize"+cardSize).append($("<img class='booksOnCard' src="+cardBooks[i]+"></img>"));
            }
            resizeBooks(cardBooks.length, thisImage);
        }

        function resizeBooks(numberOfBooks, thisImage){
            var w = $('#'+thisImage).width();
            var h = $('#'+thisImage).height();
            //alert(h);
            if (cardSize <= 1) {
                switch (numberOfBooks){
                    case 1:
                        $(".booksOnCard").width("50%");
                        $("#booksPrintSize1").css("paddingTop",h*0.093);//"5%");
                        //alert($("#booksPrintSize1").css("paddingTop"));
                        //alert(h);
                        break;
                    case 2:
                        $(".booksOnCard").width("40%");
                        $("#booksPrintSize1").css("paddingTop",h*0.14);//"7.5%");
                        break;
                    case 3:
                        $(".booksOnCard").width("30%");
                        $("#booksPrintSize1").css("paddingTop",h*0.233);//"12.5%");
                        break;
                    case 4:
                    case 5:
                    case 6:
                        $(".booksOnCard").width("25%");
                        $("#booksPrintSize1").css("paddingTop",h*0.074);//"4%");
                        break;
                    default: // 7 books
                        $(".booksOnCard").width("20%");
                        $("#booksPrintSize1").css("paddingTop",h*0.14);//"7.5%");
                        //$(".booksOnCard").width("31%");
                        //$("#booksPrint").css("paddingTop","12.5%");
                        break;
                }
            }
            else {
                switch (numberOfBooks){
                    case 1:
                        $(".booksOnCard").width("100%");
                        $("#booksPrintSize2").css("paddingTop",h/5.75);//"12.5%");
                        break;
                    case 2:
                        $(".booksOnCard").width("77%");
                        $("#booksPrintSize2").css("paddingTop",h/9.58);//"7.5%");
                        break;
                    case 3:
                    case 4:
                        $(".booksOnCard").width("48%");
                        $("#booksPrintSize2").css("paddingTop",h/5.75);//"12.5%");
                        break;
                    case 5:
                    case 6:
                        $(".booksOnCard").width("48%");
                        $("#booksPrintSize2").css("paddingTop",h/12);//"6%");
                        break;
                    default: // 7 books
                        $(".booksOnCard").width("40%");
                        $("#booksPrintSize2").css("paddingTop",h/20.54);//"3.5%");
                        //$(".booksOnCard").width("31%");
                        //$("#booksPrint").css("paddingTop","12.5%");
                        break;
                }
            }
        }

        // -------------------- STEP 4 --------------------
        function cardPreview(){
            var step4Text = "";

            step4Text += "<div id='previewDiv'>";
            step4Text += "<button id='previewButton'>Click to view the card</button>";
            step4Text += "<div id='cardPreview'>";
            step4Text += "<img id='envelopeFrontAnimation' class='envelopeFrontSet' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_front.png'>";
            step4Text += "<div id='textPreview' class='envelopeFrontSet'>";
            step4Text += "<p>Recipient's name will go here</p>";
            step4Text += "</div>";
            step4Text += "</div>";
            step4Text += "</div>";

            return step4Text;
        }

        function cardPreviewInteractions(){

            $( "#previewButton" ).button()
            .click(function(){
                startAnimation();
            });
        }

        // -------------------- STEP 5 --------------------
        function sendCardForm(){
            var step5Text = "";

            step5Text += "<form id='emailForm' name='emailForm' method='post' action='" + prefix+ "/default/ecard/index/processor'>";
            step5Text += "<div class='formDiv to-fields'>";
            step5Text += "<div class='sender-block'>";
            step5Text += "<div class='sender-from'>From: <textarea id='fromNameField' name='fromNameField' autocomplete='on' class='textField senderNameField' placeholder='Your name' wrap='off' rows='1' cols='20'></textarea><span class='error' style='display:none;' id='from-name'>- Please enter your name!</span></div>";
            step5Text += "<div class='sender-email'>E-Mail: <textarea id='fromEmailField' name='fromEmailField' autocomplete='on' class='textField senderEmailField' placeholder='your@address.com' wrap='off' rows='1' cols='30'></textarea><span class='error' style='display:none;' id='from-email'>- The email address is invalid!</span></div>";
            step5Text += "</div>";
            step5Text += "</div>";
            step5Text += "<div class='formDiv person'>";
            step5Text += "<div class='person-block'>";
            step5Text += "<div class='person-to'>To: <textarea name='toNameField0' class='textField toNameField' autocomplete='on' placeholder='Recipient\'s name' wrap='off' rows='1' cols='24'></textarea></div>";
            step5Text += "<div class='person-email'>E-Mail: <textarea name='toEmailField0' class='textField toEmailField' autocomplete='on' placeholder='recipient@address.com' wrap='off' rows='1' cols='30'></textarea></div>";
            step5Text += "<div class='person-schedule'> Schedule Sending: <input type='text' name='toSendDate0' autocomplete='on' class='textField toSendDate'  placeholder='(click)'></div>";
            step5Text += "</div>";
            step5Text += "</div>";
            step5Text += "<div id='people' class='formDiv people'></div>";
            step5Text += "<p class='pink'>Each time you 'Add a Person' your cart total will increase. Click 'Done' before designing a new card or checking out.</p>";
            step5Text += "<p class='addremove-controls'><a href='#' class='addPerson'>Add a Person</a></p>";
            step5Text += "<div class='cartWrapper'></div><h3>Your Cart:</h3>";
            step5Text += "<div class='clearfix'></div>";
            step5Text += "</form>";
            step5Text += "<div id='messagesDiv'></div>";
            step5Text += "<input type='submit' id='sendCard' value='Done'>";
            return step5Text;
        }

        function sendCardInteractions(){
            $('#sendCard').button()
            .click(function(){
                $('.error').remove();

                function isEmail(email) {
                  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                  return regex.test(email);
                }
                var error = false;

                if($($('#fromNameField')[0]).val() == "") {
                    $('#from-name').show();
                    error = true;
                    $($('#fromNameField')[0]).css('border', 'red 3px solid');
                } else {
	                $($('#fromNameField')[0]).css('border', '1px solid #cbcbcb');
	                $('#from-name').hide();
                }

                if(!isEmail($($('#fromEmailField')[0]).val())) {
                    $('#from-email').show();
                    error = true;
                    $($('#fromEmailField')[0]).css('border', 'red 3px solid');
                } else {
	                $($('#fromEmailField')[0]).css('border', '1px solid #cbcbcb');
	                $('#from-email').hide();
                }


					if($($('.senderNameField')[0]).val() == "") {
						$($('.sender-from')[0]).append('<span class="error">- Please enter a sender name!</span>');
						error = true;
						$($('.senderNameField')[0]).css('border', 'red 3px solid');
					} else {
						$($('.senderNameField')[0]).css('border', '1px solid #cbcbcb');
					}
					if(!isEmail($($('.senderEmailField')[0]).val())) {
						$($('.sender-email')[0]).append('<span class="error">- The email address is invalid!</span>');
						error = true;
						$($('.senderEmailField')[0]).css('border', 'red 3px solid');
					} else {
						$($('.senderEmailField')[0]).css('border', '1px solid #cbcbcb');
					}



                if($($('.toNameField')[0]).val() == "") {
                    $($('div.person .person-to')[0]).append('<span class="error">- Please enter a recipient\'s name!</span>');
                    error = true;
					$($('.toNameField')[0]).css('border', 'red 3px solid');
                } else {
	                $($('.toNameField')[0]).css('border', '1px solid #cbcbcb');
                }
                if(!isEmail($($('.toEmailField')[0]).val())) {
                    $($('div.person .person-email')[0]).append('<span class="error">- The email address is invalid!</span>');
                    error = true;
					$($('.toEmailField')[0]).css('border', 'red 3px solid');
                } else {
	                $($('.toEmailField')[0]).css('border', '1px solid #cbcbcb');
                }


                for(var i = 1; i < $('.toNameField').length; i++) {
                    if($($('.toNameField')[i]).val() == "") {
                        $($('div.people .person-to')[i - 1]).append('<span class="error">- Please enter a recipient\'s name!</span>');
                        error = true;
	                    $($('.toNameField')[i]).css('border', 'red 3px solid');
	                } else {
		                $($('.toNameField')[i]).css('border', '1px solid #cbcbcb');
	                }
                    if(!isEmail($($('.toEmailField')[i]).val())) {
                        $($('div.people .person-email')[i - 1]).append('<span class="error">- The email address is invalid!</span>');
                        error = true;
	                    $($('.toEmailField')[i]).css('border', 'red 3px solid');
	                } else {
		                $($('.toEmailField')[i]).css('border', '1px solid #cbcbcb');
	                }
                    //if($('.toEmailField')[i])
                }



                /*var fromNameField = $('#fromNameField').val();
                var fromEmailField = $('#fromEmailField').val();
                var toNameField = $('#toNameField').val();
                var toEmailField = $('#toEmailField').val();

                if (fromNameField != "" && fromEmailField != "" && toNameField != "" && toEmailField != "")
                {
                */
                if(!error) {


                    $('#messagesDiv').empty();
                    var data = {
                        //toNameField: toNameField,
                        //toEmailField: toEmailField,
                        //toSendDate: toSendDate,
                        cardSize: cardSize,
                        cardNumber: cardNumber,
                        isOpeningCard: isOpeningCard,
                        cardText: cardText,
                        fontFamily: fontFamily,
                        fontSize: fontSize,
                        fontWeight: fontWeight,
                        fontStyle: fontStyle,
                        fontColor: fontColor,
                        textAlign: textAlign,
                        cardBooks: cardBooks
                    };
                    if(typeof magentoItemId !== 'undefined') {
                        data.itemId = magentoItemId;
                    }
                    var toNameField0 = $('textarea[name=toNameField0]').val();
                    var toNameField1 = $('textarea[name=toNameField1]').val();
                    var toNameField2 = $('textarea[name=toNameField2]').val();
                    var toNameField3 = $('textarea[name=toNameField3]').val();
                    var toNameField4 = $('textarea[name=toNameField4]').val();
                    var toNameField5 = $('textarea[name=toNameField5]').val();
                    var toNameField6 = $('textarea[name=toNameField6]').val();
                    var toNameField7 = $('textarea[name=toNameField7]').val();
                    var toNameField8 = $('textarea[name=toNameField8]').val();
                    var toNameField9 = $('textarea[name=toNameField9]').val();

                    var toEmailField0 = $('textarea[name=toEmailField0]').val();
                    var toEmailField1 = $('textarea[name=toEmailField1]').val();
                    var toEmailField2 = $('textarea[name=toEmailField2]').val();
                    var toEmailField3 = $('textarea[name=toEmailField3]').val();
                    var toEmailField4 = $('textarea[name=toEmailField4]').val();
                    var toEmailField5 = $('textarea[name=toEmailField5]').val();
                    var toEmailField6 = $('textarea[name=toEmailField6]').val();
                    var toEmailField7 = $('textarea[name=toEmailField7]').val();
                    var toEmailField8 = $('textarea[name=toEmailField8]').val();
                    var toEmailField9 = $('textarea[name=toEmailField9]').val();

                    var toSendDate0 = $('input[name=toSendDate0]').val();
                    var toSendDate1 = $('input[name=toSendDate1]').val();
                    var toSendDate2 = $('input[name=toSendDate2]').val();
                    var toSendDate3 = $('input[name=toSendDate3]').val();
                    var toSendDate4 = $('input[name=toSendDate4]').val();
                    var toSendDate5 = $('input[name=toSendDate5]').val();
                    var toSendDate6 = $('input[name=toSendDate6]').val();
                    var toSendDate7 = $('input[name=toSendDate7]').val();
                    var toSendDate8 = $('input[name=toSendDate8]').val();
                    var toSendDate9 = $('input[name=toSendDate9]').val();

                    if(toEmailField0 != "") {
                        data.toNameField0 = toNameField0;
                        data.toEmailField0 = toEmailField0;
                        data.toSendDate0 = toSendDate0;
                    }
                    if(toEmailField1 != "") {
                        data.toNameField1 = toNameField1;
                        data.toEmailField1 = toEmailField1;
                        data.toSendDate1 = toSendDate1;
                    }
                    if(toEmailField2 != "") {
                        data.toNameField2 = toNameField2;
                        data.toEmailField2 = toEmailField2;
                        data.toSendDate2 = toSendDate2;
                    }
                    if(toEmailField3 != "") {
                        data.toNameField3 = toNameField3;
                        data.toEmailField3 = toEmailField3;
                        data.toSendDate3 = toSendDate3;
                    }
                    if(toEmailField4 != "") {
                        data.toNameField4 = toNameField4;
                        data.toEmailField4 = toEmailField4;
                        data.toSendDate4 = toSendDate4;
                    }
                    if(toEmailField5 != "") {
                        data.toNameField5 = toNameField5;
                        data.toEmailField5 = toEmailField5;
                        data.toSendDate5 = toSendDate5;
                    }
                    if(toEmailField6 != "") {
                        data.toNameField6 = toNameField6;
                        data.toEmailField6 = toEmailField6;
                        data.toSendDate6 = toSendDate6;
                    }
                    if(toEmailField7 != "") {
                        data.toNameField7 = toNameField7;
                        data.toEmailField7 = toEmailField7;
                        data.toSendDate7 = toSendDate7;
                    }
                    if(toEmailField8 != "") {
                        data.toNameField8 = toNameField8;
                        data.toEmailField8 = toEmailField8;
                        data.toSendDate8 = toSendDate8;
                    }
                    if(toEmailField9 != "") {
                        data.toNameField9 = toNameField9;
                        data.toEmailField9 = toEmailField9;
                        data.toSendDate9 = toSendDate9;
                    }



                    $.post(prefix + '/default/ecard/index/processor', data, function(result) {
                        result = result.split(",");
                        $('#emailForm').hide(500);
                        $('#sendCard').hide(500);
                        $('#messagesDiv').prepend("<p style='color:#ec008c; margin: 7% 0;'>Card(s) saved, you will be redirected to your cart in 3 seconds...</p>");
                        cardSent = true;
                        setTimeout(function(){
                            if ($('#cardAppExit').attr('href') != '')
                                window.location = $('#cardAppExit').attr('href');
                            else
                                window.location = prefix + '/default/checkout/cart/';
                                //window.location = 'ecard/index/displaycard?id='+ result[0] +'&email='+ result[1];
                           //window.location = 'http://leo.forner.free.fr/giftcard/users_cards_data/'+fromNameField+'/'+toNameField+'.txt';
                        }, 3000);
                    });
                       //$('#toNameField').val(result);
                        //alert(result);
                }
                    /*$.get('cardprocessing.php', function(result) {
                       $('#toNameField').val(result);
                    });*/
                /* }
                else {
                    //$('#messagesDiv').empty();
                    if (toEmailField == "") {
                        //$('#messagesDiv').prepend("<p>Please provide a receiver email address.</p>");
                        $('#message4').css('visibility', 'visible');
                        $('#toEmailField').focus();
                    }
                    else $('#message4').css('visibility', 'hidden');
                    if (toNameField == "") {
    //                    $('#messagesDiv').prepend("<p>Please provide a receiver name.</p>");
                        $('#message3').css('visibility', 'visible');
                        $('#toNameField').focus();
                    }
                    else $('#message3').css('visibility', 'hidden');
                    if (fromEmailField == "") {
    //                    $('#messagesDiv').prepend("<p>Please provide your email address.</p>");
                        $('#message2').css('visibility', 'visible');
                        $('#fromEmailField').focus();
                    }
                    else $('#message2').css('visibility', 'hidden');
                    if (fromNameField == "") {
    //                    $('#messagesDiv').prepend("<p>Please provide your name.</p>");
                        $('#message1').css('visibility', 'visible');
                        $('#fromNameField').focus();
                    }
                    else $('#message1').css('visibility', 'hidden');
                }
                */
            });
        }


        function startAnimation() {
            var booksLoaded = false;
            var animStep = -1;

            var cardImg = "";
            cardImg += "<div class='envDiv'>";
            cardImg += "<div id='envelopeMessage'><p>Click to open</p></div>";
            cardImg += "<img id='envelopeBackAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_back.png'>";
            cardImg += "<img id='envelopeTopBackAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_top_back.png'>";
            cardImg += "</div>";

            cardImg += "<div id='cardDiv'>";

            if (isOpeningCard) {
                cardImg += "<img id='cardOpenLeftAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_open_left.png'>";
                cardImg += "<img id='cardOpenRightAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_open_right.png'>";
                cardImg += "<textarea class='cardTextAnimation textAnim_size"+cardSize+"Opening' readonly></textarea>";
                cardImg += "<p class='open-text' id='open-text1'>Click to open card</p><p class='open-text' id='open-text2'>Click to view your gift</p><div id='cardFrontDiv'>";

                if (cardSize >= 2) {
                    cardImg += "<img id='cardFrontTopAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_top.png'>";
                    cardImg += "<img id='cardFrontBottomAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_bottom.png'>";
                }
                else cardImg += "<img id='cardFrontTopAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front.png'>";

                cardImg += "</div>";
            }
            else {
                cardImg += "<div id='cardFrontDiv'>";

                if (cardSize >= 2) {
                cardImg += "<img id='cardFrontTopAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_top.png'>";
                    cardImg += "<img id='cardFrontBottomAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front_bottom.png'>";
                }
                else cardImg += "<img id='cardFrontTopAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_front.png'>";

                cardImg += "<textarea class='cardTextAnimation textAnim_size"+cardSize+"NonOpening' readonly></textarea>";
                cardImg += "</div>";
            }

            cardImg += "<img id='cardBackAnimation' class='booksSet' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/card_back.png'>";
            cardImg += "<div id='booksPrintSize"+cardSize+"Div' class='booksSet'><div id='booksPrintSize"+cardSize+"'></div></div>";
            cardImg += "</div>";

            cardImg += "<div class='envDiv'>";
            cardImg += "<img id='envelopeBackNoBottomAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_back_no_bottom.png'>";
            cardImg += "<img id='envelopeTopFrontAnimation' src='/images/cards_size_"+cardSize+"/card_"+cardNumber+"/envelope_top_front.png'>";
            cardImg += "</div>";

            $('#envelopeFrontAnimation').after(cardImg);

            //alert(parseInt(fontSize)*1.3);
            $('.cardTextAnimation').val(cardText)
            .css('fontFamily',fontFamily)
            //.css('fontSize',parseInt(fontSize)*1.3 + 'px')
            .css('fontWeight',fontWeight)
            .css('fontStyle',fontStyle)
            .css('color',fontColor)
            .css('textAlign',textAlign);
            $('#open-text1').css('visibility', 'hidden');
	        $('#open-text2').css('visibility', 'hidden');

            if(fontFamily.search('Somebody') != -1) {
                 $('.cardTextAnimation').css('fontSize',parseInt(fontSize) + 'px')
                    .css('lineHeight',parseInt(fontSize) + 'px')
                    .css('paddingTop', '22px');
            }

            if(fontFamily.search('Tempus') != -1) {
                 $('.cardTextAnimation').css('fontSize',parseInt(fontSize) + 'px')
                    .css('lineHeight',parseInt(fontSize) + 'px')
                    .css('paddingTop', '22px');
            }

            if(fontFamily.search('Brooklyn') != -1) {
                 $('.cardTextAnimation').css('fontSize',parseInt(fontSize) + 'px')
                    .css('lineHeight',parseInt(fontSize) + 'px')
                    .css('paddingTop', '5px');
            }
            if(fontFamily.search('Baskerville') != -1) {
                 $('.cardTextAnimation').css('fontSize',parseInt(fontSize) + 'px')
                    .css('lineHeight',parseInt(fontSize) + 'px')
            }
            if(fontFamily.search('Claudette') != -1) {
                 $('.cardTextAnimation').css('fontSize',parseInt(fontSize) + 'px')
                    .css('lineHeight',parseInt(fontSize) + 'px')
                    .css('paddingTop', '22px');
            }
            if(fontFamily.search('Myriad') != -1) {
                 $('.cardTextAnimation').css('fontSize',parseInt(fontSize) + 'px')
                    .css('lineHeight',parseInt(fontSize) + 'px')
            }


            $('.cardTextAnimation:first').css('fontSize',autoResizeText($('.cardTextAnimation:last')));

            if (cardSize >= 2) {
                //$('#booksPrintSize2').css('top','40px').css('left','67px').width('56%');
            }
            else {
                $('#booksPrintSize1').css('top','20px').css('left','30px').width('77%');
            }

            //cardBooks[0] = '/_images/books/book_1.png';

            $('#textback_text').hide(500);
            $('#textback_back').hide(500);
            $( "#previewButton" ).hide(500);
            $( ".envelopeFrontSet" ).hide(500);

            $( "#previewDiv" ).animate({
                //marginTop:'+=310px',
                height: '700px'

            },1000,function(){

                $('.envDiv').addClass('envDivAnim0');
                $('#cardDiv').addClass('envDivAnim0');
                animStep = 0;

                $( '#envelopeMessage' ).delay(2500).animate({
                    opacity:'1'
                });
            });

            $( "#previewDiv" ).click(function(){
                if (animStep == 0) {

                    $('#envelopeTopFrontAnimation').addClass('envelopeTopFrontAnimated1');
                    $('#envelopeTopBackAnimation').addClass('envelopeTopBackAnimated1');

                    $('#cardFrontDiv').addClass('cardFrontAnimated1');
                    $('#cardDiv').addClass('cardDivAnim1');

                    animStep = 1;

                    $( '#cardDiv' ).animate({
                        zIndex:'0'
                    },2000,function(){
                        $(this).css('zIndex','1');
                        $('#open-text1').css('visibility', 'visible');
                        $('#open-text2').css('visibility', 'hidden');
                    });
                    $('#open-text1').delay(3500).animate({
		                opacity:'1'
		            });
                }


                else if (isOpeningCard) {

                    if (animStep == 3) {

                        $('#cardOpenLeftAnimation').css('visibility', 'hidden');
                        $('#cardOpenRightAnimation').css('visibility', 'hidden');
                        $('#cardFrontDiv').css('visibility', 'visible');
                        $('#open-text1').css('visibility', 'visible');
					    $('#open-text2').css('visibility', 'hidden');

                        $('.booksSet').addClass('cardBackAnimated3')
                        .removeClass('cardBackAnimated_close');
                        $('#cardOpenLeftAnimation').removeClass('cardOpenLeftAnimated_open');
                        $('#cardOpenRightAnimation').removeClass('cardOpenRightAnimated_close');
                        $('#cardFrontDiv').addClass('cardFrontAnimated3')
                        .removeClass('cardFrontAnimated_close');


                        $('#cardDiv').addClass('cardDivAnim3')
                        .removeClass('cardDivAnim_close');
                        animStep = 4;
                    }
                    else if (animStep == 2) {

                        $('#cardFrontDiv').css('visibility', 'hidden');

						$('#open-text1').css('visibility', 'hidden');
						$('#open-text2').css('visibility', 'hidden');

                        $('#cardOpenRightAnimation').addClass('cardOpenRightAnimated_close');
                        $('.cardTextAnimation').addClass('cardOpenRightAnimated_close');
                        $('.booksSet').addClass('cardBackAnimated_close')
                        .removeClass('cardBackAnimated_open');
                        $('#cardFrontDiv').removeClass('cardFrontAnimated_open');

                        $('#cardDiv').addClass('cardDivAnim_close')
                        .removeClass('cardDivAnim_open');
                        animStep = 3;
                    }
                    else if (animStep == 1 || animStep == 4) {
						$('#open-text1').css('visibility', 'hidden');
						$('#open-text2').css('visibility', 'visible');
						$('#open-text2').delay(2000).animate({
			                opacity:'1'
			            });
                        if (!booksLoaded) {
                            loadBooks('cardBackAnimation');
                            booksLoaded = true;
                        }
                        $('#cardOpenLeftAnimation').css('visibility', 'visible');
                        $('#cardOpenRightAnimation').css('visibility', 'visible');
                        $('.cardTextAnimation').css('visibility', 'visible');
                        //$('#cardFrontTopAnimation').css('zIndex', '1');

                        $('#cardOpenLeftAnimation').addClass('cardOpenLeftAnimated_open')
                        .removeClass('cardOpenLeftAnimated3');
                        $('.cardTextAnimation').removeClass('cardOpenRightAnimated_close');
                        $('.booksSet').removeClass('cardBackAnimated3');
                        $('#cardFrontDiv').addClass('cardFrontAnimated_open')
                        .removeClass('cardFrontAnimated3')
                        .removeClass('cardFrontAnimated1');

                        $('#cardDiv').addClass('cardDivAnim_open')
                        .removeClass('cardDivAnim3');
                        animStep = 2;
                    }
                }
                else {
                    if (animStep == 2) {

                        $('#cardFrontDiv').addClass('cardFrontAnimated3')
                        .removeClass('cardFrontAnimated2');
                        $('.booksSet').addClass('cardBackAnimated3')
                        .removeClass('cardBackAnimated2');

                        animStep = 3;
                    }
                    else if (animStep == 1 || animStep == 3) {
                        if (!booksLoaded) {
                            loadBooks('cardBackAnimation');
                            booksLoaded = true;
                        }

                        $('#cardFrontDiv').addClass('cardFrontAnimated2')
                        .removeClass('cardFrontAnimated3');
                        $('.booksSet').addClass('cardBackAnimated2')
                        .removeClass('cardBackAnimated3');

                        animStep = 2;
                    }
                }
            });
        }
        $('#cardText').live('focusin',function () {
        if (this.value === cardText) {
	            this.value = '';
	        }
	    });
    });
});
