//jQuery.noConflict();
(function( $ ) {
    Reminder = function  (recipientsArr) {
        var recipients = recipientsArr;
        var name = '';
        var arrName = [];
        var arrEmail;

        jQuery.each(recipients, function(index, value){
            arrName.push(index);
        });

        jQuery( ".name-recipient" ).autocomplete({
            source: arrName,
            appendTo: '#name-container',
            select: function(e, ui) {
                name = ui.item.value
                jQuery.each(recipients, function(index, value) {
                    if (index == name) {
                        arrEmail = value;
                    }
                });
                jQuery( "#email-tags" ).autocomplete({
                    source: arrEmail,
                    appendTo: '#mail-container'
                });
            }
        });

        jQuery(".add-persan").click(function(e){
            jQuery( ".name-recipient" ).autocomplete({
                source: arrName,
                appendTo: '#name-container',
                select: function(e, ui) {
                    name = ui.item.value
                    jQuery.each(recipients, function(index, value) {
                        if (index == name) {
                            arrEmail = value;
                        }
                    });
                    var emailTeg = '#ecard_to_' + jQuery(this).attr('name').match(/[0-9]/g) + '_email';
                    jQuery( emailTeg ).autocomplete({
                        source: arrEmail,
                        appendTo: '#mail-container'
                    });
                }
            });
        });
    }

})( jQuery );