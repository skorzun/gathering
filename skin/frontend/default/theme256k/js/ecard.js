//jQuery.noConflict();
(function( $ ) {
	var defaultFontSize = 12;
	var hexDigits = new Array
	("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

	//Function to convert hex format to a rgb color
	function rgb2hex(rgb) {
		rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}

	function hex(x) {
		return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
	};
	var prefix = "";

	var step = 0;
	var contentForm = "";
	var contentData = "";
	var defaultFontSize = '25px';
	var buttonChecked = '';

	if (typeof window.fromNameField !== 'undefined') {
		var fromNameField = window.fromNameField;
	}
	if (typeof window.fromEmailField !== 'undefined') {
		var fromEmailField = window.fromEmailField;
	}
	if (typeof window.toNameField !== 'undefined') {
		var toNameField = window.toNameField;
	}
	if (typeof window.toEmailField !== 'undefined') {
		var toEmailField = window.toEmailField;
	}

	if (typeof window.cardSize !== 'undefined') {
		var cardSize = window.cardSize;
	} else {
		var cardSize = 2;
	}

	if (typeof window.cardNumber !== 'undefined') {
		var cardNumber = window.cardNumber;
	} else {
		var cardNumber = 1;
	}

	if (typeof window.isOpeningCard !== 'undefined') {
		var isOpeningCard = window.isOpeningCard;
	} else {
		var isOpeningCard = false;
	}

	if (typeof window.fontFamily !== 'undefined') {
		var fontFamily = window.fontFamily;
	} else {
		var fontFamily = "'Litos'";
	}

	if (typeof window.fontSize !== 'undefined') {
		var fontSize = window.fontSize;
	} else {
		var fontSize = defaultFontSize;
	}

	if (typeof window.fontWeight !== 'undefined') {
		var fontWeight = window.fontWeight;
	} else {
		var fontWeight = 'bold';
	}

	if (typeof window.fontStyle !== 'undefined') {
		var fontStyle = window.fontStyle;
	} else {
		var fontStyle = 'normal';
	}

	if (typeof window.fontColor !== 'undefined') {
		var fontColor = window.fontColor;
	} else {
		var fontColor = 'rgb(0,0,0)';
	}

	if (typeof window.textAlign !== 'undefined') {
		var textAlign = window.textAlign;
	} else {
		var textAlign = 'left';
	}

	if (typeof window.cardBooks !== 'undefined') {
		var cardBooks = window.cardBooks;
	} else {
		var cardBooks = new Array();
	}
	if (typeof window.itemId !== 'undefined') {
		var magentoItemId = window.itemId;
	} else {
		var cardBooks = new Array();
	}

	if (typeof window.cardText !== 'undefined') {
		var cardText = window.cardText;
	}
	var fontFamilyButton = fontFamily;
	var fontSizeButton = fontSize;
	var fontColorButton = rgb2hex(fontColor);
	function autoResizeText(thisText){
		console.log('scrollHeight', thisText.prop('scrollHeight'));
		console.log('clientHeight', thisText.prop('clientHeight'));

		while (thisText.prop('scrollHeight') > thisText.prop('clientHeight')) {
			//while (thisText.prop('scrollHeight') > 100) {
			thisText.css('font-size', '-=1');
		}
        jQuery( "#spinner" ).spinner( "value", parseInt(thisText.css('font-size')) );

		return thisText.css('font-size');
	}


	jQuery(document).ready(function () {

        jQuery('.egifts-view-index').on( "click", "#previewButton,#previewDiv", function() {
            jQuery('.egifts-view-index .container').addClass('active');
        })
        jQuery('.ecards-view-index').on( "click", "#previewButton,#previewDiv", function() {
            jQuery('.ecards-view-index .container').addClass('active');
        })
		jQuery('.evites-view-index').on( "click", "#previewButton,#previewDiv", function() {
			jQuery('.evites-view-index .container').addClass('active');
		})
        jQuery('#ecards-recipients-method').on( "click", "input", function() {
			if(jQuery(".input-manually").is(':checked')){
				jQuery("#ecards-recipients-csv").hide();
				jQuery(".recipients-block-holder").show();
			}
			if ( jQuery(".upload-csv").is(':checked')) {
				jQuery("#ecards-recipients-csv").show();
				jQuery(".recipients-block-holder").hide();
			}


		});

		jQuery('#booksListDiv').on( "click", "input", function() {
			if(jQuery(this).is(':checked')){
				jQuery(this).parent().addClass('active');
			}
			if ( !jQuery(this).is(':checked')) {
				jQuery(this).parent().removeClass('active');
			}
            jQuery('#ecards-gifts-view').removeClass('validate-error');
		});

		jQuery(".somediv").on("click", ".fonts", function(){
			autoResizeText(jQuery('#cardText'));
			if (jQuery(this).hasClass('fontsHovered')) {
				jQuery(this).addClass('fontsClickedHovered');
				jQuery(this).removeClass('fontsHovered');
				jQuery( ".fonts" ).each(function(){
					if (jQuery(this).hasClass('fontsClicked')) {
						jQuery(this).addClass('fontsOriginal')
						jQuery(this).removeClass('fontsClicked');
					}
				});
			}
		})
			.on("mouseenter", ".fonts",
			function(){
				if (jQuery(this).hasClass('fontsClicked')) {
					jQuery(this).addClass('fontsClickedHovered');
					jQuery(this).removeClass('fontsClicked');
				}
				else {
					jQuery(this).addClass('fontsHovered');
					jQuery(this).removeClass('fontsOriginal');
				}
			})

			.on("mouseleave", ".fonts",
			function() {
				if (jQuery(this).hasClass('fontsClickedHovered')) {
					jQuery(this).addClass('fontsClicked');
					jQuery(this).removeClass('fontsClickedHovered');
				}
				else {
					jQuery(this).addClass('fontsOriginal');
					jQuery(this).removeClass('fontsHovered');
				}
			}
		);
		var myCounter = 1;
		// jQuery(".myDate").datepicker();

		var max_fields      = 1000;
        if (jQuery.exists('#ecard_count')) {
            max_fields = jQuery('#ecard_count').val();
        }
		var wrapper         = jQuery(".ecards-recipients-from");
		var add_button      = jQuery(".add-persan");

		var n = 1;
		jQuery(add_button).click(function(e){

			e.preventDefault();
			if(n < max_fields){
                var today = new Date();
				n++;
				//var newDiv = jQuery('<div class="ecards-recipients-to formDiv person"><dl><dd>To: </dd><dt><input class="name-recipient" type="text" name="ecard_to[0][name]" placeholder="Recipient"><div class="validate-advice-block">Please enter recipient</div></dt> <dd>Email: </dd> <dt><input class="email-recipient" type="email" name="ecard_to[0][email]" placeholder="recipient@address.com"><div class="validate-advice-block">Please enter valid email</div></dt> <dd>Schedule Sending: </dd> <dt><input class="myDate date-delivery" type="text" name="ecard_to[0][date] size="10" value="'+(today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear()+'" placeholder="click" /></dt> </dl> <a href="#" class="close">x</a> </div>')

				var newDiv = jQuery('<div class="ecards-recipients-to formDiv person"><dl>' +
					'<dd>To: </dd> ' +
					'<dt><input class="name-recipient" type="text" name="ecard_to[0][name]" placeholder="Recipient">' +
					'<div class="validate-advice-block">Please enter recipient</div></dt> ' +
					'<dd>Email: </dd> ' +
					'<dt><input class="email-recipient" type="email" name="ecard_to[0][email]" placeholder="">' +
					'<div class="validate-advice-block">Please enter a valid email or telephone number</div></dt> ' +
					'<dd>Phone:  </dd> ' +
					'<dt><input id="phone-tags"  class="phone-recipient" type="text" name="ecard_to[0][phone]" placeholder="447971796595">' +
					'<div class="validate-advice-block validate-phoneStrict">Please enter a valid email or telephone number</div> </dt> ' +
					'<dd>Schedule: </dd> ' +
					'<dt><input class="myDate date-delivery" type="text" name="ecard_to[0][date] size="10" value="'+(today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear()+'" placeholder="click" /></dt> </dl> <a href="#" class="close">x</a> </div>')

				jQuery('.ecards-recipients .pink').before(newDiv);

				var i=0;
				jQuery('.ecards-recipients-to.formDiv.person .name-recipient').each(function() {
					jQuery(this).attr('name', 'ecard_to['+ i +'][name]');
					i++;
				});

				var x=0;
				jQuery('.ecards-recipients-to.formDiv.person .email-recipient').each(function() {
                    jQuery(this).attr('id', 'ecard_to_'+ x +'_email');
					jQuery(this).attr('name', 'ecard_to['+ x +'][email]');
					x++;
				});

				var y=0;
				jQuery('.ecards-recipients-to.formDiv.person .phone-recipient').each(function() {
					jQuery(this).attr('name', 'ecard_to['+ y +'][phone]');
					y++;
				});

				var z=1;
				jQuery('.ecards-recipients-to.formDiv.person .date-delivery').each(function() {
					jQuery(this).attr('name', 'ecard_to['+ z +'][date]');
					z++;
				});
			}
			var newDate  = jQuery('.myTemplate')
				.clone()
				.removeClass("myTemplate")
				.addClass("additionalDate")
				.show();

			myCounter++;
			jQuery('.additionalDate input[name=inputDate]').each(function(index) {
				jQuery(this).addClass("myDate");
				jQuery(this).attr("name", jQuery(this).attr("name") + myCounter);
			});

			jQuery(".myDate").on('focus', function(){
				var $this = jQuery(this);
				if(!$this.data('datepicker')) {
					$this.removeClass("hasDatepicker");
					$this.datepicker({minDate: 0});
					$this.datepicker("show");
				}
			});
		});

		jQuery(wrapper).on("click",".close", function(e){
			e.preventDefault();  jQuery(this).parent('div').remove(); n--;
		})


		var currentTab = 0;
		jQuery(function () {
			jQuery("#ecard-tabs").tabs({
				select: function (e, i) {
					currentTab = i.index;
				}
			});
		});

		jQuery("#btnNext").live("click", function () {
			var tabs = jQuery('#ecard-tabs').tabs();
			var c = jQuery('#ecard-tabs').tabs("length");
			currentTab = currentTab == (c - 1) ? currentTab : (currentTab + 1);
			tabs.tabs('select', currentTab);
			jQuery("#btnPrevious").show();
			if (currentTab == (c - 1)) {
				jQuery("#btnNext").hide();
			} else {
				jQuery("#btnNext").show();
			}

			if (jQuery('#ecards-fifth').is(':visible')) {
				jQuery('#ecards-fifth').addClass('active');
			}
			else{
				jQuery('#ecards-fifth').removeClass('active');
			}
			if (jQuery('#ecards-second').is(':visible')) {
				jQuery('.text-tab a').removeClass('disabled');
			}
			if (jQuery('#ecards-third').is(':visible')) {
				jQuery('.gift-tab a').removeClass('disabled');
			}
			if (jQuery('#ecards-fourth').is(':visible')) {
				jQuery('.recipient-tab a').removeClass('disabled');
			}
			if (jQuery('#ecards-fifth').is(':visible')) {
				jQuery('.preview-tab a').removeClass('disabled');
			}
			if (jQuery('#ecards-summary').is(':visible')) {
				jQuery('.summary-tab a').removeClass('disabled');
			}

		});
		jQuery(".ui-tabs .ui-tabs-nav li a").live("click", function () {
			var tabs = jQuery('#ecard-tabs').tabs();
			var c = jQuery('#ecard-tabs').tabs("length");

			tabs.tabs('select', currentTab);
			if (currentTab == (c - 1)) {
				jQuery("#btnNext").hide();
			} else {
				jQuery("#btnNext").show();
			}

		})
		jQuery("#btnPrevious").live("click", function () {
			var tabs = jQuery('#ecard-tabs').tabs();
			var c = jQuery('#ecard-tabs').tabs("length");
			currentTab = currentTab == 0 ? currentTab : (currentTab - 1);
			tabs.tabs('select', currentTab);
			if (currentTab == 0) {
				jQuery("#btnNext").show();
				jQuery("#btnPrevious").hide();
			}
			if (currentTab < (c - 1)) {
				jQuery("#btnNext").show();
			}

			if (jQuery('#ecards-fifth').is(':visible')) {
				jQuery('#ecards-fifth').addClass('active');
			}
			else{
				jQuery('#ecards-fifth').removeClass('active');
			}
		});


		var animStep = -1;
		jQuery("#previewButton,#previewDiv").click(function (event) {
			event.preventDefault(event);
			jQuery( "#previewButton" ).hide(500);
			jQuery( ".envelopeFrontSet" ).hide(500);
			jQuery( "#previewDiv" ).animate({
				height: '700px'

			},1000,function(){

				jQuery('.envDiv').addClass('envDivAnim0');
				jQuery('#cardDiv').addClass('envDivAnim0');
				animStep = 0;

				jQuery( '#envelopeMessage' ).delay(2500).animate({
					opacity:'1'
				});
			});
		});



		jQuery("#envelopeMessage a").click(function (event) {
			event.preventDefault(event);
		})

		jQuery( "#contentDiv,#evetiContentDiv p,#evetiContentDiv a,.block1,.eviteCardFrontDiv,#evetiContentDiv .eviteInsideSize2Opening").click(function(event){

			if(jQuery('#cardDiv').hasClass('open-card-block')){
				jQuery('#cardPreview').addClass("active");
				if (animStep == 0) {

					jQuery('#envelopeTopFrontAnimation').addClass('envelopeTopFrontAnimated1');
					jQuery('#envelopeTopBackAnimation').addClass('envelopeTopBackAnimated1');

					jQuery('#cardFrontDiv').addClass('cardFrontAnimated1');
					jQuery('#cardDiv').addClass('cardDivAnim1');

					animStep = 1;

					jQuery( '#cardDiv' ).animate({
						zIndex:'0'
					},2000,function(){
						jQuery(this).css('zIndex','1');
						jQuery('#open-text1').css('visibility', 'visible');
						jQuery('#open-text2').css('visibility', 'hidden');
                        jQuery('#open-text4').css('visibility', 'hidden');
					});
					jQuery('#open-text1').delay(3500).animate({
						opacity:'1'
					});
                    jQuery('#open-text8').delay(3500).animate({
                        opacity:'1'
                    });
				}

				else if (animStep == 3) {

                    var curentURL = jQuery(location).attr('href');
                    if(curentURL.indexOf('/ecards/') + 1) {
                        return;
                    }

					jQuery('#cardOpenLeftAnimation').css('visibility', 'hidden');
					jQuery('#cardOpenRightAnimation').css('visibility', 'hidden');
					jQuery('#cardFrontDiv').css('visibility', 'visible');
					jQuery('#open-text1').css('visibility', 'visible');
                    jQuery('#open-text8').css('visibility', 'visible');
                    jQuery('#open-text1.ecard-text').css('visibility', 'hidden');
					jQuery('#open-text2').css('visibility', 'hidden');
                    jQuery('#open-text4').css('visibility', 'hidden');

					jQuery('.booksSet').addClass('cardBackAnimated3')
						.removeClass('cardBackAnimated_close');
					jQuery('#cardOpenLeftAnimation').removeClass('cardOpenLeftAnimated_open');
                    jQuery('#image_card_open_left').removeClass('image_card_open_left_open');
					jQuery('#cardOpenRightAnimation').removeClass('cardOpenRightAnimated_close');
					jQuery('#cardFrontDiv').addClass('cardFrontAnimated3')
						.removeClass('cardFrontAnimated_close');


					jQuery('#cardDiv').addClass('cardDivAnim3')
						.removeClass('cardDivAnim_close');
					animStep = 4;
				}
				else if (animStep == 2) {

					jQuery('#cardFrontDiv').css('visibility', 'hidden');

					jQuery('#open-text1').css('visibility', 'hidden');
                    jQuery('#open-text8').css('visibility', 'hidden');
					jQuery('#open-text2').css('visibility', 'hidden');
                    jQuery('#open-text4').css('visibility', 'visible');
                    jQuery('#open-text4').delay(2000).animate({
						opacity:'1'
					});

					jQuery('#cardOpenRightAnimation').addClass('cardOpenRightAnimated_close');
					jQuery('.cardTextAnimation').addClass('cardOpenRightAnimated_close');
					jQuery('.evitesAnimation').addClass('cardOpenRightAnimated_close');
					jQuery('.booksSet').addClass('cardBackAnimated_close')
						.removeClass('cardBackAnimated_open');
					jQuery('#cardFrontDiv').removeClass('cardFrontAnimated_open');

					jQuery('#cardDiv').addClass('cardDivAnim_close')
						.removeClass('cardDivAnim_open');
					animStep = 3;
				}
				else if (animStep == 1 || animStep == 4) {
					jQuery('#open-text1').css('visibility', 'hidden');
                    jQuery('#open-text8').css('visibility', 'hidden');
                    jQuery('#open-text4').css('visibility', 'hidden');
					jQuery('#open-text2').css('visibility', 'visible');
					jQuery('#cardOpenLeftAnimation').css('visibility', 'visible');
					jQuery('#cardOpenRightAnimation').css('visibility', 'visible');
					jQuery('.cardTextAnimation').css('visibility', 'visible');
					jQuery('.evitesAnimation').css('visibility', 'visible');


					jQuery('#cardOpenLeftAnimation').addClass('cardOpenLeftAnimated_open')
						.removeClass('cardOpenLeftAnimated3');
                    jQuery('#image_card_open_left').addClass('image_card_open_left_open').removeClass('cardOpenLeftAnimated3');
					jQuery('.cardTextAnimation').removeClass('cardOpenRightAnimated_close');
					jQuery('.evitesAnimation').removeClass('cardOpenRightAnimated_close');
					jQuery('.booksSet').removeClass('cardBackAnimated3');
					jQuery('#cardFrontDiv').addClass('cardFrontAnimated_open')
						.removeClass('cardFrontAnimated3')
						.removeClass('cardFrontAnimated1');

					jQuery('#cardDiv').addClass('cardDivAnim_open')
						.removeClass('cardDivAnim3');
					animStep = 2;
					jQuery('#open-text2').delay(2000).animate({
						opacity:'1'
					});
				}
			}
			if(jQuery('#cardDiv').hasClass('single-card-block')){
				if (animStep == 0) {

					jQuery('#envelopeTopFrontAnimation').addClass('envelopeTopFrontAnimated1');
					jQuery('#envelopeTopBackAnimation').addClass('envelopeTopBackAnimated1');

					jQuery('#cardFrontDiv').addClass('cardFrontAnimated1');
					jQuery('#cardDiv').addClass('cardDivAnim1');
					jQuery('#open-text7').css('visibility', 'visible')
					jQuery( '#open-text7' ).delay(3500).animate({
						opacity:'1'
					});
                    jQuery('#open-text5').css('visibility', 'visible')
                    jQuery( '#open-text5' ).delay(3500).animate({
                        opacity:'1'
                    });

					animStep = 1;
					jQuery( '#cardDiv' ).animate({
						zIndex:'0'
					},2000,function(){
						jQuery(this).css('zIndex','1');
						jQuery('#open-text1').css('visibility', 'visible');
						jQuery('#open-text2').css('visibility', 'hidden');
					});
					jQuery('#open-text1').delay(3500).animate({
						opacity:'1'
					});
				}
				else if (animStep == 2) {
					jQuery('#open-text4').css('visibility', 'hidden')

                    jQuery('#open-text7.text-show').css('visibility', 'visible')
					jQuery('#open-text5').css('display', 'block')
						.css('font-size', '16px')
						.css('text-transform', 'uppercase')
						.css('color', '#8fa5c6')
						.css('visibility', 'visible');
                    jQuery('#open-text5').delay(2000).animate({
						opacity:'1'
					});
					jQuery('#cardFrontDiv').addClass('cardFrontAnimated3')
						.removeClass('cardFrontAnimated2');
					jQuery('.booksSet').addClass('cardBackAnimated3')
						.removeClass('cardBackAnimated2');

					animStep = 3;
				}
				else if (animStep == 1 || animStep == 3) {
					jQuery('#open-text7').css('visibility', 'visible')
                    jQuery('#open-text5').css('visibility', 'hidden')
                    jQuery('#open-text7.text-show').css('visibility', 'hidden')

                    jQuery('#open-text4').css('display', 'block')
						.css('font-size', '16px')
						.css('text-transform', 'uppercase')
						.css('color', '#8fa5c6')
						.css('visibility', 'visible');
                    jQuery('#open-text4').delay(2000).animate({
						opacity:'1'
					});

					jQuery('#cardFrontDiv').addClass('cardFrontAnimated2')
						.removeClass('cardFrontAnimated3');
					jQuery('.booksSet').addClass('cardBackAnimated2')
						.removeClass('cardBackAnimated3');

					animStep = 2;
				}
			}
		});
        previewTextResize();
        textEditorInteractions();
	});

    function previewTextResize() {
        if(jQuery.exists('.cardTextAnimation')) {
            var text = document.querySelector('.cardTextAnimation');
            var fontSizeOld = text.style["font-size"];
            var fontSizeNew = fontSizeOld.substr(0, fontSizeOld.length - 2);
            while (text.scrollHeight > text.clientHeight) {
                fontSizeNew = fontSizeNew - 1;
                text.style["font-size"] = fontSizeNew + 'px';
            }
        }
    }

	function textEditorInteractions() {
        if(jQuery.exists('#spinner')) {
            jQuery( "#spinner" ).spinner();
        }

        if (jQuery.exists('#color')) {
            jQuery('#color').colorPicker({showHexField: false});
        }

        if (jQuery.exists('#format')) {
            jQuery( "#format" ).buttonset();
        }

        if (jQuery.exists('#align')) {
            jQuery( "#align" ).buttonset();
        }

        if (jQuery.exists('#fit')) {
            jQuery( "#fit" ).button({
                text: false,
                icons: {
                    primary: "ui-icon-arrow-4-diag"
                }
            });
        }

        if (jQuery.exists('#cardText')) {
            jQuery('#cardText').val(cardText);
            jQuery('#cardText').css('font-size', fontSize);
            jQuery('#cardText').css("text-align", textAlign);
            jQuery('#cardText').css('font-family', fontFamily);
        }

        if (jQuery.exists('#spinner')) {
            jQuery( "#spinner" ).spinner( "value", parseInt(fontSizeButton) );
        }

		//jQuery( "#spinner" ).spinner( "value", parseInt(fontSizeButton) );

		/*, function() {
         jQuery(this).css('font-size', defaultFontSize);
         jQuery( "#spinner" ).spinner( "value", defaultFontSize );
		 alert(defaultFontSize);
		 //autoResizeText(jQuery('#cardText'));

         jQuery(window).resize(function() {
		 autoResizeText(jQuery('#cardText'));
		 });
		 });*/


		/*jQuery(window).resize(function() {
		 autoResizeText(jQuery('#cardText'));
		 });*/

        jQuery( "#spinner" ).keypress(function(event){
			if (event.which == 13) {
				console.log('keypress');
				//alert( jQuery( "#spinner" ).spinner( "value" ));
				//alert(jQuery('#cardText').attr("rows"));
				var fontSize = jQuery( "#spinner" ).spinner("value");
				if (fontSize <= 0) jQuery( "#spinner" ).spinner("value", "1")
                jQuery('#cardText').css('font-size',jQuery( "#spinner" ).spinner("value"));
				autoResizeText(jQuery('#cardText'));
			}
		});
        jQuery( "#spinnerDiv" ).click(function(){
			//alert( jQuery( "#spinner" ).spinner( "value" ));
			var fontSize = jQuery( "#spinner" ).spinner("value");
			if (fontSize <= 0) jQuery( "#spinner" ).spinner("value", "1")
            jQuery('#cardText').css('font-size',jQuery( "#spinner" ).spinner("value"));
			autoResizeText(jQuery('#cardText'));
		});

        jQuery('#color').change(function(){
            jQuery('#cardText').css("color",jQuery('#color').val());
		});
		/*jQuery( "#colorButton" ).click(function() {
         jQuery( "#color" ).trigger( "click" );
		 });*/
        jQuery( "#check1" ).click(function(){
			//alert(jQuery('#cardText').css("font-weight"));
			if (jQuery('#cardText').css("font-weight") == "bold" || jQuery('#cardText').css("font-weight") >= 600)
                jQuery('#cardText').css("font-weight","normal");
			else {
                jQuery('#cardText').css("font-weight","bold");
				autoResizeText(jQuery('#cardText'));
			}
		});

        jQuery( "#check2" ).click(function(){
			if (jQuery('#cardText').css("font-style") == "italic")
                jQuery('#cardText').css("font-style","normal");
			else jQuery('#cardText').css("font-style","italic");
		});
        jQuery( "#radio1" ).click(function(){
            jQuery('#cardText').css("text-align","left");
		});
        jQuery( "#radio2" ).click(function(){
            jQuery('#cardText').css("text-align","center");
		});
        jQuery( "#radio3" ).click(function(){
            jQuery('#cardText').css("text-align","right");
		});
        jQuery( "#radio4" ).click(function(){
            jQuery('#cardText').css("text-align","justify");
		});
        jQuery( "#fit" ).click(function(e) {
			e.preventDefault();
			jQuery('#cardText').css('font-size', '100px');
			autoResizeText(jQuery('#cardText'));
		});



        jQuery( "#Somebody" ).click(function(){
            jQuery('#cardText').css("fontFamily",jQuery(this).css("fontFamily"))
				.css('font-size', '20px')
				.css('font-weight', 'bold')
				.css("letter-spacing", "0");
		});
        jQuery( "#Tempus" ).click(function(){
            jQuery('#cardText').css("fontFamily",jQuery(this).css("fontFamily"))
				.css('font-size', '18px')
				.css('font-weight', 'bold')
				.css("letter-spacing", "0");
		});
        jQuery( "#Litos" ).click(function(){
            jQuery('#cardText').css("fontFamily",jQuery(this).css("fontFamily"))
				.css('font-size', '25px')
				.css('font-weight', 'bold')
				.css("letter-spacing", "0");
		});
        jQuery( "#Allura" ).click(function(){
            jQuery('#cardText').css("fontFamily",jQuery(this).css("fontFamily"))
				.css('font-size', '30px')
				.css('font-weight', 'bold')
				.css("letter-spacing", "0");
		});
		jQuery( "#Kgeyeswideopen" ).click(function(){
			jQuery('#cardText').css("fontFamily",jQuery(this).css("fontFamily"))
				.css('font-size', '26px')
				.css('font-weight', 'bold')
				.css("letter-spacing", "0");
		});
        jQuery( "#Baskerville" ).click(function(){
            jQuery('#cardText').css("fontFamily",jQuery(this).css("fontFamily"))
				.css('font-size', '18px')
				.css('font-weight', 'normal')
				.css("letter-spacing", "0");
		});
        jQuery( "#Myriad" ).click(function(){
            jQuery('#cardText').css("fontFamily",jQuery(this).css("fontFamily"))
				.css('font-size', '18px')
				.css('font-weight', 'normal')
				.css("letter-spacing", "0");
		});

        jQuery('#cardText').keypress(function(){
		//jQuery('#cardText').change(function(){
			while (jQuery(this).prop('scrollHeight') > jQuery(this).prop('clientHeight')) {
                jQuery(this).css('font-size', '-=1');
			}
		});
		jQuery('#cardText').bind('paste', function (){
			while (jQuery(this).prop('scrollHeight') > jQuery(this).prop('clientHeight')) {
				jQuery(this).css('font-size', '-=1');
			}
		});

        jQuery("#cardAppDiv").on("click", ".fonts", function(){
			autoResizeText(jQuery('#cardText'));
			if (jQuery(this).hasClass('fontsHovered')) {
                jQuery(this).addClass('fontsClickedHovered');
                jQuery(this).removeClass('fontsHovered');
                jQuery( ".fonts" ).each(function(){
					if (jQuery(this).hasClass('fontsClicked')) {
                        jQuery(this).addClass('fontsOriginal')
                        jQuery(this).removeClass('fontsClicked');
					}
				});
			}
		})
			.on("mouseenter", ".fonts",
			function(){
				if (jQuery(this).hasClass('fontsClicked')) {
                    jQuery(this).addClass('fontsClickedHovered');
                    jQuery(this).removeClass('fontsClicked');
				}
				else {
                    jQuery(this).addClass('fontsHovered');
                    jQuery(this).removeClass('fontsOriginal');
				}
			})

			.on("mouseleave", ".fonts",
			function() {
				if (jQuery(this).hasClass('fontsClickedHovered')) {
                    jQuery(this).addClass('fontsClicked');
                    jQuery(this).removeClass('fontsClickedHovered');
				}
				else {
                    jQuery(this).addClass('fontsOriginal');
                    jQuery(this).removeClass('fontsHovered');
				}
			}
		);

	}

    jQuery.exists = function(selector) {
        return (jQuery(selector).length > 0);
    }
})( jQuery );
jQuery(document).ready(function() {
    jQuery('.evite-radio-butoons input').click(function() {
        if(jQuery('#radioBtn2').is(':checked')) {
            jQuery('.evites-index-index .egift-image-block').addClass('active');
        }
        if(jQuery('#radioBtn1').is(':checked')) {
            jQuery('.evites-index-index .egift-image-block').removeClass('active');
        }
    });
});