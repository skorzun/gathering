<?php

class Magecom_Base_Model_Observer
{
    public function sendEmail()
    {
        Mage::helper('base')->sendEmail();
    }

    public function sendReminder()
    {
        Mage::helper('base')->sendReminder();
    }

    public function updatePrice(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('customer/session')->getCardProductId()) {
            $items = $observer->getCart()->getItems()->getItems();
            foreach ($items as $item) {
                $productId = $item->getProduct()->getId();
                if ($productId == Mage::getSingleton('customer/session')->getCardProductId()) {
                    $singlePrice = Mage::helper('base')->getSinglePrice($productId);
                    $item->setOriginalCustomPrice($singlePrice);
                    $item->save();
                    Mage::getSingleton('customer/session')->unsCardProductId();
                }
            }
        }



    }
}
