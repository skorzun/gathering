<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * QuickaccountController.php
 *
 * @category   CLS
 * @package    CustomerAdvanced
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

require_once Mage::getModuleDir('controllers', 'Mage_Customer') . DS . 'AccountController.php';

class CLS_CustomerAdvanced_Customer_QuickaccountController extends Mage_Customer_AccountController
{
    /**
     * Define target URL and redirect customer after logging in
     */
    protected function _loginPostRedirect()
    {
        $session = $this->_getSession();

        if (count($session->getMessages()->getErrors()) > 0) {
            $this->_redirectError(Mage::getUrl('customer/account/login'));
            return $this;
        }

        parent::_loginPostRedirect();
    }

    /**
     * Action predispatch
     */
    public function preDispatch()
    {
        if (($referer = $this->getRequest()->getParam('unencoded_referer'))
            && !empty($referer)) {
            $this->getRequest()->setParam('referer', Mage::helper('core')->urlEncode($referer));
        }
        parent::preDispatch();
    }

    public function postDispatch()
    {
        parent::postDispatch();

        // Add any customer session messages to the core session instead, because we don't know what page we'll end up on
        $customerSession = $this->_getSession();
        $coreSession = Mage::getSingleton('core/session');
        $messages = $customerSession->getMessages(true);
        foreach ($messages->getItems() as $message) {
            $coreSession->addMessage($message);
        }
    }
}