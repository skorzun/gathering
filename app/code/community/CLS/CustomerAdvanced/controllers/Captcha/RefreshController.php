<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * RefreshController.php
 *
 * @category   CLS
 * @package    CustomerAdvanced
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

require_once Mage::getModuleDir('controllers', 'Mage_Captcha') . DS . 'RefreshController.php';

class CLS_CustomerAdvanced_Captcha_RefreshController extends Mage_Captcha_RefreshController
{
    /**
     * Refreshes captcha and returns JSON encoded URL to image (AJAX action)
     * Example: {'imgSrc': 'http://example.com/media/captcha/67842gh187612ngf8s.png'}
     *
     * @return null
     */
    public function indexAction()
    {
        $formId = $this->getRequest()->getPost('formId');
        $formId = preg_replace('/'.Mage::helper('cls_customeradvanced/quickaccess')->getHtmlIdPrefix().'_/', '', $formId);
        $this->getRequest()->setPost('formId', $formId);

        return parent::indexAction();
    }
}
