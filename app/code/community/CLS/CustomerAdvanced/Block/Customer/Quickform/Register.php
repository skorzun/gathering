<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Register.php
 *
 * @category   CLS
 * @package    CustomerAdvanced
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_CustomerAdvanced_Block_Customer_Quickform_Register extends Mage_Customer_Block_Form_Register
{
    /**
     * Retrieve form posting url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return Mage::getUrl('customer/quickaccount/createpost');
    }

    /**
     * Determine the URL to redirect the customer to after they log in
     */
    public function getSuccessUrl()
    {
        $url = parent::getSuccessUrl();
        if (is_null($url) && !(bool)Mage::getStoreConfig(Mage_Customer_Model_Customer::XML_PATH_IS_CONFIRM)) {
            $url = Mage::helper('core/url')->getCurrentUrl();
        }
        return $url;
    }
}