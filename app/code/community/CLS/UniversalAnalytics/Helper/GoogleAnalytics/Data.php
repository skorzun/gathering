<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Data.php
 *
 * @category   CLS
 * @package    UniversalAnalytics
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_UniversalAnalytics_Helper_GoogleAnalytics_Data extends Mage_GoogleAnalytics_Helper_Data
{
    const CONFIG_PATH_GA_TYPE = 'google/analytics/type';

    /**
     * Whether Classic GA is ready to use
     *
     * @param mixed $store
     * @return bool
     */
    public function isGoogleAnalyticsAvailable($store = null)
    {
        return parent::isGoogleAnalyticsAvailable($store) && (Mage::getStoreConfig(self::CONFIG_PATH_GA_TYPE) == CLS_UniversalAnalytics_Helper_Data::GA_TYPE_CLASSIC);
    }

    /**
     * Whether Universal Analytics is ready to use
     *
     * @param mixed $store
     * @return bool
     */
    public function isUniversalAnalyticsAvailable($store = null)
    {
        return parent::isGoogleAnalyticsAvailable($store) && (Mage::getStoreConfig(self::CONFIG_PATH_GA_TYPE) == CLS_UniversalAnalytics_Helper_Data::GA_TYPE_UNIVERSAL);
    }

    /**
     * Whether GA IP Anonymization is enabled
     *
     * @param null $store
     * @return bool
     */
    public function isIpAnonymizationEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ANONYMIZATION, $store);
    }
}
