<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    UniversalAnalytics
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_UniversalAnalytics_Block_Googleanalytics_Ga_Universal extends Mage_GoogleAnalytics_Block_Ga
{
    protected $_dimensionsAndMetrics = array();

    /**
     * Return the basic per-page tracking code
     *
     * @param string $accountId
     * @return string
     */
    protected function _getPageTrackingCode($accountId)
    {
        return "
            ga('create', '{$this->jsQuoteEscape($accountId)}', 'auto');
            " . $this->_getAnonymizationCode();
    }

    /**
     * JS code dealing with anonymizing IP address
     *
     * @return string
     */
    protected function _getAnonymizationCode()
    {
        if (!method_exists(Mage::helper('googleanalytics'), 'isIpAnonymizationEnabled') || !Mage::helper('googleanalytics')->isIpAnonymizationEnabled()) {
            return '';
        }
        return "ga('set', 'anonymizeIp', 'true');";
    }

    /**
     * JS code dealing with tracking order data
     *
     * @return string
     */
    protected function _getOrdersTrackingCode()
    {
        $orderIds = $this->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        $result = array();
        $result[] = "ga('require', 'ecommerce', 'ecommerce.js');";

        foreach ($collection as $order) {
            $result[] = sprintf("
                ga('ecommerce:addTransaction', {
                    'id': '%s',
                    'affiliation': '%s',
                    'revenue': '%s',
                    'shipping': '%s',
                    'tax': '%s'
                });",
                $order->getIncrementId(),
                $this->jsQuoteEscape(Mage::app()->getStore()->getFrontendName()),
                $order->getBaseGrandTotal(),
                $order->getBaseShippingAmount(),
                $order->getBaseTaxAmount()
            );

            $itemCosts = 0;
            foreach ($order->getAllVisibleItems() as $item) {
                $itemCosts += $item->getBaseCost();
                $result[] = sprintf(
                    "ga('ecommerce:addItem', {
                        'id': '%s',
                        'name': '%s',
                        'sku': '%s',
                        'category': '%s',
                        'price': '%s',
                        'quantity': '%s'
                    });",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()),
                    null,
                    $item->getBasePrice(),
                    intval($item->getQtyOrdered())
                );
            }
            $result[] = "ga('ecommerce:send');";

            $this->_fillOrderCount($order);
        }
        return implode("\n", $result);
    }

    /**
     * Return dimension and metrics string for GA
     *
     * @return string
     */
    protected function _getDimensionsAndMetrics()
    {
        $product = Mage::registry('current_product');
        $this
            ->_fillPageType()
            ->_fillCustomerGroupCode()
            ->_fillCustomProductAttribute($product);

        $result = '';
        if (!empty($this->_dimensionsAndMetrics)) {
            $result = "\n";
            $result .= "ga('set', " . json_encode($this->_dimensionsAndMetrics) . ");";
            $result .= "\n";
        }
        return $result;
    }

    /**
     * Adds order count to $this->_dimensionsAndMetrics array
     *
     * @param Mage_Sales_Model_Order $order
     * @return CLS_UniversalAnalytics_Block_Googleanalytics_Ga
     */
    protected function _fillOrderCount($order)
    {
        $orderCount = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('customer_email', $order->getCustomerEmail())->count();
        $code = Mage::helper('cls_universalanalytics/config')->getOrderCountCode();
        if ($orderCount && $code) {
            $this->_dimensionsAndMetrics[$code] =  (string)$orderCount;
        }
        return $this;
    }

    /**
     * Adds customer group code to $this->_dimensionsAndMetrics array
     *
     * @return CLS_UniversalAnalytics_Block_Googleanalytics_Ga
     */
    protected function _fillCustomerGroupCode()
    {
        $customerGroupCodeMage = Mage::getModel('customer/group')->load(Mage::getSingleton('customer/session')->getCustomerGroupId())->getCode();
        $code = Mage::helper('cls_universalanalytics/config')->getCustomerGroupCode();
        if ($customerGroupCodeMage && $code) {
            $this->_dimensionsAndMetrics[$code] =  (string)$customerGroupCodeMage;
        }
        return $this;
    }

    /**
     * Adds page type to $this->_dimensionsAndMetrics array
     *
     * @return CLS_UniversalAnalytics_Block_Googleanalytics_Ga
     */
    protected function _fillPageType()
    {
        $pageType = null;

        $currentCategory = Mage::registry('current_category');
        $currentProduct = Mage::registry('current_product');
        if ($currentProduct && $currentProduct->getId()) {
            $pageType = $this->__('Product-%s', ucfirst($currentProduct->getTypeId()));
        } elseif ($currentCategory && $currentCategory->getId()) {
            $pageType = $this->__('Category');
        } elseif ($this->getRequest()->getModuleName() == 'cms') {
            $pageType = $this->__('CMS');
        } elseif (Mage::app()->getLayout()->getBlock('customer_account_navigation') !== false) {
            $pageType = $this->__('Customer Account');
        }

        $code = Mage::helper('cls_universalanalytics/config')->getPageTypeCode();
        if ($pageType && $code) {
            $this->_dimensionsAndMetrics[$code] = $pageType;
        }
        return $this;
    }

    /**
     * Adds customer product atrtibute to $this->_dimensionsAndMetrics array
     *
     * @param Mage_Catalog_Model_Product $product
     * @return CLS_UniversalAnalytics_Block_Googleanalytics_Ga
     */
    protected function _fillCustomProductAttribute($product)
    {
        $productAttributeCode = Mage::helper('cls_universalanalytics/config')->getCustomProductAttribute();
        $code = Mage::helper('cls_universalanalytics/config')->getCustomProductAttributeCode();
        if ($code && $productAttributeCode && $product && $product->getId()) {
            $attributeValue = $product->getData($productAttributeCode);
            if (!$attributeValue) {
                $attribute = $product->getResource()->getAttribute($code);
                if ($attribute) {
                    $attributeValue = $attribute->getFrontend()->getValue($productAttributeCode);
                }
            }
            if ($attributeValue) {
                $this->_dimensionsAndMetrics[$code] = $attributeValue;
            }
        }
        return $this;
    }

    /**
     * Returns UA account id
     *
     * @return string
     */
    protected function getAccountId()
    {
        return Mage::getStoreConfig(Mage_GoogleAnalytics_Helper_Data::XML_PATH_ACCOUNT);
    }

    /**
     * Render GA tracking scripts
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!Mage::helper('googleanalytics')->isUniversalAnalyticsAvailable()) {
            return '';
        }
        return Mage_Core_Block_Template::_toHtml();
    }
}
