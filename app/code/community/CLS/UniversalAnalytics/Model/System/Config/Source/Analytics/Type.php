<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Type.php
 *
 * @category   CLS
 * @package    UniversalAnalytics
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_UniversalAnalytics_Model_System_Config_Source_Analytics_Type
{
    public function toOptionArray()
    {
        $result = array(
            array('label' => Mage::helper('cls_universalanalytics')->__('Classic Analytics'), 'value' => CLS_UniversalAnalytics_Helper_Data::GA_TYPE_CLASSIC),
            array('label' => Mage::helper('cls_universalanalytics')->__('Universal Analytics'), 'value' => CLS_UniversalAnalytics_Helper_Data::GA_TYPE_UNIVERSAL),
        );

        return $result;
    }

}