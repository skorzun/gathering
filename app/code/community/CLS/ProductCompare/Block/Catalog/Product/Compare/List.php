<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    ProductCompare
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_ProductCompare_Block_Catalog_Product_Compare_List extends Mage_Catalog_Block_Product_Compare_List {

    /**
     * Retrieve Product Compare Attributes, but only ones with actual values for some products.
     *
     * @return array
     */
    public function getAttributes()
    {
        if (is_null($this->_attributes)) {
            $this->_attributes = $this->getItems()->getComparableAttributes();
            //In addition to just getting the attributes, make sure each attribute has at least one real value among the products.
            $validAttributes = array();
            foreach ($this->_attributes as $attribute) {
                foreach ($this->getItems() as $product) {
                    //If a value is found, add this to the list of valid attributes and move on.
                    if ((string) $product->getData($attribute->getAttributeCode()) != '') {
                        $validAttributes[] = $attribute;
                        continue 2;
                    }
                }
            }
            $this->_attributes = $validAttributes;
        }

        return $this->_attributes;
    }

}