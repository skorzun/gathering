<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    ProductCompare
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_ProductCompare_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * When change of product comparison occurs, update the custom cookie that tracks product IDs
     *
     * @param Varien_Event_Observer $observer
     */
    public function updateCustomCookie(Varien_Event_Observer $observer)
    {
        Mage::helper('catalog/product_compare')->calculate();

        $listItems = Mage::helper('catalog/product_compare')->getItemCollection();
        $ids = array();
        foreach ($listItems as $item) {
            $ids[] = $item->getId();
        }
        sort($ids);

        Mage::getSingleton('core/cookie')->set(CLS_ProductCompare_Helper_Data::COOKIE_COMPARE_LIST, implode(',', $ids), null, null, null, null, false);
    }
}