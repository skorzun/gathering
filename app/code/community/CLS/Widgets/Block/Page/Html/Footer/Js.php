<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    Widgets
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_Widgets_Block_Page_Html_Footer_Js extends CLS_LlamaCommerceCore_Block_Page_Html_Footer_Js
{
    protected function _toHtml()
    {
        // If the appropriate type of widget was never registered, don't output anything
        if ($this->hasData('register_check_type') && !Mage::helper('cls_widgets/block')->widgetTypeIsRegistered($this->getRegisterCheckType())) {
            return '';
        }

        // For each item, if it's already in the head block, remove it
        $headBlock = $this->getLayout()->getBlock('head');
        $headBlockItems = $headBlock->getItems();
        $thisItems = $this->getItems();
        if (!is_null($thisItems)) {
            foreach ($this->getItems() as $id=>$item) {
                if (isset($headBlockItems[$id])) {
                    unset($this->_data['items'][$id]);
                }
            }
        }

        return parent::_toHtml();
    }
}