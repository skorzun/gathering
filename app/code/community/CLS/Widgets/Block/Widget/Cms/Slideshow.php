<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    Widgets
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_Widgets_Block_Widget_Cms_Slideshow extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
    const DEFAULT_PREV_NEXT_TEXT = '<span class="slide-arrow"></span>';

    protected $_slideshowModel = null;

    public function __construct(array $args = array())
    {
        $this->_slideshowModel = Mage::getModel('cls_widgets/slideshow', array('block' => $this));
        parent::__construct($args);
    }

    /**
     * Register the existence of this widget
     *
     * @return Mage_Core_Block_Abstract|void
     */
    protected function _prepareLayout()
    {
        $helper = Mage::helper('cls_widgets/block');
        $helper->registerWidgetType(CLS_Widgets_Helper_Block::TYPE_WIDGET);
        $helper->registerWidgetType(CLS_Widgets_Helper_Block::TYPE_SLIDESHOW);
        return parent::_prepareLayout();
    }

    /**
     * Get the text to use for the "previous" link
     *
     * @return string
     */
    public function getPrevText()
    {
        return $this->_slideshowModel->getPrevText();
    }

    /**
     * Get the text to use for the "next" link
     *
     * @return string
     */
    public function getNextText()
    {
        return $this->_slideshowModel->getNextText();
    }

    /**
     * Retrieve converted to an array and filtered parameter "block_ids"
     *
     * @return array
     */
    public function getBlockIds()
    {
        if (!$this->_getData('block_ids')) {
            $this->setData('block_ids', array(0));
        } elseif (is_string($this->_getData('block_ids'))) {
            $blockIds = explode(',', $this->_getData('block_ids'));
            foreach ($blockIds as $key => $id) {
                $blockIds[$key] = (int)trim($id);
            }
            $this->setData('block_ids', $blockIds);
        }

        return $this->_getData('block_ids');
    }

    /**
     * Get array of the slides
     *
     * @return array
     */
    public function getSlidesContent()
    {
        $content = array();
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();

        $blockCollection = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(Mage::app()->getStore(), true)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('main_table.block_id', array('in' => $this->getBlockIds()));
        foreach ($blockCollection as $block) {
            $content[$block->getId()] = $processor->filter($block->getContent());
        }

        return $content;
    }
}