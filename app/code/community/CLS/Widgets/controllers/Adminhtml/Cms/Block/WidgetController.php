<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    Widgets
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Cms' . DS . 'Block' . DS . 'WidgetController.php';

class CLS_Widgets_Adminhtml_Cms_Block_WidgetController extends Mage_Adminhtml_Cms_Block_WidgetController
{
    /**
     * Chooser Source action
     */
    public function multichooserAction()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        $pagesGrid = $this->getLayout()->createBlock('cls_widgets/adminhtml_cms_block_widget_multichooser', '', array(
            'id' => $uniqId,
        ));
        $this->getResponse()->setBody($pagesGrid->toHtml());
    }
}
