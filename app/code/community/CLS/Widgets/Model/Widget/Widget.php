<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    Widgets
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_Widgets_Model_Widget_Widget extends Mage_Widget_Model_Widget
{
    /**
     * Return filtered list of widgets as SimpleXml object
     *
     * @param array $filters Key-value array of filters for widget node properties
     * @return Varien_Simplexml_Element
     */
    public function getWidgetsXml($filters = array())
    {
        $isEnterprise = (Mage::getEdition() == Mage::EDITION_ENTERPRISE);

        $widgets = parent::getWidgetsXml($filters);
        if (!$isEnterprise) {
            $result = clone $widgets;
            foreach ($widgets as $code => $widget) {
                $value = $widget->getAttribute(CLS_Widgets_Helper_Data::WIDGET_OPTION_ENTERPRISE);
                if ((bool) $value) {
                    unset($result->{$code});
                }
            }
            return $result;
        } else {
            return $widgets;
        }
    }
}