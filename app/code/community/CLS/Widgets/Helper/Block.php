<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    Widgets
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_Widgets_Helper_Block extends Mage_Core_Helper_Abstract
{
    const TYPE_WIDGET = 'widget';
    const TYPE_SLIDESHOW = 'slideshow';
    const TYPE_PRODUCT_CAROUSEL = 'product_carousel';

    protected $_registeredTypes = array();

    /**
     * Register the existence of a certain type of widget
     *
     * @param string $type
     */
    public function registerWidgetType($type)
    {
        $this->_registeredTypes[$type] = true;
    }

    /**
     * Determine if a type of widget has been registered on the page
     *
     * @param string $type
     * @return bool
     */
    public function widgetTypeIsRegistered($type)
    {
        return (isset($this->_registeredTypes[$type]));
    }
}