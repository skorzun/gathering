<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Swatch.php
 *
 * @category   CLS
 * @package    ConfigurableSwatches
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_ConfigurableSwatches_Block_Catalog_Layer_State_Swatch extends Mage_Core_Block_Template
{
    protected $_initDone = false;

    /**
     * Determine if we should use this block to render a state filter
     *
     * @param Mage_Catalog_Model_Layer_Filter_Item $filter
     * @return bool
     */
    public function shouldRender($filter)
    {
        $helper = Mage::helper('cls_configurableswatches');
        try {
            $attrModel = $filter->getFilter()->getAttributeModel();
        } catch (Exception $e) {
            // attribute model doesn't exist
            return false;
        }
        if ($helper->isEnabled() && $helper->attrIsSwatchType($attrModel)) {
            $this->_init($filter);
            if ($this->getSwatchUrl()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set one-time data on the renderer
     *
     * @param Mage_Catalog_Model_Layer_Filter_Item $filter
     */
    protected function _init($filter)
    {
        if (!$this->_initDone) {
            $dimHelper = Mage::helper('cls_configurableswatches/swatchdimensions');
            $this->setSwatchInnerWidth(
                $dimHelper->getInnerWidth(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_LAYER));
            $this->setSwatchInnerHeight(
                $dimHelper->getInnerHeight(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_LAYER));
            $this->setSwatchOuterWidth(
                $dimHelper->getOuterWidth(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_LAYER));
            $this->setSwatchOuterHeight(
                $dimHelper->getOuterHeight(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_LAYER));

            $swatchUrl = Mage::helper('cls_configurableswatches/productimg')
                ->getGlobalSwatchUrl(
                    $this->stripTags($filter->getLabel()),
                    $this->getSwatchInnerWidth(),
                    $this->getSwatchInnerHeight()
                );
            $this->setSwatchUrl($swatchUrl);

            $this->_initDone = true;
        }
    }
}
