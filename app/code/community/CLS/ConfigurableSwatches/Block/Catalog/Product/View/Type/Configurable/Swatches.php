<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Swatches.php
 *
 * @category   CLS
 * @package    ConfigurableSwatches
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_ConfigurableSwatches_Block_Catalog_Product_View_Type_Configurable_Swatches extends Mage_Core_Block_Template
{
    protected $_initDone = false;

    /**
     * Determine if the renderer should be used
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Catalog_Model_Product_Type_Configurable_Attribute $attribute
     * @param string $jsonConfig
     * @return bool
     */
    public function shouldRender($product, $attribute, $jsonConfig)
    {
        if (Mage::helper('cls_configurableswatches')->isEnabled()) {
            if (Mage::helper('cls_configurableswatches')->attrIsSwatchType($attribute->getProductAttribute())) {
                $this->_init($jsonConfig);
                return true;
            }
        }
        return false;
    }

    /**
     * Set one-time data on the renderer
     *
     * @param string $jsonConfig
     */
    protected function _init($jsonConfig)
    {
        if (!$this->_initDone) {
            $this->setJsonConfig($jsonConfig);

            $dimHelper = Mage::helper('cls_configurableswatches/swatchdimensions');
            $this->setSwatchInnerWidth(
                $dimHelper->getInnerWidth(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_DETAIL));
            $this->setSwatchInnerHeight(
                $dimHelper->getInnerHeight(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_DETAIL));
            $this->setSwatchOuterWidth(
                $dimHelper->getOuterWidth(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_DETAIL));
            $this->setSwatchOuterHeight(
                $dimHelper->getOuterHeight(CLS_ConfigurableSwatches_Helper_Swatchdimensions::AREA_DETAIL));

            $this->_initDone = true;
        }
    }
}
