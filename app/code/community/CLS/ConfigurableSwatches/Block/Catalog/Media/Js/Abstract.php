<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Abstract.php
 *
 * @category   CLS
 * @package    ConfigurableSwatches
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

abstract class CLS_ConfigurableSwatches_Block_Catalog_Media_Js_Abstract extends Mage_Core_Block_Template
{
    protected $_template = 'cls/configurableswatches/catalog/media/js.phtml';

    /**
     * Get target product IDs
     *
     * @return array
     */
    abstract public function getProducts();

    protected function _getJsImageFallbackString(array $imageFallback) {
        /* @var $coreHelper Mage_Core_Helper_Data */
        $coreHelper = Mage::helper('core');

        return $coreHelper->jsonEncode($imageFallback);
    }

    /**
     * Get image fallbacks by product as
     * array(product ID => array( product => product, image_fallback => image fallback ) )
     *
     * @return array
     */
    public function getProductImageFallbacks() {
        /* @var $helper CLS_ConfigurableSwatches_Helper_Mediafallback */
        $helper = Mage::helper('cls_configurableswatches/mediafallback');

        $fallbacks = array();

        $products = $this->getProducts();

        /* @var $product Mage_Catalog_Model_Product */
        foreach($products as $product) {
            $imageFallback = $helper->getConfigurableImagesFallbackArray($product);

            $fallbacks[$product->getId()] = array(
                'product' => $product,
                'image_fallback' => $this->_getJsImageFallbackString($imageFallback)
            );
        }

        return $fallbacks;
    }

    /**
     * Get image type to pass to configurable media image JS
     *
     * @return string
     */
    public function getImageType() {
        return parent::getImageType();
    }
}
