<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Helper for functionality dealing with product images
 *
 * @category   CLS
 * @package    ConfigurableSwatches
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_ConfigurableSwatches_Helper_Productimg extends Mage_Core_Helper_Abstract
{   
    /**
     * This array stores product images and separates them:
     * One group keyed by labels that match attribute values, another for all other images
     *
     * @var array
     */
    protected $_productImagesByLabel = array();

    const SWATCH_LABEL_SUFFIX = '-swatch';
    const SWATCH_FALLBACK_MEDIA_DIR = 'wysiwyg/swatches';
    const SWATCH_FILE_EXT = '.png';

    const MEDIA_IMAGE_TYPE_BASE = 'product_base_image';
    const MEDIA_IMAGE_TYPE_THUMBNAIL = 'product_thumbnail_image';

    const MEDIA_THUMBNAIL_IMAGE_SIZE = 300;
    const MEDIA_BASE_IMAGE_SIZE = 1200;


    /**
     * Determine if the passed text matches the label of any of the passed product's images
     *
     * @param string $text 
     * @param Mage_Catalog_Model_Product $product
     * @param string $type
     * @return Varien_Object|null
     */
    public function getProductImgByLabel($text, $product, $type=null)
    {
        $this->indexProductImages($product);
        
        //Get the product's image array and prepare the text
        $images = $this->_productImagesByLabel[$product->getId()];
        $text = trim(strtolower($text));

        $resultImages = array(
            'standard' => isset($images[$text]) ? $images[$text] : null,
            'swatch' => isset($images[$text . self::SWATCH_LABEL_SUFFIX]) ? $images[$text . self::SWATCH_LABEL_SUFFIX]
                    : null,
        );

        if (!is_null($type) && $resultImages[$type]) {
            $image = $resultImages[$type];
        } else {
            $image = (!is_null($resultImages['swatch'])) ? $resultImages['swatch'] : $resultImages['standard'];
        }
        
        return $image;
    }
    
    /**
     * Create the separated index of product images
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $optionValues
     * @return CLS_ConfigurableSwatches_Helper_Data
     */
    public function indexProductImages($product, $preValues=null)
    {
        if (!isset($this->_productImagesByLabel[$product->getId()])) {
            $images = array();

            // Is product configurable?
            $isConfigurable = ($product->getTypeID() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE);

            if ($isConfigurable) {
                $attrValues = array();
                $swatchAttrValues = array();

                if (!is_null($preValues) && is_array($preValues)) { // If a pre-defined list of valid values was passed
                    foreach ($preValues as $value) {
                        $attrValues[] = strtolower($value);
                        $swatchAttrValues[] = strtolower($value) . self::SWATCH_LABEL_SUFFIX;
                    }
                } else { // . . . otherwise we get them from all config attributes
                    // Get product's attributes
                    $attrs = $product->getTypeInstance(true)->getConfigurableAttributes($product);

                    // Collect valid values of image type attributes
                    $attrValues = array();
                    foreach ($attrs as $attr) {
                        if (Mage::helper('cls_configurableswatches')->attrIsSwatchType($attr->getAttributeId())) {
                            // getPrices returns info on the attributes's individual options
                            foreach ($attr->getPrices() as $option) {
                                $attrValues[] = strtolower($option['label']);
                                $swatchAttrValues[] = strtolower($option['label']) . self::SWATCH_LABEL_SUFFIX;
                            }
                        }
                    }
                }

                //Loop through the product's images and add them to the array
                if($galleryImages = $product->getMediaGalleryImages()) {
                    foreach ($galleryImages as $image) {
                        $label = trim(strtolower($image->getLabel()));
                        if ($isConfigurable && !empty($label)
                            && (in_array($label, $swatchAttrValues) || in_array($label, $attrValues)) ) {
                            // If this is a configurable product and the image label matches one of the attribute values
                            $images[$label] = $image;
                        }
                    }
                }
            }
            
            $this->_productImagesByLabel[$product->getId()] = $images;
        }
    }

    /**
     * Return the appropriate swatch URL for the given value (matches against product's image labels)
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $value
     * @param int $width
     * @param int @height
     * @param $swatchType
     * @param string $fallbackFileExt
     * @return string
     */
    public function getSwatchUrl($product, $value, $width = 21, $height = 21, &$swatchType, $fallbackFileExt=null)
    {
        $url = '';

        // Get the (potential) swatch image that matches the value
        $image = $this->getProductImgByLabel($value, $product, 'swatch');

        // Check in swatch directory if $image is null
        if (is_null($image)) {
            // Check if file exists in fallback directory
            $fallbackUrl = $this->getGlobalSwatchUrl($value, $width, $height, $fallbackFileExt);
            if (!empty($fallbackUrl)) {
                $url = $fallbackUrl;
                $swatchType = 'media';
            }
        }

        // If we still don't have a URL or matching product image, look for one that matches just
        // the label (not specifically the swatch suffix)
        if (empty($url) && is_null($image)) {
            $image = $this->getProductImgByLabel($value, $product, 'standard');
        }

        if (!is_null($image)) {
            $url = Mage::helper('catalog/image')
                ->init($product, 'swatch', $image->getFile())
                ->keepAspectRatio(true)
                ->resize($width, $height)
                ->__toString();
            $swatchType = 'product';
        } else {
            $swatchType = 'none';
        }

        return $url;
    }

    /**
     * Return URL for a matching swatch image from the global directory
     *
     * @param string $value
     * @param int $width
     * @param int $height
     * @param string $fileExt
     * @return string
     */
    public function getGlobalSwatchUrl($value, $width = 21, $height = 21, $fileExt=null)
    {
        if (is_null($fileExt)) {
            $fileExt = self::SWATCH_FILE_EXT;
        }

        $filename = Mage::helper('cls_configurableswatches')->getHyphenatedString($value) . $fileExt;

        // Form full path to where we want to cache resized version
        $destPathArr = array(
            self::SWATCH_FALLBACK_MEDIA_DIR,
            'cache',
            Mage::app()->getStore()->getId(),
            $width.'x'.$height,
            $filename,
        );

        $destPath = implode('/', $destPathArr);

        // Check if cached image exists already
        if (!file_exists(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/' . $destPath)) {
            // Check for source image
            $sourceFilePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA)
                . '/' . self::SWATCH_FALLBACK_MEDIA_DIR . '/' . $filename;
            if (!file_exists($sourceFilePath)) {
                return '';
            }

            // Do resize and save
            $processor = new Varien_Image($sourceFilePath);
            $processor->resize($width, $height);
            $processor->save(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/' . $destPath);
        }

        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $destPath;
    }


    /**
     * Determine whether to show an image in the product media gallery
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Varien_Object $image
     * @return bool
     */
    public function filterImageInGallery($product, $image)
    {
        if (!Mage::helper('cls_configurableswatches')->isEnabled()) {
            return true;
        }

        return is_null($this->getProductImgByLabel($image->getLabel(), $product));
    }
}