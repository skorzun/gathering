<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Mediafallback.php
 *
 * @category    CLS
 * @package     ConfigurableSwatches
 * @copyright   Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license     http://www.llamacommerce.com/license
 */

class CLS_ConfigurableSwatches_Helper_Mediafallback extends Mage_Core_Helper_Abstract
{
    const MEDIA_GALLERY_ATTRIBUTE_CODE = 'media_gallery';

    /**
     * Set child_attribute_label_mapping on products with attribute label -> product mapping
     * Depends on following product data:
     * - product must have children products attached
     *
     * @param array $parentProducts
     * @param $storeId
     */
    public function attachConfigurableProductChildrenAttributeMapping(array $parentProducts, $storeId) {
        $listSwatchAttr = Mage::helper('cls_configurableswatches/productlist')->getSwatchAttribute();

        $parentProductIds = array();
        /* @var $parentProduct Mage_Catalog_Model_Product */
        foreach($parentProducts as $parentProduct) {
            $parentProductIds[] = $parentProduct->getId();
        }

        $attributeLabels = array();

        $attributeCollection =
            Mage::getResourceModel('cls_configurableswatches/catalog_product_attribute_super_collection');
        $attributeCollection->addParentProductsFilter($parentProductIds);
        $attributeCollection->attachEavAttributes();
        $attributeCollection->setStoreId($storeId);
        $attributeCollection->attachAttributeLabels();

        /* @var $attribute Mage_Catalog_Model_Product_Type_Configurable_Attribute */
        foreach($attributeCollection as $attribute) {
            $labelsRaw = explode(',', $attribute->getAttributeLabels());
            foreach($labelsRaw as $labelRaw) {
                list($storeId, $optionId, $label) = explode('|', $labelRaw);

                if(!isset($attributeLabels[$optionId])) {
                    $attributeLabels[$optionId] = array();
                }
                $attributeLabels[$optionId][$storeId] = $label;
            }
        }

        /* @var $parentProduct Mage_Catalog_Model_Product */
        foreach($parentProducts as $parentProduct) {
            $mapping = array();
            $listSwatchValues = array();

            /* @var $attribute Mage_Catalog_Model_Product_Type_Configurable_Attribute */
            foreach($attributeCollection as $attribute) {
                /* @var $childProduct Mage_Catalog_Model_Product */
                if(!is_array($parentProduct->getChildrenProducts())) {
                    continue;
                }

                foreach($parentProduct->getChildrenProducts() as $childProduct) {
                    $attributeValue = $childProduct->getData($attribute->getAttributeCode());

                    if(empty($attributeValue)) {
                        continue;
                    }

                    $attributeLabel = null;
                    if(!isset($attributeLabels[$attributeValue])) { //no label at all
                        continue; //skip
                    }
                    if(isset($attributeLabels[$attributeValue][$storeId])) { //prefer store-specific value
                        $attributeLabel = $attributeLabels[$attributeValue][$storeId];
                    } else if(isset($attributeLabels[$attributeValue][0])) { //fall back to default value
                        $attributeLabel = $attributeLabels[$attributeValue][0];
                    }

                    if(!isset($mapping[$attributeLabel])) {
                        $mapping[$attributeLabel] = array();
                    }
                    $mapping[$attributeLabel][] = $childProduct->getId();
                    $mapping[$attributeLabel] = array_unique($mapping[$attributeLabel]);

                    if ($attribute->getAttributeId() == $listSwatchAttr->getAttributeId()
                        && !in_array($attributeLabel, $listSwatchValues)) {
                        $listSwatchValues[$attributeValue] = $attributeLabel;
                    }
                } //end looping child products
            } //end looping attributes

            $parentProduct->setChildAttributeLabelMapping($mapping)
                ->setListSwatchAttrValues($listSwatchValues);
        } //end looping parent products
    }

    /**
     * Get resized media image URL
     *
     * @param Mage_Catalog_Model_Product
     * @param string $mediaImageFile
     * @param string $attrName
     * @param string $size
     * @return string
     */
    protected function _getImageUrl($product, $mediaImageFile, $attrName, $size) {
        if(empty($mediaImageFile) || $mediaImageFile == 'no_selection') {
            return null;
        }
        return (string) Mage::helper('catalog/image')->init($product, $attrName, $mediaImageFile)
            ->keepFrame(true)->resize($size);
    }

    /**
     * For given product, get configurable images fallback array
     * Depends on following data available on product:
     * - product must have child attribute label mapping attached
     * - product must have media gallery attached which attaches and differentiates local images and child images
     * - product must have child products attached
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getConfigurableImagesFallbackArray(Mage_Catalog_Model_Product $product) {
        if(!$product->hasConfigurableImagesFallbackArray()) {
            Varien_Profiler::start(__CLASS__ . '::' . __FUNCTION__);

            $mapping = $product->getChildAttributeLabelMapping();

            $mediaGallery = $product->getMediaGallery();

            if(!isset($mediaGallery['images']) || !isset($mediaGallery['configurable_images'])) {
                return array(); //nothing to do here
            }

            $imagesByLabel = array();

            foreach($mapping as $label => $productIds) {
                $configurableProductImagePath = null;
                foreach($mediaGallery['images'] as $galleryImage) {
                    if($galleryImage['label'] == $label) {
                        $configurableProductImagePath = $galleryImage['file'];
                        break;
                    }
                }

                $imagesByLabel[$label] = array(
                    'configurable_product' => array(
                        CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_THUMBNAIL =>
                            $this->_getImageUrl(
                                $product,
                                $configurableProductImagePath,
                                'small_image',
                                CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_THUMBNAIL_IMAGE_SIZE
                            ),
                        CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE =>
                            $this->_getImageUrl(
                                $product,
                                $configurableProductImagePath,
                                'image',
                                CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_BASE_IMAGE_SIZE
                            ),
                    ),
                    'products' => $productIds
                );
            }

            $thumbnailImages = array();
            $baseImages = array();
            /* @var $childProduct Mage_Catalog_Model_Product */
            if ($product->hasChildrenProducts()) {
                foreach($product->getChildrenProducts() as $childProduct) {
                    $thumbnailImages[$childProduct->getId()] = (string)Mage::helper('catalog/image')
                        ->init($childProduct, 'small_image')
                        ->keepFrame(true)
                        ->resize(CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_THUMBNAIL_IMAGE_SIZE);
                    $baseImages[$childProduct->getId()] = (string)Mage::helper('catalog/image')
                        ->init($childProduct, 'image')
                        ->keepFrame(true)
                        ->resize(CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_BASE_IMAGE_SIZE);
                }
            }


            $array = array(
                'option_labels' => $imagesByLabel,
                CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_THUMBNAIL => $thumbnailImages,
                CLS_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE => $baseImages
            );

            $product->setConfigurableImagesFallbackArray($array);

            Varien_Profiler::stop(__CLASS__ . '::' . __FUNCTION__);
        }

        return $product->getConfigurableImagesFallbackArray();
    }

    /**
     * Groups media gallery images by local images and child images
     *
     * @param Mage_Catalog_Model_Product $product
     */
    public function groupMediaGalleryImages(Mage_Catalog_Model_Product $product) {
        $mediaGallery = $product->getMediaGallery();

        if(!isset($mediaGallery['images'])) { //hmmm ... no media gallery images
            return; //nothing to do here
        }

        $newMediaGalleryImages = array();
        $configurableImages = array();

        foreach($mediaGallery['images'] as $mediaGalleryImage) {
            if($mediaGalleryImage['product_id'] == $product->getId()) {
                $newMediaGalleryImages[] = $mediaGalleryImage;
            } else {
                $configurableImages[] = $mediaGalleryImage;
            }
        }

        $mediaGallery['images'] = $newMediaGalleryImages;
        $mediaGallery['configurable_images'] = $configurableImages;

        $product->setMediaGallery($mediaGallery); //reset product media images based on new grouping
    }

    /**
     * For given product set, attach media_gallery attribute values.
     *
     * @param array $products
     * @param int $storeId
     */
    public function attachGallerySetToCollection(array $products, $storeId) {
        $productIds = array();
        /* @var $product Mage_Catalog_Model_Product */
        foreach($products as $product) {
            $productIds[] = $product->getId();
            if(!is_array($product->getChildrenProducts())) {
                continue;
            }
            /* @var $childProduct Mage_Catalog_Model_Product */
            foreach($product->getChildrenProducts() as $childProduct) {
                $productIds[] = $childProduct->getId();
            }
        }

        $attrCode = self::MEDIA_GALLERY_ATTRIBUTE_CODE;

        /* @var $resourceModel CLS_ConfigurableSwatches_Model_Resource_Catalog_Product_Attribute_Backend_Media */
        $resourceModel = Mage::getResourceModel('cls_configurableswatches/catalog_product_attribute_backend_media');

        $images = $resourceModel->loadGallerySet($productIds, $storeId);

        $relationship = array();
        foreach($products as $product) {
            $relationship[$product->getId()] = $product->getId();

            if(!is_array($product->getChildrenProducts())) {
                continue;
            }

            /* @var $childProduct Mage_Catalog_Model_Product */
            foreach($product->getChildrenProducts() as $childProduct) {
                $relationship[$childProduct->getId()] = $product->getId();
            }
        }

        foreach($images as $image) {
            $productId = $image['product_id'];
            $realProductId = $relationship[$productId];
            $product = $products[$realProductId];

            if (is_null($image['label'])) {
                $image['label'] = $image['label_default'];
            }
            if (is_null($image['position'])) {
                $image['position'] = $image['position_default'];
            }
            if (is_null($image['disabled'])) {
                $image['disabled'] = $image['disabled_default'];
            }

            $value = $product->getData($attrCode);
            if(!$value) {
                $value = array(
                    'images' => array(),
                    'value' => array()
                );
            }

            $value['images'][] = $image;

            $product->setData($attrCode, $value);
        }
    }

    /**
     * Attaches children product to each product via
     * ->setChildrenProducts()
     *
     * @param array $products
     * @param int $storeId
     */
    public function attachChildrenProducts(array $products, $storeId) {
        $productIds = array();
        /* @var $product Mage_Catalog_Model_Product */
        foreach($products as $product) {
            $productIds[] = $product->getId();
        }

        $collection = Mage::getResourceModel(
            'cls_configurableswatches/catalog_product_type_configurable_product_collection');

        $collection->setFlag('require_stock_items', true)
            ->setFlag('product_children', true)
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('*');
        $collection->addProductSetFilter($productIds);

        $collection->load();

        $mapping = array();
        /* @var $childProduct Mage_Catalog_Model_Product */
        foreach($collection as $childProduct) {
            foreach ($childProduct->getParentIds() as $parentId) {
                if(!isset($mapping[$parentId])) {
                    $mapping[$parentId] = array();
                }
                $mapping[$parentId][] = $childProduct;
            }
        }

        foreach($mapping as $parentId => $childrenProducts) {
            $products[$parentId]->setChildrenProducts($childrenProducts);
        }
    }
}
