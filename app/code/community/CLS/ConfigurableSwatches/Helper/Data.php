<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Data helper for configurable swatches
 *
 * @category   CLS
 * @package    ConfigurableSwatches
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_ConfigurableSwatches_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CONFIG_PATH_BASE = 'configswatches';
    const CONFIG_PATH_ENABLED = 'configswatches/general/enabled';
    const CONFIG_PATH_SWATCH_ATTRIBUTES = 'configswatches/general/swatch_attributes';
    const CONFIG_PATH_LIST_SWATCH_ATTRIBUTE = 'configswatches/general/product_list_attribute';


    protected $_enabled = null;
    protected $_configAttributeIds = null;

    /**
     * Is the extension enabled?
     *
     * @return bool
     */
    public function isEnabled()
    {
        if (is_null($this->_enabled)) {
            $this->_enabled = (bool) Mage::getStoreConfig(self::CONFIG_PATH_ENABLED);
        }
        return $this->_enabled;
    }

    /**
     * return the formatted hyphenated string
     *
     * @var $str string
     * @return string
     **/
    public function getHyphenatedString($str)
    {
        return strtolower(preg_replace('/([^A-Za-z0-9]+)/','-', $str));
    }

    /**
     * Get list of attributes that should use swatches
     *
     * return array
     */
    public function getSwatchAttributeIds()
    {
        if (is_null($this->_configAttributeIds)) {
            $this->_configAttributeIds = explode(',', Mage::getStoreConfig(self::CONFIG_PATH_SWATCH_ATTRIBUTES));
        }
        return $this->_configAttributeIds;
    }

    /**
     * Determine if an attribute should be a swatch
     *
     * @param int|Mage_Eav_Model_Attribute $attr
     * @return bool
     */
    public function attrIsSwatchType($attr)
    {
        if ($attr instanceof Varien_Object) {
            $attr = $attr->getId();
        }
        $configAttrs = $this->getSwatchAttributeIds();
        return in_array($attr, $configAttrs);
    }
}