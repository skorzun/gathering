## Configurable Swatches

This extension implements image swatches to represent configurable attributes.

For specified attributes . . .

* Swatches are used instead of the native dropdown for option selection on a configurable product detail page.
* Swatches can be shown in product listings.
* Swatches are shown in layered navigation instead of the native text links.

### Configuration

After installation, there will be a new section in System Configuration called Configurable Swatches.  Following are the available options.  (Only attributes with type "Dropdown" and "Use to Create Configurable Product" set to "Yes" can be chosen as swatch attributes.)

* Enabled - Whether the extension is enabled
* Product Attributes to Show as Swatches in Product Detail - All selected attributes will use swatches for their interface instead of the native dropdown on the product detail page. Note that a text "swatch" will be shown even if there is no corresponding image.
* Product Attribute to Use for Swatches in Product Listing - Only one attribute can be displayed as a series of swatches for each product on a listing page.
* Swatch Dimensions on . . . - Each group of dimensions defines the inner width and height (not including border) that swatch images will be re-sized to on the respective pages

### Usage

Rather than having a specific admin interface for managing swatch images, this extension relies on naming conventions to make use of standard images that are attached to products or located in the filesystem. Below are instructions for how to place/configure your images correctly to utilize swatch functionality.

#### Swatch Images

There's a three-level fallback for where the extension looks for the image to use as the swatch for a particular configurable option. In all cases, the front-end label of the specific option is what is used to try and match an image name/label.

1. If there is an image attached to the configurable product with label matching the option label plus "-swatch", that image will be used.  (Ex: If the option label is "Light Green" and there is an image labeled "Light Green-swatch" on the config product, that image will be used.)
2. If no image matches the first condition, the extension will next look in media/wysiwyg/swatches for a PNG matching the option label, all lowercase with hyphens in place of non-alphanumeric characters. (Ex: If the option label is "Light Green" and media/wysiwyg/swatches/light-green.png exists, that image will be used.)
3. If no image matches the first conditions, the final fallback is an image attached to the configurable product with label matching the option label exactly. (Ex: If the option label is "Light Green" and there is an image labeled "Light Green" on the config product, that image will be used.) The reason for the distinction between this and the first condition deals with the image swap functionality described below.

Note that, for images attached to the product to be found and used as a swatch, they must be included in the media gallery (i.e., not have the "exclude" checkbox checked). Nevertheless, any image being used for a swatch will not actually be shown in the media gallery.

#### Main Image Swap

The extension dynamically changes the main product image on the page when a configurable option is selected. (So when a swatch is clicked, the main image will change if an appropriate matching image exists.)

There are two ways of attaching images that should be used for the main image switch:  The image can either be attached to the configurable product with a label exactly matching the specific option label (ex. "Light Green"), or the image can be attached to the appropriate child products as the "base" image.

Note that, because of the way the _swatch_ image is searched for, this means there are three ways of managing the relationship between the swatch image and the main image (if you are specifying swatch images on a per-product basis instead of using the media directory fallback):

1. If you want the swatch to simply be a smaller version of the image that loads in the main image area when the swatch is clicked, simply attach a single image to the config product with the right label. (Ex: Having only an image labeled "Light Green" attached to the configurable product will result in the same image being used for the Light Green swatch and for the main image when Light Green is selected.)
2. If you want a different swatch image than the main image, attach two separate ones to the config product with the appropriate labels (ex. "Light Green" and "Light Green-swatch").
3. If you prefer to manage the main image on the child products, attach a "-swatch" image to the config product, and attach an image to each child product set as the "base" image. (Ex. Attach image labeled "Light Green-swatch" to the config product, and make sure an appropriate "base" image is attached to each child product with "Light Green" as its attribute value.)

A final note: Just as the Configurable Swatches extension prevents images used as swatches from actually showing in the product's media gallery, it does the same for the "swap" images.  (Ex. If you have both "Light Green" and "Light Green-swatch" images attached to the configurable product, neither will be shown in the media gallery.)