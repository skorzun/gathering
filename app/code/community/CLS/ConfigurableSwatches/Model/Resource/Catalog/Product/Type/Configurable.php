<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Configurable.php
 *
 * @category    CLS
 * @package     ConfigurableSwatches
 * @copyright   Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license     http://www.llamacommerce.com/license
 */

class CLS_ConfigurableSwatches_Model_Resource_Catalog_Product_Type_Configurable
    extends Mage_Catalog_Model_Resource_Product_Type_Configurable
{

    /**
     * Retrieve Required children ids
     * Grouped by parent id.
     *
     * @param mixed $parentId may be array of integers or scalar integer.
     * @param bool $required
     * @return array
     * @see Mage_Catalog_Model_Resource_Product_Type_Configurable::getChildrenIds()
     */
    public function getChildrenIds($parentId, $required = true) {
        if (is_array($parentId)) {
            $childrenIds = array();
            if(!empty($parentId)) {
                $select = $this->_getReadAdapter()->select()
                    ->from(array('l' => $this->getMainTable()), array('product_id', 'parent_id'))
                    ->join(
                        array('e' => $this->getTable('catalog/product')),
                        'e.entity_id = l.product_id AND e.required_options = 0',
                        array()
                    )
                    ->where('parent_id IN (?)', $parentId);

                foreach ($this->_getReadAdapter()->fetchAll($select) as $row) {
                    $childrenIds[$row['parent_id']][$row['product_id']] = $row['product_id'];
                }
            }
            return $childrenIds;
        } else {
            return parent::getChildrenIds($parentId, $required);
        }
    }
}
