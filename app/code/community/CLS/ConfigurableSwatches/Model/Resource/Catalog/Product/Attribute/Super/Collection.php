<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Collection.php
 *
 * @category   CLS
 * @package    ConfigurableSwatches
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_ConfigurableSwatches_Model_Resource_Catalog_Product_Attribute_Super_Collection
    extends Mage_Catalog_Model_Resource_Product_Type_Configurable_Attribute_Collection
{
    private $_eavAttributesJoined = false;
    private $_labelsJoined = false;
    private $_storeId = null;

    /**
     * Filter parent products to these IDs
     *
     * @param array $parentProductIds
     */
    public function addParentProductsFilter(array $parentProductIds) {
        $this->addFieldToFilter('product_id', array('in' => $parentProductIds));
    }

    /**
     * Attach (join) info from eav_attribute table
     */
    public function attachEavAttributes() {
        if($this->_eavAttributesJoined) {
            return;
        }

        $this->join(
            array('eav_attributes' => 'eav/attribute'),
            '`eav_attributes`.`attribute_id` = `main_table`.`attribute_id`'
        );

        $this->_eavAttributesJoined = true;
    }

    /**
     * Set store ID
     *
     * @param $storeId
     */
    public function setStoreId($storeId) {
        $this->_storeId = $storeId;
    }

    /**
     * Retrieve Store Id
     *
     * @return int
     */
    public function getStoreId()
    {
        return (int)$this->_storeId;
    }

    /**
     * Attach attribute labels. Each row will have a
     * comma separated list of labels as such:
     * store ID | option ID | label
     */
    public function attachAttributeLabels() {
        if($this->_labelsJoined) {
            return;
        }

        $this->getSelect()->join(
            array('options' => $this->getTable('eav/attribute_option')),
            '`options`.`attribute_id` = `main_table`.`attribute_id`',
            array()
        )->join(
            array('option_labels' => $this->getTable('eav/attribute_option_value')),
            '`option_labels`.`option_id` = `options`.`option_id`',
            array('attribute_labels' => new Zend_Db_Expr(
                    "group_concat(distinct concat("
                    . "`option_labels`.`store_id`, '|', `option_labels`.`option_id`, '|', `option_labels`.`value`))"
                )
            )
        )->group('main_table.product_super_attribute_id');

        $this->_labelsJoined = true;
    }

    /**
     * Bypass parent _afterLoad() -- parent depends on single product context
     *
     * @return Mage_Catalog_Model_Resource_Product_Type_Configurable_Attribute_Collection
     */
    protected function _afterLoad()
    {
        Mage_Core_Model_Resource_Db_Collection_Abstract::_afterLoad();
        return $this;
    }
}
