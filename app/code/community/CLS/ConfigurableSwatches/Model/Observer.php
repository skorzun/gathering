<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    ConfigurableSwatches
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_ConfigurableSwatches_Model_Observer extends Mage_Core_Model_Abstract
{
    public function productListCollectionLoadAfter(Varien_Event_Observer $observer)
    {
        if(!Mage::helper('cls_configurableswatches')->isEnabled()) { //functionality disabled
            return; //bail
        }

        /* @var $helper CLS_ConfigurableSwatches_Helper_Mediafallback */
        $helper = Mage::helper('cls_configurableswatches/mediafallback');

        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $observer->getCollection();

        if($collection
            instanceof CLS_ConfigurableSwatches_Model_Resource_Catalog_Product_Type_Configurable_Product_Collection) {
            //avoid recursion
            return;
        }

        $products = $collection->getItems();

        $helper->attachChildrenProducts($products, $collection->getStoreId());

        $helper->attachConfigurableProductChildrenAttributeMapping($products, $collection->getStoreId());

        $helper->attachGallerySetToCollection($products, $collection->getStoreId());

        /* @var $product Mage_Catalog_Model_Product */
        foreach($products as $product) {
            $helper->groupMediaGalleryImages($product);
            Mage::helper('cls_configurableswatches/productimg')
                ->indexProductImages($product, $product->getListSwatchAttrValues());
        }

    }

    /**
     *
     * Observes: catalog_product_load_after
     *
     * @param Varien_Event_Observer $observer
     */
    public function productLoadAfter(Varien_Event_Observer $observer) {

        if(!Mage::helper('cls_configurableswatches')->isEnabled()) { //functionality disabled
            return; //bail
        }

        /* @var $helper CLS_ConfigurableSwatches_Helper_Mediafallback */
        $helper = Mage::helper('cls_configurableswatches/mediafallback');

        /* @var $product Mage_Catalog_Model_Product */
        $product = $observer->getDataObject();

        if($product->getTypeId() != Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return;
        }

        $helper->groupMediaGalleryImages($product);

        $helper->attachConfigurableProductChildrenAttributeMapping(array($product), $product->getStoreId());
    }

    /**
     *
     * Observes: catalog_product_attribute_backend_media_load_gallery_before
     *
     * @param Varien_Event_Observer $observer
     */
    public function loadChildProductImagesOnMediaLoad(Varien_Event_Observer $observer) {

        if(!Mage::helper('cls_configurableswatches')->isEnabled()) { //functionality disabled
            return; //bail
        }

        /* @var $eventWrapper Varien_Object */
        $eventWrapper = $observer->getEventObjectWrapper();
        /* @var $product Mage_Catalog_Model_Product */
        $product = $eventWrapper->getProduct();

        if($product->getTypeId() != Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return;
        }

        /* @var $productType Mage_Catalog_Model_Product_Type_Configurable */
        $productType = Mage::getModel('catalog/product_type_configurable');

        $childrenProducts = $productType->getUsedProducts(null,$product);
        $product->setChildrenProducts($childrenProducts);

        $mediaProductIds = array();
        foreach($childrenProducts as $childProduct) {
            $mediaProductIds[] = $childProduct->getId();
        }

        if(empty($mediaProductIds)) { //no children product IDs found
            return; //bail
        }

        $mediaProductIds[] = $product->getId(); //ensure original product's media images are still loaded

        $eventWrapper->setProductIdsOverride($mediaProductIds);
    }

    /**
     * Convert a catalog layer block with the right templates
     *
     * @param Varien_Event_Observer $observer
     */
    public function convertLayerBlock(Varien_Event_Observer $observer)
    {
        $front = Mage::app()->getRequest()->getRouteName();
        $controller = Mage::app()->getRequest()->getControllerName();
        $action = Mage::app()->getRequest()->getActionName();

        // Perform this operation if we're on a category view page or search results page
        if (($front == 'catalog' && $controller == 'category' && $action == 'view')
            || ($front == 'catalogsearch' && $controller == 'result' && $action == 'index')) {

            // Block name for layered navigation differs depending on which Magento edition we're in
            $blockName = 'catalog.leftnav';
            if (Mage::getEdition() == Mage::EDITION_ENTERPRISE) {
                $blockName = ($front == 'catalogsearch') ? 'enterprisesearch.leftnav' : 'enterprisecatalog.leftnav';
            } elseif ($front == 'catalogsearch') {
                $blockName = 'catalogsearch.leftnav';
            }
            Mage::helper('cls_configurableswatches/productlist')->convertLayerBlock($blockName);
        }
    }
}