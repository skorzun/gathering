<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    LlamaCommerceCore
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_LlamaCommerceCore_Block_Customer_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{
    /**
     * Removes link by name
     *
     * @param string $name
     * @return Mage_Customer_Block_Account_Navigation
     */
    public function removeLinkByName($name)
    {
        if (isset($this->_links[$name])) {
            unset($this->_links[$name]);
        }
        return $this;
    }

    /**
     * Removes link by url
     *
     * @param string $url
     * @param array $urlParams
     * @return Mage_Customer_Block_Account_Navigation
     */
    public function removeLinkByUrl($url, $urlParams=array())
    {
        $urlToMatch = $this->getUrl($url, $urlParams);
        foreach ($this->_links as $k => $v) {
            if ($v->getUrl() == $urlToMatch) {
                unset($this->_links[$k]);
            }
        }

        return $this;
    }
}
