<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    LlamaCommerceCore
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_LlamaCommerceCore_Model_Core_Design_Fallback extends Mage_Core_Model_Design_Fallback
{
    /**
     * Get fallback scheme according to theme config
     *
     * @param string $area
     * @param string $package
     * @param string $theme
     * @return array
     * @throws Mage_Core_Exception
     */
    protected function _getFallbackScheme($area, $package, $theme)
    {
        $scheme = array(array());
        $this->_visited = array();
        while ($parent = Mage::helper('cls_llamacommercecore')->getNodeStringValue($this->_config->getNode($area . '/' . $package . '/' . $theme . '/parent'))) {

            $this->_checkVisited($area, $package, $theme);

            $parts = explode('/', $parent);
            if (count($parts) !== 2) {
                throw new Mage_Core_Exception('Parent node should be defined as "package/theme"');
            }
            list($package, $theme) = $parts;
            $scheme[] = array('_package' => $package, '_theme' => $theme);
        }

        return $scheme;
    }
}