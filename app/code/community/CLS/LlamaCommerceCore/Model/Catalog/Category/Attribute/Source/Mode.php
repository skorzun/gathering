<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    LlamaCommerceCore
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_LlamaCommerceCore_Model_Catalog_Category_Attribute_Source_Mode extends Mage_Catalog_Model_Category_Attribute_Source_Mode
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            parent::getAllOptions();

            $this->_options[] = array(
                'value' => CLS_LlamaCommerceCore_Helper_Data::CATEGORY_DM_SUBCAT,
                'label' => Mage::helper('cls_llamacommercecore')->__('Sub-categories only'),
            );
            $this->_options[] = array(
                'value' => CLS_LlamaCommerceCore_Helper_Data::CATEGORY_DM_SUBCAT_PAGE,
                'label' => Mage::helper('cls_llamacommercecore')->__('Static block and sub-categories'),
            );
            $this->_options[] = array(
                'value' => CLS_LlamaCommerceCore_Helper_Data::CATEGORY_DM_SUBCAT_PRODUCT,
                'label' => Mage::helper('cls_llamacommercecore')->__('Sub-categories and products'),
            );
            $this->_options[] = array(
                'value' => CLS_LlamaCommerceCore_Helper_Data::CATEGORY_DM_SUBCAT_PAGE_PRODUCT,
                'label' => Mage::helper('cls_llamacommercecore')->__('Static block, sub-categories and products'),
            );

        }
        return $this->_options;
    }
}
