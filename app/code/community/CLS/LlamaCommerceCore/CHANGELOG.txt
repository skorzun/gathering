CLS_LlamaCommerceCore 1.4.2 (7/7/2014)
=======================================
[CHANGED] Re-formatted README
[FIXED] Invalid markup in sub-category listing
[FIXED] Proofed against spaces in category image filenames
[ADDED] Override of PayPal info code

CLS_LlamaCommerceCore 1.4.1 (6/6/2014)
=======================================
[CHANGED] Improved styles of the customized LCP admin login

CLS_LlamaCommerceCore 1.4.0 (5/12/2014)
=======================================
[CHANGED] Base compatibility with CE 1.9/EE 1.14
[CHANGED] Re-structured llamacommerce themes to match responsive
[CHANGED] Re-structured CSS into SASS structure
[CHANGED] Implemented a remove/re-add of jQuery that puts it earlier in source than the core code
[CHANGED] Default theme fallback now defined via setup script instead of default nodes in config.xml
[ADDED] Added correct config.rb files in rwd themes that correctly compile
[ADDED] Added compile.sh and compass.sh files to facilitate compiling all themes' SASS
[ADDED] Added support for a theme fallback defined in etc/theme.xml by a call to a helper method
[ADDED] Fix for core bug where additional layout files defined in etc/theme.xml aren't retained if the theme is a fallback
[ADDED] owl-carousel JS library
[ADDED] colorbox JS library
[REMOVED] jquery.cycle and jcarousel libraries

CLS_LlamaCommerceCore 1.3.0 (4/30/2014)
=======================================
[ADDED] Community compatibility
[ADDED] Product list template override to add "additional" block
[ADDED] Added text list hook for information beneath product names in product list template
[ADDED] Turned off WYSIWYG by default
[ADDED] Added license info to docblocks
[ADDED] Added Llama Commerce logo to admin login
[ADDED] Added Llama Commerce header with user guide and Llama Desk info in admin dashboard
[CHANGED] Shifted info in System Config from CLS_Core to this module
[CHANGED] Changed sub-category listing column count to be dynamic by layout

CLS_LlamaCommerceCore 1.2.4 (3/25/2014)
=======================================
[ADDED] Set "No" as default for Redirect Customer to Account Dashboard after Logging in

CLS_LlamaCommerceCore 1.2.3 (3/9/2014)
=======================================
[FIXED] Added translate file definition to config

CLS_LlamaCommerceCore 1.2.2 (2/21/2014)
=======================================
[ADDED] Added llamacommerce as default theme path directly in config XML

CLS_LlamaCommerceCore 1.2.1 (2/20/2014)
=======================================
[CHANGED] Removed compatibility information

CLS_LlamaCommerceCore 1.2.0 (2/19/2014)
=======================================
[ADDED] Added composer.json file for automated building of LLamaCommerce

CLS_LlamaCommerceCore 1.1.0 (2/15/2014)
=======================================
[CHANGED] Official rename to LlamaCommerceCore

CLS_LlamaCommerceCore 1.0.2 (2/5/2014)
=======================================
[CHANGED] Moved documentation files for compatibility with packager script

CLS_LlamaCommerceCore 1.0.1 (1/22/2014)
=======================================
Add jquery.cookie so frontend modules can easily access cookies.

CLS_LlamaCommerceCore 1.0.0 (1/6/2014)
=======================================
Initial release
