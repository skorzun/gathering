## Llama Commerce Theme

The theme component of the Llama Commerce product offering makes various enhancements to the standard Magento theme.

### Description of Theme Enhancements

#### Global

Product tags and site poll are removed from left and right columns site-wide.

#### Pager

The default pager element (most prominently used on product listing pages) has been modified to remove the link and dropdown styles of the pager and limiter controls in favor of a cleaner button style.

#### Sub-category Listings

Categories can now be configured to display a list of their sub-categories instead of, or in addition to, product listings and CMS content.

When editing a category, the Display Mode option under Display Settings has new values for sub-categories and combinations of this content and others.

#### Product View Page

By default, product options will be displayed in the product info column instead of lower in the page beneath both columns. NOTE: This is done by changing the default value of "Display Product Options In" (see the Design tab when editing a product), and therefore this requires LlamaCommerceCore to be in place before running Magento installation. Pre-existing products will not be affected.

Other changes include:

* Removed "In stock" availability message. (i.e., now only shows if out of stock)
* Removed "Email to friend"
* Writing a product review is integrated into the product view page, instead of requiring navigation to a separate page
* Removed support for product tags

#### JavaScript libraries

To support various CLS extensions and for general use, owl.carousel and colorbox JS libraries are added to core codebase

#### Development Related

The following enhancements have no front-end implications but assist development.

* "Remove" methods have been added to the customer account navigation block.
* "Footer JS" block added so that JavaScript files can be included at the bottom of the page.
* jQuery is added more reliably to the beginning of HTML source