<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    LlamaCommerceCore
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

class CLS_LlamaCommerceCore_Helper_Url extends Mage_Core_Helper_Abstract
{
    /**
     * Get URL to Llama Commerce site
     *
     * @return string
     */
    public function getLlamaCommerceUrl()
    {
        return 'http://www.llamacommerce.com';
    }

    /**
     * Get URL to the Magento User Guide
     *
     * @return string
     */
    public function getMagentoGuideUrl()
    {
        return 'http://www.magentocommerce.com/resources/user-guide-download';
    }

    /**
     * Get URL to the Llama Commerce User Guide0
     *
     * @return string
     */
    public function getLCGuideUrl()
    {
        return 'http://www.llamacommerce.com/userguide';
    }

    /**
     * Get URL to user login on magentocommerce.com
     *
     * @return string
     */
    public function getMagentoLoginUrl()
    {
        return 'https://www.magentocommerce.com/products/customer/account/login/';
    }

    /**
     * Get URL to Llama Desk
     *
     * @return string
     */
    public function getLlamaDeskUrl()
    {
        return 'http://www.llamadesk.com';
    }
}