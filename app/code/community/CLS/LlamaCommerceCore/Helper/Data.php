<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    LlamaCommerceCore
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_LlamaCommerceCore_Helper_Data extends Mage_Core_Helper_Abstract
{
    const FILE_PATH_LLAMA_COMMERCE_VERSION = 'VERSION_LC.txt';

    const CATEGORY_DM_SUBCAT = 'SUBCATEGORIES';
    const CATEGORY_DM_SUBCAT_PAGE = 'SUBCATEGORIES_AND_PAGE';
    const CATEGORY_DM_SUBCAT_PRODUCT = 'SUBCATEGORIES_AND_PRODUCTS';
    const CATEGORY_DM_SUBCAT_PAGE_PRODUCT = 'SUBCATEGORIES_PAGE_PRODUCTS';

    /**
     * Get Llama Commerce Platform version
     *
     * @return string
     */
    public function getVersion()
    {
        $version = '';
        $filePath = Mage::getBaseDir() . DS . self::FILE_PATH_LLAMA_COMMERCE_VERSION;
        if (is_readable($filePath)) {
            $version = file_get_contents($filePath);
        }

        return $version;
    }

    /**
     * Get the phone number for Llama Desk
     *
     * @return string
     */
    public function getLlamaDeskPhone()
    {
        return '(417) 866-8887, ext. 126';
    }

    /**
     * Get the specific RWD theme that Llama Commerce should fall back on
     *
     * @return string
     */
    public function getRwdFallback()
    {
        return (Mage::getEdition() == Mage::EDITION_ENTERPRISE) ? 'rwd/enterprise' : 'rwd/default';
    }

    /**
     * Get the appropriate Llama Commerce theme for this install
     *
     * @return string
     */
    public function getLlamaCommerceTheme()
    {
        return (Mage::getEdition() == Mage::EDITION_ENTERPRISE) ? 'enterprise' : 'default';
    }

    /**
     * Take an XML node and return the simple string value or value defined by resolution of "helper" attr
     *
     * @param Varien_Simplexml_Element $node
     * @return string
     */
    public function getNodeStringValue($node)
    {
        // Return nothing if node wasn't an XML element
        if (!($node instanceof Varien_Simplexml_Element)) {
            return '';
        }

        if (isset($node['helper'])) {
            // If the node has a "helper" attribute, resolve it by getting the helper and calling the method
            $helperName = explode('/', (string)$node['helper']);
            $helperMethod = array_pop($helperName);
            $helperName = implode('/', $helperName);
            $node = $node->asArray();
            unset($node['@']);
            return call_user_func_array(array(Mage::helper($helperName), $helperMethod), $node);
        } else {
            // Otherwise, return the literal value of the node
            return (string) $node;
        }
    }

    /**
     * Resize category thumbnail if necessary and return URL
     *
     * @param Mage_Catalog_Model_Category $category
     * @param string $attributeName
     * @param int $width
     * @param int $height
     * @return string
     */
    public function resizeCategoryThumbnail($category, $attributeName, $width, $height=null)
    {
        // Basic data
        $categoryThumbPath = trim($category->getData($attributeName));
        if (empty($categoryThumbPath)) {
            return '';
        }

        $categoryBaseMediaPath = 'catalog/category';

        // Form full path to where we want to cache resized version
        $destPathArr = array(
            $categoryBaseMediaPath,
            'cache',
            Mage::app()->getStore()->getId(),
            $attributeName,
            $width.'x'.$height,
            $categoryThumbPath,
        );

        $destPath = implode('/', $destPathArr);

        // Check if cached image exists already
        if (!file_exists(Mage::getBaseDir('media') . '/' . $destPath)) {
            // Check for source image
            $sourceFilePath = Mage::getBaseDir('media') . '/' . $categoryBaseMediaPath . '/' . $categoryThumbPath;
            if (!file_exists($sourceFilePath)) {
                return '';
            }

            // Do resize and save
            $processor = new Varien_Image($sourceFilePath);
            $processor->resize($width, $height);
            $processor->save(Mage::getBaseDir('media') . '/' . $destPath);
        }

        return Mage::getBaseUrl('media') . $destPath;
    }
}