<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    LlamaCommerceCore
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */
class CLS_LlamaCommerceCore_Helper_Block extends Mage_Core_Helper_Abstract
{
    /**
     * Loop through the children of a child text list, add specific data to each, and collect the output
     *
     * @param Mage_Core_Block_Abstract $parentBlock
     * @param string $listBlockName
     * @param array $data
     * @return string
     */
    public function getChildHtmlDataRecursive($parentBlock, $listBlockName, $data = array())
    {
        $listBlock = $parentBlock->getChild($listBlockName);

        if (!($listBlock instanceof Mage_Core_Block_Text_List)) {
            return '';
        }

        $html = '';
        $children = $listBlock->getSortedChildren();
        foreach($children as $childName) {
            $child = $listBlock->getChild($childName);

            if (!($child instanceof Mage_Core_Block_Abstract)) {
                continue;
            }

            $child->addData($data);
            $html .= $child->toHtml();
        }

        return $html;
    }
}