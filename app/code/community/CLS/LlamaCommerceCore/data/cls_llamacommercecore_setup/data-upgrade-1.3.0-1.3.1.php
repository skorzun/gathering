<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * @category   CLS
 * @package    LlamaCommerceCore
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

// Set default package and theme
$packageConfigPath = 'design/package/name';
Mage::getModel('core/config_data')
    ->load($packageConfigPath, 'path')
    ->setValue('llamacommerce')
    ->setPath($packageConfigPath)
    ->save();

$themeConfigPath = 'design/theme/default';
Mage::getModel('core/config_data')
    ->load($themeConfigPath, 'path')
    ->setValue(Mage::helper('cls_llamacommercecore')->getLlamaCommerceTheme())
    ->setPath($themeConfigPath)
    ->save();