<?php
/**
 * Mage SMS - SMS notification & SMS marketing
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the BSD 3-Clause License
 * It is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/BSD-3-Clause
 *
 * @category    TOPefekt
 * @package     TOPefekt_Magesms
 * @copyright   Copyright (c) 2012-2015 TOPefekt s.r.o. (http://www.mage-sms.com)
 * @license     http://opensource.org/licenses/BSD-3-Clause
 */
class Topefekt_Magesms_Model_Hooks extends Mage_Core_Model_Abstract {

    public static $groups = array(
        'order_status'  => 0,
        'order'         => 1,
        'account'       => 2,
        'product'       => 3,
        'contactform'   => 4,
        'emailsend'     => 5
    );

    protected $cardType = array(
        'evite',
        'egift',
        'ecard',
    );


    protected function _construct()
    {
        $this->_init('magesms/hooks');
    }

    public function send($eventType, $observerData)
    {
        $oSmsprofile = Mage::getSingleton('magesms/smsprofile');
        if ($oSmsprofile->user->user) {
            $groupId = Mage::app()->getStore()->getGroupId();
            if (!$groupId && $observerData->hasStoreId()) {
                $groupId = Mage::getModel('core/store')->load($observerData->getStoreId())->getGroupId();
            }
            if ($observerData->getStore())
                $storeId = $observerData->getStore()->getId();
            elseif (Mage::app()->getStore())
                $storeId = Mage::app()->getStore()->getStoreId();
            else $storeId = null;
            if (!Mage::helper('magesms')->isActive($storeId)) return $this;
            if (Mage::registry('magesms_store_id')) Mage::unregister('magesms_store_id');
            Mage::register('magesms_store_id', $storeId, true);

            $oHooksAdminsCollection = Mage::getSingleton('magesms/hooks_admins')->getCollection();
            //$oHooksAdminsCollection->addFieldToFilter('name', 'contactForm');
            //$hooksAdm = $oHooksAdminsCollection->getFirstItem()->getSmstext();

            if ($groupId || !Mage::getSingleton('admin/session')->isLoggedIn()) {
                $oHooksAdminsCollection->addFieldToFilter('store_group_id', $groupId);
            } else {
                $oHooksAdminsCollection->getSelect()->group('admin_id');
            }
            if ($eventType == 'updateOrderStatus') {
                $status = $observerData->getStatus();
                $status = preg_replace('/[^a-zA-Z0-9_]/', '_', $status);
                $status = preg_replace('/^([^a-zA-Z])/', 'x$1', $status);
                $oHooksAdminsCollection->addFieldToFilter('name', 'orderStatus' . uc_words($status, ''));
                $eventT = $eventType . ' - orderStatus' . uc_words($status, '');
            } else {
                $oHooksAdminsCollection->addFieldToFilter('name', $eventType);
                $eventT = $eventType;
            }

            if ($oHooksAdminsCollection->count()) {
                $oFirstItem = Mage::getSingleton('magesms/hooks_unicode')->getCollection() ->addFieldToFilter('type', 'admin')->getFirstItem();

                //============================================================

                //if ($eventType == 'contactForm') {
//                if (in_array($eventType, $this->cardType)) {
//                    //$eventType = $eventType . 'Send';
//                    $aRecepientData = $observerData->getData('recepient_data');
//                    $oSms = Mage::getModel('magesms/sms');
//                    foreach($aRecepientData as $aRecepient){
//                        $name   = $aRecepient['name'];
//                        $number = $aRecepient['number'];
//                        $oSms->addRecipient($number, array(
//                                'recipient' => $name
//                                //'adminId'   => 1
//                            )
//                        ) ->setMessage($this->prepareText(
//                        //$oHooksAdmins->getSmstext(),
//                            $oHooksAdminsCollection->getFirstItem()->getSmstext(),
//                            $eventType,
//                            $observerData))
//                            ->setSubject($eventT)
//                            ->setType(Topefekt_Magesms_Model_Sms::TYPE_ADMIN)
//                            ->setPriority(true)
//                            ->setUnicode($oFirstItem->getUnicode())
//                            ->setStoreId($storeId);
//                        $oSms->setHookName($eventT);
//
//                        //$oSms->send();
//                    }
//                    $oSms->send();

                if (in_array($eventType, $this->cardType)) {
                    $oSms   = Mage::getModel('magesms/sms');
                    $name   = $observerData->getName();
                    $number = $observerData->getNumber();
                    $oSms->addRecipient($number, array(
                            'recipient' => $name
                            //'adminId'   => 1
                        )
                    ) ->setMessage($this->prepareText(
                        $oHooksAdminsCollection->getFirstItem()->getSmstext(),
                        $eventType,
                        $observerData))
                        ->setSubject($eventT)
                        ->setType(Topefekt_Magesms_Model_Sms::TYPE_ADMIN)
                        ->setPriority(true)
                        ->setUnicode($oFirstItem->getUnicode())
                        ->setStoreId($storeId);
                    $oSms->setHookName($eventT);
                    $oSms->send();
                } else {
                    //============================================================
                    foreach($oHooksAdminsCollection as $oHooksAdmins) {
                        $adminId = Mage::getModel('magesms/admins')->load($oHooksAdmins->getAdminId());
                        if (!$adminId) continue;
                        $oSms = Mage::getModel('magesms/sms');
                        $oSms->addRecipient($adminId->getNumber(), array(
                                'recipient' => $adminId->getName(),
                                'adminId'   => $adminId->getId()
                            )
                        ) ->setMessage($this->prepareText(
                            $oHooksAdmins->getSmstext(),
                            $eventType,
                            $observerData
                        ))
                            ->setSubject($eventT)
                            ->setType(Topefekt_Magesms_Model_Sms::TYPE_ADMIN)
                            ->setPriority(true)
                            ->setUnicode($oFirstItem->getUnicode())
                            ->setStoreId($storeId);
                        $oSms->setHookName($eventT);
                        $oSms->send();
                    }
                    //====================
                }
                //====================

//                foreach($oHooksAdminsCollection as $oHooksAdmins) {
//                    $adminId = Mage::getModel('magesms/admins')->load($oHooksAdmins->getAdminId());
//                    if (!$adminId) continue;
//                    $oSms = Mage::getModel('magesms/sms');
//                    $oSms->addRecipient($adminId->getNumber(), array(
//                            'recipient' => $adminId->getName(),
//                            'adminId'   => $adminId->getId()
//                        )
//                    ) ->setMessage($this->prepareText(
//                         $oHooksAdmins->getSmstext(),
//                         $eventType,
//                         $observerData))
//                        ->setSubject($eventT)
//                        ->setType(Topefekt_Magesms_Model_Sms::TYPE_ADMIN)
//                        ->setPriority(true)
//                        ->setUnicode($oFirstItem->getUnicode())
//                        ->setStoreId($storeId);
//                    $oSms->setHookName($eventT);
//                    $oSms->send();
//                }
            }

            if ($observerData instanceof Mage_Sales_Model_Order && Mage::getStoreConfig('magesms/magesms/customer_groups_enable', $storeId)) {
                $groups = Mage::getStoreConfig('magesms/magesms/customer_groups', $storeId);
                if (!in_array($observerData->getCustomerGroupId(), explode(',', $groups))) return $this;
            }
            $oHooksCustomer = Mage::getSingleton('magesms/hooks_customers')
                ->getCollection()
                ->addFieldToFilter('active', 1)
                ->addFieldToFilter('mutation', Mage::getStoreConfig('general/locale/code', $storeId));
            if ($eventType == 'updateOrderStatus') {
                $status = $observerData->getStatus(); $status = preg_replace('/[^a-zA-Z0-9_]/', '_', $status);
                $status = preg_replace('/^([^a-zA-Z])/', 'x$1', $status);
                $oHooksCustomer->addFieldToFilter('name', 'orderStatus'.uc_words($status, ''));
                $eventT = $eventType.' - orderStatus'.uc_words($status, '');
            } else {
                $oHooksCustomer->addFieldToFilter('name', $eventType);
                $eventT = $eventType;
            } if ($oHooksCustomer->count()) {
                if ($observerData instanceof Mage_Sales_Model_Order) {
                    $oOutputOrder = Mage::getModel('magesms/optout_order')->getCollection() ->addFieldToFilter('order_id', $observerData->getId()) ->addFieldToFilter('disabled', 1);
                    if ($oOutputOrder->count()) {
                        return $this;
                    }
                    if ($oSmsprofile->user->getPrefbilling()) {
                        if ($observerData->getShippingAddress()) {
                            $telephone = $observerData->getShippingAddress()->getTelephone();
                            $countryId = $observerData->getShippingAddress()->getCountryId();
                        }
                        if (empty($telephone) || !empty($telephone) && !preg_match('/^[0-9+()\/\.\s-]+$/', $telephone)) {
                            $telephone = $observerData->getBillingAddress()->getTelephone();
                            $countryId = $observerData->getBillingAddress()->getCountryId();
                        }
                    } else {
                        $telephone = $observerData->getBillingAddress()->getTelephone();
                        $countryId = $observerData->getBillingAddress()->getCountryId();
                        if (!$telephone || preg_match('/^[0-9+()\/\.\s-]+$/', $telephone)) {
                            $telephone = $observerData->getShippingAddress()->getTelephone();
                            $countryId = $observerData->getShippingAddress()->getCountryId();
                        }
                    }
                    if (!preg_match('/^[0-9+()\/\.\s-]+$/', $telephone)) $telephone = '';
                    $customerId = $observerData->getCustomerId();
                    if (!$customerId) $customerId = 0; $customerName = $observerData->getCustomerName();
                } else {
                    $telephone = '';
                    $customerId = $observerData->getId();
                }

                if (Mage::helper('core')->isModuleEnabled('Amasty_Orderattr')) {
                    if (($moderattr = Mage::app()->getRequest()->getParam('amorderattr')) && !empty($moderattr['contact_tel'])) {
                        $telephone = $moderattr['contact_tel'];

                    } else {
                        $oAttr = Mage::getModel('amorderattr/attribute')->load($observerData->getId(), 'order_id');
                        if ($oAttr->getContactTel()) {
                            $telephone = $oAttr->getContactTel();
                        }
                    }
                } if (!$telephone) {
                    $aParams = Mage::app()->getRequest()->getParams();
                    foreach(array('mobilenumber', 'mobile_number', 'phone', 'phone_number', 'telephone', 'mobile') as $param) {
                        if (!empty($aParams[$param])) {
                            $telephone = $aParams[$param];
                            $countryId = Mage::getStoreConfig('general/country/default', $storeId);;
                            break;
                        }
                    }
                } if ($telephone && is_numeric($customerId)) {
                    $oHookCustomerFirstItem = $oHooksCustomer->getFirstItem();
                    $smsText = $oHookCustomerFirstItem->getSmstext();
                    if ($smsText) {
                        $oSms = Mage::getModel('magesms/sms');
                        $oSms->addRecipient($telephone, array(
                                'recipient'  => $customerName,
                                'customerId' => $customerId,
                                'country'    => $countryId)
                        )
                            ->setMessage($this->prepareText($smsText, $eventType, $observerData
                            ))
                            ->setSubject($eventT) ->setType(Topefekt_Magesms_Model_Sms::TYPE_CUSTOMER) ->setPriority(true) ->setStoreId($storeId);
                        $oHooksUnicode = Mage::getSingleton('magesms/hooks_unicode')
                            ->getCollection()
                            ->addFieldToFilter('type', 'customer')
                            ->addFieldToFilter('area', $oHookCustomerFirstItem->getMutation());
                        if ($oHooksUnicode->count()) {
                            $oFirstItem = $oHooksUnicode->getFirstItem();
                            $oSms->setUnicode($oFirstItem->getUnicode());
                        }
                        $oSms->setHookName($eventT);
                        $oSms->send();
                    }
                }
            }
        }
        return $this;
    }

    public function prepareText($sPrepareText, $eventType, $observerData)
    {
        if (preg_match_all('/{(.*?)}/', $sPrepareText, $dataText)) {
            $aData = array();
            foreach($dataText[1] as $sData) {
                $aData[$sData] = '{'.$sData.'}';
            }
            if ($observerData->getStore()) $storeId = $observerData->getStore()->getId();
            elseif (Mage::app()->getStore()) $storeId = Mage::app()->getStore()->getStoreId();
            else $storeId = null;
            if (isset($aData['shop_domain'])) {
                $aUrl = parse_url(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB));
                $aData['shop_domain'] = $aUrl['host'].($aUrl['path'] != '/' ? $aUrl['path'] : '');
            }
            if (isset($aData['shop_name'])) {
                $aData['shop_name'] = Mage::getStoreConfig('general/store_information/name', $storeId);
            }
            if (isset($aData['shop_name2'])) {
                $aData['shop_name2'] = Mage::app()->getStore()->getName();
            }
            if (isset($aData['shop_email'])) {
                $aData['shop_email'] = Mage::getStoreConfig('trans_email/ident_general/email', $storeId);
            }
            if (isset($aData['shop_phone'])) {
                $aData['shop_phone'] = Mage::getStoreConfig('general/store_information/phone', $storeId);
            }
            //======================================

            if(in_array($eventType, $this->cardType)) {
                if(isset($aData['sender_name'])) {
                    $aData['sender_name'] = $observerData->getSender();
                }
                if(isset($aData['link_to_' . $eventType])) {
                    $aData['link_to_' . $eventType] = $observerData->getLink();
                }
            }

            //======================================
            if ($eventType == 'contactForm') {
                $oRequest = Mage::app()->getRequest();
                if (isset($aData['customer_email'])) {
                    $aData['customer_email'] = trim($oRequest->getPost('email'));
                }
                if (isset($aData['customer_name'])) {
                    $aData['customer_name'] = trim($oRequest->getPost('name'));
                }
                if (isset($aData['customer_phone'])) {
                    $aData['customer_phone'] = trim($oRequest->getPost('telephone'));
                }
                if (isset($aData['customer_message'])) {
                    $aData['customer_message'] = trim($oRequest->getPost('comment'));
                }
                if (isset($aData['customer_message_short1'])) {
                    $aData['customer_message_short1'] = Mage::helper('magesms')->substr(trim($oRequest->getPost('comment')), 0, 120);
                }
                if (isset($aData['customer_message_short2'])) {
                    $aData['customer_message_short2'] = Mage::helper('magesms')->substr(trim($oRequest->getPost('comment')), 0, 100);
                }
                if (isset($aData['customer_message_short3'])) {
                    $aData['customer_message_short3'] = Mage::helper('magesms')->substr(trim($oRequest->getPost('comment')), 0, 80);
                }
            }
            if ($eventType == 'customerRegisterSuccess') {
                if (isset($aData['customer_id'])) {
                    $aData['customer_id'] = $observerData->getId();
                }
                if (isset($aData['customer_email'])) {
                    $aData['customer_email'] = $observerData->getEmail();
                }
                if (isset($aData['customer_password'])) {
                    $aData['customer_password'] = Mage::app()->getRequest()->getParam('password');
                }
                if (isset($aData['customer_firstname'])) {
                    $aData['customer_firstname'] = $observerData->getFirstname();
                }
                if (isset($aData['customer_lastname'])) {
                    $aData['customer_lastname'] = $observerData->getLastname();
                }
            }

            if ($eventType == 'newOrder' || $eventType == 'updateOrderStatus' || $eventType == 'updateOrderTrackingNumber') {
                if (isset($aData['customer_id'])) {
                    $aData['customer_id'] = $observerData->getCustomerId();
                }
                if (isset($aData['customer_email'])) {
                    $aData['customer_email'] = $observerData->getCustomerEmail();
                }
                if (isset($aData['customer_firstname'])) {
                    $aData['customer_firstname'] = $observerData->getCustomerFirstname();
                }
                if (isset($aData['customer_lastname'])) {
                    $aData['customer_lastname'] = $observerData->getCustomerLastname();
                }
                if (!isset($oShippingAddress)) $oShippingAddress = $observerData->getShippingAddress();
                if (isset($aData['customer_company'])) {
                    $aData['customer_company'] = $oShippingAddress->getCompany();
                }
                if (isset($aData['customer_address'])) {
                    $aData['customer_address'] = $oShippingAddress->getStreet(1);
                }
                if (isset($aData['customer_postcode'])) {
                    $aData['customer_postcode'] = $oShippingAddress->getPostcode();
                }
                if (isset($aData['customer_city'])) {
                    $aData['customer_city'] = $oShippingAddress->getCity();
                }
                if (isset($aData['customer_country'])) {
                    $aData['customer_country'] = $oShippingAddress->getCountry();
                }
                if (isset($aData['customer_state'])) {
                    $aData['customer_state'] = $oShippingAddress->getRegion();
                }
                if (isset($aData['customer_phone'])) {
                    $aData['customer_phone'] = $oShippingAddress->getTelephone();
                }
                if (isset($aData['customer_vat_number'])) {
                    $aData['customer_vat_number'] = $oShippingAddress->getVatId();
                }
                if (!isset($oBillingAddress)) $oBillingAddress = $observerData->getBillingAddress();
                if (isset($aData['customer_invoice_company'])) {
                    $aData['customer_invoice_company'] = $oBillingAddress->getCompany();
                }
                if (isset($aData['customer_invoice_firstname'])) {
                    $aData['customer_invoice_firstname'] = $oBillingAddress->getFirstname();
                }
                if (isset($aData['customer_invoice_lastname'])) {
                    $aData['customer_invoice_lastname'] = $oBillingAddress->getLastname();
                }
                if (isset($aData['customer_invoice_address'])) {
                    $aData['customer_invoice_address'] = $oBillingAddress->getStreet(1);
                }
                if (isset($aData['customer_invoice_postcode'])) {
                    $aData['customer_invoice_postcode'] = $oBillingAddress->getPostcode();
                }
                if (isset($aData['customer_invoice_city'])) {
                    $aData['customer_invoice_city'] = $oBillingAddress->getCity();
                }
                if (isset($aData['customer_invoice_country'])) {
                    $aData['customer_invoice_country'] = $oBillingAddress->getCountry();
                }
                if (isset($aData['customer_invoice_state'])) {
                    $aData['customer_invoice_state'] = $oBillingAddress->getRegion();
                }
                if (isset($aData['customer_invoice_phone'])) {
                    $aData['customer_invoice_phone'] = $oBillingAddress->getTelephone();
                }
                if (isset($aData['customer_invoice_vat_number'])) {
                    $aData['customer_invoice_vat_number'] = $oBillingAddress->getVatId();
                }
                if (isset($aData['order_id'])) {
                    $aData['order_id'] = $observerData->getIncrementId();
                }
                if (isset($aData['order_payment'])) {
                    $aData['order_payment'] = $observerData->getPayment()->getMethodInstance()->getTitle();
                }
                if (isset($aData['order_payment_html'])) {
                    $oPayment = Mage::helper('payment')->getInfoBlock($observerData->getPayment()) ->setIsSecureMode(true);
                    $oPayment->getMethod()->setStore($storeId);
                    $paymentHtml = strip_tags($oPayment->toHtml());
                    $paymentHtml = preg_replace('/  +/', ' ', $paymentHtml);
                    $paymentHtml = preg_replace("/ \n/", "\n", $paymentHtml);
                    $paymentHtml = preg_replace("/\n /", "\n", $paymentHtml);
                    $paymentHtml = preg_replace("/\n\n+/", "\n", $paymentHtml);
                    $aData['order_payment_html'] = trim($paymentHtml);
                }
                if (isset($aData['order_total_paid'])) {
                    $aData['order_total_paid'] = Mage::getModel('directory/currency')->format($observerData->getGrandTotal(), array('display'=>Zend_Currency::NO_SYMBOL), false);
                }
                if (isset($aData['order_currency'])) {
                    $aData['order_currency'] = $observerData->getOrderCurrency()->getCurrencyCode();
                }
                $this->f2b4066ec99f97011a4a9f20dd18d97b5a49b8b51($aData, $observerData->getCreatedAt());
                if (isset($aData['delivery_date'])) {
                    $oOrder = Mage::getModel('ecommerceteam_ddc/order');
                    if ($oOrder) {
                        $aOrder = $oOrder->load($observerData->getEntityId(), 'order_id')->getData();
                        if (isset($aOrder['order_id'])) {
                            if (strtotime($aOrder['delivery_date'])) {
                                $deliveryData = Mage::getSingleton('core/locale')->date($aOrder['delivery_date'], Zend_Date::ISO_8601, null, false)->toString(Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_FULL));
                                $aData['delivery_date'] = $deliveryData;
                            }
                        } elseif ($paramDeliveryData = Mage::app()->getRequest()->getParam('delivery_date')) {
                            $aData['delivery_date'] = $paramDeliveryData;
                        }
                    }
                }
                if ($eventType == 'newOrder') {
                    if (isset($aData['cart_id'])) {
                        $aData['cart_id'] = Mage::getSingleton('checkout/session')->getQuoteId();
                    }
                    $oObserverDataCollection = $observerData->getItemsCollection();
                    if (isset($aData['newOrder1'])) {
                        $aObserverData = array();
                        foreach($oObserverDataCollection as $observerData) {
                            $aObserverData[] = $observerData->getId().'/'.$observerData->getName().'/'.$observerData->getQtyOrdered();
                        }
                        $aData['newOrder1'] = implode('; ', $aObserverData);
                    }
                    if (isset($aData['newOrder2'])) {
                        $aObserverData = array();
                        foreach($oObserverDataCollection as $observerData) {
                            $aObserverData[] = 'id:'.$observerData->getId().', ' .Mage::helper('magesms')->__('name').':'.$observerData->getName().', ' .Mage::helper('magesms')->__('qty').':'.$observerData->getQtyOrdered();
                        }
                        $aData['newOrder2'] = implode('; ', $aObserverData);
                    }
                    if (isset($aData['newOrder3'])) {
                        $aObserverData = array();
                        foreach($oObserverDataCollection as $observerData) {
                            $aObserverData[] = $observerData->getId().'/'.$observerData->getQtyOrdered();
                        }
                        $aData['newOrder3'] = implode('; ', $aObserverData);
                    }
                    if (isset($aData['newOrder4'])) {
                        $aObserverData = array();
                        foreach($oObserverDataCollection as $observerData) {
                            $aObserverData[] = 'id:'.$observerData->getId().', ' .Mage::helper('magesms')->__('qty').':'.$observerData->getQtyOrdered();
                        }
                        $aData['newOrder4'] = implode('; ', $aObserverData);
                    }
                    if (isset($aData['newOrder5'])) {
                        $aObserverData = array();
                        foreach($oObserverDataCollection as $observerData) {
                            $aObserverData[] = $observerData->getId().'/'.$observerData->getSku().'/'.$observerData->getQtyOrdered();
                        }
                        $aData['newOrder5'] = implode('; ', $aObserverData);
                    }
                }
                if ($eventType == 'updateOrderStatus' || $eventType == 'updateOrderTrackingNumber') {
                    if (!($oTrack = Mage::registry('magesms_track_obj'))) $oTrack = $observerData->getTracksCollection()->getLastItem();
                    if (!$oTrack->getId()) {
                        $aParams = Mage::app()->getRequest()->getParams();
                        if (isset($aParams['tracking']) && ($tracking = $aParams['tracking'])) {
                            $tracking = end($tracking);
                            if (!empty($tracking['title'])) $oTrack->setTitle($tracking['title']);
                            if (!empty($tracking['number'])) $oTrack->setTrackNumber($tracking['number']);
                        }
                    }
                    if ($oTrack) {
                        if (isset($aData['carrier_name'])) {
                            $aData['carrier_name'] = $oTrack->getTitle();
                        }
                        if (isset($aData['order_shipping_number'])) {
                            $aData['order_shipping_number'] = $oTrack->getTrackNumber();
                        }
                    }
                    $adminId = Mage::getSingleton('admin/session')->getUser();
                    if (isset($aData['employee_id'])) {
                        $aData['employee_id'] = $adminId->getId();
                    }
                    if (isset($aData['employee_email'])) {
                        $aData['employee_email'] = $adminId->getEmail();
                    }
                }
            }
            if ($eventType == 'productOutOfStock' || $eventType == 'productLowStock') {
                if (isset($aData['product_id'])) {
                    $aData['product_id'] = $observerData->getProductId();
                }
                if (isset($aData['product_quantity'])) {
                    $aData['product_quantity'] = $observerData->getQty();
                }
                if (isset($aData['product_name']) || isset($aData['product_ref'])) {
                    $observerData = Mage::getModel('catalog/product');
                    $observerData->load($observerData->getProductId());
                    if (isset($aData['product_name'])) {
                        $aData['product_name'] = $observerData->getName();
                    }
                    if (isset($aData['product_ref'])) {
                        $aData['product_ref'] = $observerData->getSku();
                    }
                }
                if (isset($aData['customer_id']) || isset($aData['customer_email']) || isset($aData['customer_lastname']) || isset($aData['customer_firstname'])) {
                    if ($oCustomer = Mage::getSingleton('customer/session')->getCustomer()) {
                        if (isset($aData['customer_id'])) {
                            $aData['customer_id'] = $oCustomer->getId();
                        }
                        if (isset($aData['customer_email'])) {
                            $aData['customer_email'] = $oCustomer->getEmail();
                        }
                        if (isset($aData['customer_lastname'])) {
                            $aData['customer_lastname'] = $oCustomer->getLastname();
                        }
                        if (isset($aData['customer_firstname'])) {
                            $aData['customer_firstname'] = $oCustomer->getFirstname();
                        }
                    }
                }
            }
            foreach($aData as $param => $value) {
                $sPrepareText = str_replace('{'.$param.'}', $value, $sPrepareText);
            }
        }
        return $sPrepareText;
    }

    private function f2b4066ec99f97011a4a9f20dd18d97b5a49b8b51(&$aData, $orderDate)
    {
        if (isset($aData['order_date'])) {
            $aData['order_date'] = $orderDate;
        }

        $aOrderDate = date_parse($orderDate);
        if (isset($aData['order_date1'])) {
            $aData['order_date1'] = $aOrderDate['day'].'.'.$aOrderDate['month'].'.'.$aOrderDate['year'];
        }
        if (isset($aData['order_date2'])) {
            $aData['order_date2'] = $aOrderDate['day'].'/'.$aOrderDate['month'].'/'.$aOrderDate['year'];
        }
        if (isset($aData['order_date3'])) {
            $aData['order_date3'] = $aOrderDate['day'].'-'.$aOrderDate['month'].'-'.$aOrderDate['year'];
        }
        if (isset($aData['order_date4'])) {
            $aData['order_date4'] = $aOrderDate['year'].'-'.$aOrderDate['month'].'-'.$aOrderDate['day'];
        }
        if (isset($aData['order_date5'])) {
            $aData['order_date5'] = $aOrderDate['month'].'.'.$aOrderDate['day'].'.'.$aOrderDate['year'];
        }
        if (isset($aData['order_date6'])) {
            $aData['order_date6'] = $aOrderDate['month'].'/'.$aOrderDate['day'].'/'.$aOrderDate['year'];
        }
        if (isset($aData['order_date7'])) {
            $aData['order_date7'] = $aOrderDate['month'].'-'.$aOrderDate['day'].'-'.$aOrderDate['year'];
        }
        if (isset($aData['order_time'])) {
            $aData['order_time'] = $aOrderDate['hour'].':'.sprintf('%02.0f', $aOrderDate['minute']);
        }
        if (isset($aData['order_time1'])) {
            $aData['order_time1'] = $aOrderDate['hour'].':'.sprintf('%02.0f', $aOrderDate['minute']).':'.sprintf('%02.0f', $aOrderDate['second']);
        }
    }
}