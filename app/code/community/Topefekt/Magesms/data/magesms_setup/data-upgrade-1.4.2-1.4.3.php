<?php

$aCardType = array(
    'evite',
    'egift',
    'ecard'
);

$aLang = array(
    'en',
    'ru',
);

// Set user

$oUser = Mage::getModel('magesms/smsuser');
$oUser->setData(array(
    'user'        => 'ms1510261202LTWbpA',
    'passwd'      => 'iLqbb73QvQbRdM',
    'email'       => 'srieznikov@magecom.net',
    'regtype'     => 'person',
    'country0'    => 'Ukraine',
    'simulatesms' => '1',
    'deletedb'    => '0',
    'pocetkredit' => '0',
    'URLreports'  => '1',
    'prefbilling' => '1',
    'firstname'   => 'Serg',
    'lastname'    => 'R',
));

// Set variables

$oVariables = Mage::getModel('magesms/variables');
foreach ($aCardType as $cardType) {
    $oVariables->setData(array(
        'name'     => 'link_to_' . $cardType,
        'template' => 'http://www.thegatheringoffriends.com',
    ))->save();
}

$oVariables->setData(array(
    'name'     => 'sender_name',
    'template' => 'Mishel',
))->save();

// Set hooks

$oHooks = Mage::getModel('magesms/hooks');
foreach ($aLang as $lang) {
    $oHooks->setData(array(
        'name'     => 'ecard',
        'info'     => 'eCard send',
        'status'   => '0',
        'owner'    => '3',
        'group'    => '5',
        'template' => '{sender_name} has sent you a card! Click here to view your eCard {link_to_ecard}',
        'notice'   => '{sender_name}, {link_to_ecard}',
        'lang'     => $lang,
    ))->save();

    $oHooks->setData(array(
        'name'     => 'egift',
        'info'     => 'eGift send',
        'status'   => '0',
        'owner'    => '3',
        'group'    => '5',
        'template' => '{sender_name} has sent you a gift! Click here to view your gift {link_to_egift}',
        'notice'   => '{sender_name}, {link_to_egift}',
        'lang'     => $lang,
    ))->save();

    $oHooks->setData(array(
        'name'     => 'evite',
        'info'     => 'eVite send',
        'status'   => '0',
        'owner'    => '3',
        'group'    => '5',
        'template' => '{sender_name} has sent you an invitation! Click here {link_to_evite}',
        'notice'   => '{sender_name}, {link_to_evite}',
        'lang'     => $lang,
    ))->save();
}


