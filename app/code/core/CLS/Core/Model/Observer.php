<?php
/**
 * Llama Commerce Platform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Llama Commerce Platform License
 * that is bundled with this package in the file LICENSE_LC.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.llamacommerce.com/license
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@llamacommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
 * to newer versions in the future. If you wish to customize Llama Commerce
 * Platform for your needs please refer to http://www.llamacommerce.com
 * for more information.
 *
 * Observer.php
 *
 * @category   CLS
 * @package    Core
 * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
 * @license    http://www.llamacommerce.com/license
 */

Class CLS_Core_Model_Observer
{
    /**
     * Record that this was the system variable grid page
     *
     * @event controller_action_predispatch_adminhtml_system_index
     */
    public function systemVariablePredispatch() {
        if (!Mage::registry('cls_core_system_variable')) {
            Mage::register('cls_core_system_variable', 1);
        }
    }

    /**
     * @param $observer
     * @event core_collection_abstract_load_before
     */
    public function removeUserInvisibleFromCollection($observer) {
        if (Mage::registry('cls_core_system_variable') && $observer->getCollection() instanceof Mage_Core_Model_Resource_Variable_Collection) {
            $observer->getCollection()->addFieldToFilter("user_visible", true);
        }
    }
}