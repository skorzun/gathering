<?php

class Dragonrobot_Ecard_Block_Downloadable_Sales_Order_Email_Items_Downloadable
    extends Mage_Downloadable_Block_Sales_Order_Email_Items_Downloadable {
    public function getLinks() {
        $this->_purchased = Mage::getModel('downloadable/link_purchased')
            ->load($this->getItem()->getOrder()->getId(), 'order_id');
        $purchasedLinks = Mage::getModel('downloadable/link_purchased_item')->getCollection()
            ->addFieldToFilter('order_item_id', $this->getItem()->getOrderItem()->getId())
            ->addFieldToFilter('is_for_ecard', 0)
            ->addFieldToFilter('status', array('neq' => Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_EXPIRED));
        $this->_purchased->setPurchasedItems($purchasedLinks);

        return $this->_purchased;
    }
}
