<?php   
class Dragonrobot_Ecard_Block_Download extends Dragonrobot_MultipleDownloads_Block_Downloadable_Customer_Products_List {   


    public function __construct()
    {
        parent::__construct();
        //reset purchased and items
        if($ecard = Mage::registry('ecard-object')) {
            $purchased = Mage::getResourceModel('downloadable/link_purchased_collection')
                ->addFieldToFilter('order_item_id', array(
                    'in' => explode(",", $ecard->getAssociatedItems())))
                ->addOrder('created_at', 'desc');
            $this->setPurchased($purchased);
            $purchasedIds = array();
            foreach ($purchased as $_item) {
                $purchasedIds[] = $_item->getId();
            }
            if (empty($purchasedIds)) {
                $purchasedIds = array(null);
            }
            $purchasedItems = Mage::getResourceModel('downloadable/link_purchased_item_collection')
                ->addFieldToFilter('purchased_id', array('in' => $purchasedIds))
                ->addFieldToFilter('status',
                    array(
                        'nin' => array(
                            Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING_PAYMENT,
                            Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PAYMENT_REVIEW
                        )
                    )
                )
                ->setOrder('item_id', 'desc');
            $this->setItems($purchasedItems);
        } else {
            Mage::throwException("Ecard object is missing, is the uid right?");
        }
        
    }
}