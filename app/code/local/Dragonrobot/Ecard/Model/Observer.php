<?php
class Dragonrobot_Ecard_Model_Observer
{
    public function addProductOptionsToEcard($observer) {
        // set the additional options on the product
        $post = $_POST;
        $action = Mage::app()->getFrontController()->getAction();
        $fullName = $action->getFullActionName();
        if ($fullName == 'checkout_cart_add' )//|| $fullName == 'ajaxcartpro_add_add')
        {
            //$product = $observer->getProduct();
            $product = $observer->getQuoteItem()->getProduct();
            if($product->getTypeId() == "ecard") {
                $item = $observer->getQuoteItem();

                 // add to the additional options array
                $additionalOptions = array();
                if ($additionalOption = $product->getCustomOption('additional_options')) {
                    $additionalOptions = (array) unserialize($additionalOption->getValue());
                }
                $cardText = $action->getRequest()->getParam('cardText');
                echo $cardText; die;
                $isGift = $action->getRequest()->getParam('isGift');
                if($isGift && $whoToEmail == 'email-someone-else') {  //email should not be sent to myself, but recipient
                    $email = $action->getRequest()->getParam('voucher-recipient-email');
                } elseif ($isGift && $whoToEmail == 'email-myself') {
                    $email = false;  //it will get set from invoice
                } else {
                    $email = false;
                }
                $validator = new Zend_Validate_EmailAddress();

                if($email && $validator->isValid($email)) {
                    $additionalOptions[] = array(
                        'label' => "Voucher Recipient Email",
                        'value' => $email,
                    );
                } else if ($email && !$validator->isValid($email)) {
                    throw new Exception("Email address for voucher is invalid!");
                }

                $name = $action->getRequest()->getParam('voucher-recipient-name');
                if($isGift && !empty($name)) {
                    $additionalOptions[] = array(
                        'label' => "Voucher Recipient Name",
                        'value' => $name,
                    );
                }


                // add the additional options array with the option code additional_options
                $item->addOption(new Varien_Object(
                    array(
                        'product'   => $item->getProduct(),
                        'code'      => 'additional_options',
                        'value'     => serialize($additionalOptions)
                    )
                ));
            }
        }
    }
    public function isNewInvoice($event) {
        $id = $event->getInvoice()->getId();
        if($id) {
            Mage::register('voucher_is_new_invoice', false); //not new
        } else {
            Mage::register('voucher_is_new_invoice', true); //new
        }
    }
    
    public function setupEcardDatabase($observer) {

        //this check below shouldnt happen since invoice's are only saved once,
        //but just in case
        if(Mage::registry('voucher_is_new_invoice') == false) {
            return;
        }
        //associate items with links
        $invoice = $observer->getEvent()->getInvoice();
        $items = $invoice->getAllItems();
        $ecards = array();  // of item numbers
        $ebooks = array(
            "book1" => array(),
            "book2" => array(),
            "book3" => array(),
            "book4" => array(),
            "book5" => array(),
            "book6" => array(),
            "book7" => array(),
            "bookAll" => array(),
        );  
        
        //sort into nice arrays
        foreach($items as $item) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
            $type = $product->getTypeId();
            if($type == "ecard") {
                $ecards[] = $item;
            } elseif ($book = Mage::helper('ecard')->isEbook($item->getSku())){
                for($x=0; $x < $item->getQty(); $x++) {
                    $ebooks[$book][] = $item;  //add it multiple times if qty is more than once
                }
            }
            
        }
        
        foreach($ecards as $ecard) {
            //get additional options
            $orderItem = $ecard->getOrderItem();
            $options = $orderItem->getProductOptions();
            if(isset($options['additional_options'])) {
                $itemIds = array();
                foreach($options['additional_options'] as $option) {
                    switch ($option['label']) {
                        case 'eCard Associated Products':
                            $associatedProducts = explode(",", $option['value']);
                            foreach($associatedProducts as $book) {
                                $ebook = array_pop($ebooks[$book]);
                                $itemIds[] = $ebook->getOrderItem()->getId();
                            }
                            break;
                    }
                }
                
                //schedule it
                $ecard = Mage::getModel('ecard/ecard');
                
                foreach($options['additional_options'] as &$option) {
                    switch ($option['label']) {
                        case 'eCard Name':
                            $ecard->setName($option['value']);
                            break;
                        case 'eCard Email':
                            $ecard->setEmail($option['value']);
                            break;
                        case 'eCard Text':
                            $ecard->setText($option['value']);
                            break;
                        case 'eCard Data':
                            $ecard->setData('data', $option['value']);
                            break;
                        case 'eCard Send Date':
                            $ecard->setSendDate($option['value']);
                            break;
                        case 'eCard Associated Items':
                            $option['value'] = implode(",", $itemIds);
                            $ecard->setAssociatedItems(implode(",", $itemIds));
                            break;
                        case 'eCard Number':
                            $ecard->setCardNumber($option['value']);
                            break;
                        case 'eCard Recipient Name':
                            $ecard->setRecipientName($option['value']);
                            break;
                        case 'eCard Recipient Email':
                            $ecard->setRecipientEmail($option['value']);
                            break;
                        
                    }
                }
                $orderItem->setProductOptions($options);
                $orderItem->save();
                
                $uniqueId = uniqid($orderItem->getOrderId());
                $ecard->setUniqueId($uniqueId);
                $ecard->setOrderId($orderItem->getOrderId());
                $ecard->setCreatedAt(date('Y-m-d H:i:s'));
                $ecard->setCreated(date('Y-m-d H:i:s')); //createdAt wasn't working
                $ecard->setIsSent(0);
                $ecard->save();
                
                //set up downloadable links
                
                foreach($itemIds as $orderItemId) {
                    $linkCollection = Mage::getModel('downloadable/link_purchased_item')
                            ->getCollection()
                                ->addFieldToFilter('order_item_id', array('eq' => $orderItemId))
                                ->addFieldToFilter('is_for_ecard', array('eq' => 1))
                                ->addFieldToFilter('ecard_unique_id', array('null' => true));
                    
                    $foundMobi = false;
                    $foundEpub = false;
                    foreach($linkCollection as $linkItem) {
                        $title = strtolower($linkItem->getLinkTitle());
                        if(!$foundMobi) {
                            if(strpos($title, 'mobi') !== false) {
                                $linkItem->setEcardUniqueId($uniqueId);
                                $linkItem->save();
                                $foundMobi = true;
                            }
                        } else if (!$foundEpub) {
                            if(strpos($title, 'epub') !== false) {
                                $linkItem->setEcardUniqueId($uniqueId);
                                $linkItem->save();
                                $foundEpub = true;
                            }
                        }
                        if($foundEpub == true && $foundMobi == true) {
                            break;
                        }
                        
                    }
                }
                
            } else {
                Mage::throwException("Ecard should have additional options");
            }
        }
    }
    
    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer) {
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $orderItem = $observer->getOrderItem();
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
    }
}