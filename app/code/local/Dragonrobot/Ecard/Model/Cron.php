<?php
class Dragonrobot_Ecard_Model_Cron{	
    public function SendEmail(){
         $ecardCollection = Mage::getModel('ecard/ecard')
                 ->getCollection()
                 ->addFieldToFilter('send_date', array(
                     'to' => date('Y-m-j')
                 ))->addFieldToFilter('is_sent', array(
                     'neq' => "1"
                 ));
         
         foreach($ecardCollection as $ecard) {
             //send email
            
            $templateId = 30;  // you must create this template
            
            // Set sender information
            
            $senderName = Mage::getStoreConfig('trans_email/ident_support/name');
            //$senderName = Mage::getStoreConfig('trans_email/ident_support/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');
            $sender = array('name' => $senderName,
                        'email' => $senderEmail);
            $recipientName = $ecard->getRecipientName();
            $recipientEmail = $ecard->getRecipientEmail();

            // Get Store ID
            $storeId = Mage::app()->getStore()->getId();

            // Set variables that can be used in email template
            $vars = array(
                'senderName' => $ecard->getName(),
                'recipientName' => $recipientName,
                'uniqueId' => $ecard->getUniqueId()
            );
//            try
//            {
////            $vars["product_image"] = Mage::helper('catalog/image')->init($product, 'small_image')->resize(306)->setQuality(99);
//            }catch( Exception $e )
//            {
//                    // unable to set image
//            }

            $translate  = Mage::getSingleton('core/translate');

            // Send Transactional Email
            Mage::getModel('core/email_template')
                ->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $vars, $storeId);

            $translate->setTranslateInline(true);
             //set is sent to true
             
            $ecard->setIsSent(true);
            $ecard->save();
            
            //subscribe them to the newsletter.. should hook into mailchimp if setup
            $subscriber = Mage::getModel('newsletter/subscriber')->setStoreId($storeId)
                    ->loadByEmail($recipientEmail);
            if ($subscriber->getId()) {
                $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED)
                        ->save();
            } else {
                Mage::getModel('newsletter/subscriber')
                        ->setStoreId($storeId)
                        ->setImportMode(TRUE)
                        ->subscribe($recipientEmail);
            }
         }
         
    } 
}