<?php
class Dragonrobot_Ecard_Model_Downloadable_Observer extends Mage_Downloadable_Model_Observer {
    public function saveDownloadableOrderItem($observer) {
        $orderItem = $observer->getEvent()->getItem();

        if (!$orderItem->getId()) {
            return $this;
        }

        $product = $orderItem->getProduct();

        if ($product && $product->getTypeId() != Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE) {
            return $this;
        }

        if (Mage::getModel('downloadable/link_purchased')->load($orderItem->getId(), 'order_item_id')->getId()) {
            return $this;
        }

        if (!$product) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($orderItem->getOrder()->getStoreId())
                ->load($orderItem->getProductId());
        }

        if ($product->getTypeId() == Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE) {
            $links = $product->getTypeInstance(true)->getLinks($product);

            if ($linkIds = $orderItem->getProductOptionByCode('links')) {
                $linkPurchased = Mage::getModel('downloadable/link_purchased');
                Mage::helper('core')->copyFieldset(
                    'downloadable_sales_copy_order',
                    'to_downloadable',
                    $orderItem->getOrder(),
                    $linkPurchased
                );

                Mage::helper('core')->copyFieldset(
                    'downloadable_sales_copy_order_item',
                    'to_downloadable',
                    $orderItem,
                    $linkPurchased
                );

                $linkSectionTitle = (
                    $product->getLinksTitle()?
                    $product->getLinksTitle():Mage::getStoreConfig(Mage_Downloadable_Model_Link::XML_PATH_LINKS_TITLE)
                );

                $linkPurchased->setLinkSectionTitle($linkSectionTitle)->save();

                foreach ($linkIds as $linkId) {
                    if (isset($links[$linkId])) {
                        $linkPurchasedItem = Mage::getModel('downloadable/link_purchased_item')
                            ->setPurchasedId($linkPurchased->getId())
                            ->setOrderItemId($orderItem->getId());

                        Mage::helper('core')->copyFieldset(
                            'downloadable_sales_copy_link',
                            'to_purchased',
                            $links[$linkId],
                            $linkPurchasedItem
                        );

                        $linkHash = strtr(base64_encode(microtime() . $linkPurchased->getId() . $orderItem->getId()
                            . $product->getId()), '+/=', '-_,');
                        $numberOfDownloads = $links[$linkId]->getNumberOfDownloads()*$orderItem->getQtyOrdered();
                        $linkPurchasedItem->setLinkHash($linkHash)
                            ->setNumberOfDownloadsBought($numberOfDownloads)
                            ->setStatus(Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING)
                            ->setCreatedAt($orderItem->getCreatedAt())
                            ->setUpdatedAt($orderItem->getUpdatedAt())
                            ->save();

                        if (!$ebooks = Mage::registry('expected-ebook-links')) {
                            $ebooks = Mage::helper('ecard')->makeCountOfExpectedEbookLinks();
                            Mage::register('expected-ebook-links', $ebooks);
                        }

                        $sku = $orderItem->getProduct()->getSku();

                        if ($book = Mage::helper('ecard')->isEbook($sku)) {
                            $title = strtolower($linkPurchasedItem->getLinkTitle());
                            if (strpos($title, 'epub') !== false) {
                                $type = 'epub';
                            } elseif (strpos($title, 'mobi') !== false) {
                                $type = 'mobi';
                            } else {
                                throw new Exception("cannot figure out type of file for ebook");
                            }

                            if ($ebooks[$book][$type] > 0) {
                                //then create a link and reduce the other link.
                                $y = $ebooks[$book][$type];
                                $num = $linkPurchasedItem->getNumberOfDownloadsBought();

                                for ($x = 0; $x < $y; $x++ ) {
                                    if ($num > 0) {
                                        $linkHash = strtr(base64_encode(microtime() . $ebooks[$book][$type] . $linkPurchasedItem->getId() . $orderItem->getId()
                                    .       $product->getId()), '+/=', '-_,');
                                        $newLinkPurchasedItem = Mage::getModel('downloadable/link_purchased_item')
                                            ->setPurchasedId($linkPurchasedItem->getPurchasedId())
                                            ->setOrderItemId($linkPurchasedItem->getOrderItemId())
                                            ->setProductId($linkPurchasedItem->getProductId())
                                            ->setLinkHash($linkHash)
                                            ->setNumberOfDownloadsBought(1)
                                            ->setLinkId($linkPurchasedItem->getLinkId())
                                            ->setLinkTitle($linkPurchasedItem->getLinkTitle())
                                            ->setIsForEcard(1)
                                            ->setIsShareable($linkPurchasedItem->getIsShareable())  //should be 2, also dont forget to set in admin that links are sharable
                                            ->setLinkUrl($linkPurchasedItem->getLinkUrl())
                                            ->setLinkFile($linkPurchasedItem->getLinkFile())
                                            ->setLinkType($linkPurchasedItem->getLinkType())
                                            ->setStatus(Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING)
                                            ->setCreatedAt($linkPurchasedItem->getCreatedAt())
                                            ->setUpdatedAt($linkPurchasedItem->getUpdatedAt())
                                            ->save();
                                        $linkPurchasedItem->save();

                                        $ebooks[$book][$type]--;
                                    }
                                 }
                            }
                        }

                        Mage::unregister('expected-ebook-links');
                        Mage::register('expected-ebook-links', $ebooks);
                    }
                }
            }
        }

        return $this;
    }
}
