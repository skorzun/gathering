<?php
class Dragonrobot_Ecard_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	$update = $this->getLayout()->getUpdate();
        $update->addHandle('default');
        $this->addActionLayoutHandles();
        $update->addHandle('ecard');
        $this->loadLayoutUpdates();
        $this->generateLayoutXml();
        $this->generateLayoutBlocks();
        $this->_isLayoutLoaded = true; 
        
      $this->getLayout()->getBlock("head")->setTitle($this->__("The Gathering Of Friends"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("eCard"),
                "title" => $this->__("eCard")
		   ));

      $this->renderLayout(); 
	  
    }
    
    public function loginAction() {
        Mage::getSingleton('core/session')->addNotice('You should log in or register before creating an eCard.');
        $this->_redirect('customer/account/login/referer/'
            . Mage::helper('core')->urlEncode($this->_getRefererUrl()));
    }
    
    public function processorAction() {
        for($x = 0; $x <= 9; $x++) {
            if(isset($_POST['toEmailField' . $x])) {
//                $fromNameField = Mage::getSingleton('customer/session')->getCustomer()->getName();
//                $fromEmailField = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if (isset($_POST["fromNameField"])) $fromNameField = $_POST["fromNameField"];
                if (isset($_POST["fromEmailField"])) $fromEmailField = $_POST["fromEmailField"];
                if (isset($_POST["toNameField" . $x])) $toNameField = $_POST["toNameField" . $x];
                if (isset($_POST["toEmailField" . $x])) $toEmailField = $_POST["toEmailField" . $x];
                if (isset($_POST["cardSize"])) $cardSize = $_POST["cardSize"];	
                if (isset($_POST["cardNumber"])) $cardNumber = $_POST["cardNumber"];
                if (isset($_POST["isOpeningCard"])) $isOpeningCard = $_POST["isOpeningCard"];
                if (isset($_POST["cardText"])) $cardText = $_POST["cardText"];	
                if (isset($_POST["fontFamily"])) $fontFamily = $_POST["fontFamily"];	
                if (isset($_POST["fontSize"])) $fontSize = $_POST["fontSize"];	
                if (isset($_POST["fontWeight"])) $fontWeight = $_POST["fontWeight"];	
                if (isset($_POST["fontStyle"])) $fontStyle = $_POST["fontStyle"];	
                if (isset($_POST["fontColor"])) $fontColor = $_POST["fontColor"];	
                if (isset($_POST["textAlign"])) $textAlign = $_POST["textAlign"];
                if (isset($_POST["cardBooks"])) $cardBooks = $_POST["cardBooks"];
                if (isset($_POST["itemId"])) $itemId = $_POST["itemId"];
                $carddata =  "var fromNameField = '".$fromNameField."';\nvar fromEmailField = '".$fromEmailField."';\nvar toNameField = '".$toNameField."';\nvar toEmailField = '".$toEmailField."';\nvar cardSize = ".$cardSize.";\nvar cardNumber = ".$cardNumber.";\nvar isOpeningCard = ".$isOpeningCard.";\nvar fontFamily = '".str_replace("'", '"', $fontFamily)."';\nvar fontSize = '".$fontSize."';\nvar fontWeight = '".$fontWeight."';\nvar fontStyle = '".$fontStyle."';\nvar fontColor = '".$fontColor."';\nvar textAlign = '".$textAlign."';";

                $carddata .= "\nvar cardBooks = new Array();";
                for ($i = 0; $i < count($cardBooks); $i++)
                    $carddata .= "\ncardBooks[".$i."] = '".$cardBooks[$i]."';";

                $carddata .= "\nvar cardText;";
                
                if(isset($_POST['toSendDate' . $x])) {
                    $time = $_POST['toSendDate' . $x];
                    if($_POST['toSendDate' . $x] == "") {
                        $time = date("Y-m-d H:i:s");
                    }
                    $time = strtotime($time);
                    $date = date("Y-m-d", $time);
                    
                }
                
                $product = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku('ecard'));
                $qty = 1;
                $quote = Mage::getSingleton('checkout/session')->getQuote();
                
                if(isset($itemId)) {
                    //deletes the previous ecard so it can be updated
                    $oldItem = $quote->getItemById($itemId);
                    $qtyOld = $oldItem->getQty();
                    if($qtyOld == 1) {
                        $quote->removeItem($itemId);
                    }
                    else {
                        $oldItem->setQty($qtyOld - 1);
                    }
                }
                
                $item = $quote->addProduct($product, $qty);
                
                 // add to the additional options array
                $additionalOptions = array();
                if ($additionalOption = $product->getCustomOption('additional_options')) {
                    $additionalOptions = (array) unserialize($additionalOption->getValue());
                }
//                $cardText = $action->getRequest()->getParam('cardText');
                
                $additionalOptions[] = array(
                    'label' => "eCard Name",
                    'value' => Mage::getSingleton('customer/session')->getCustomer()->getName(),
                );
                
                $additionalOptions[] = array(
                    'label' => "eCard Email",
                    'value' => Mage::getSingleton('customer/session')->getCustomer()->getEmail(),
                );
                
                $additionalOptions[] = array(
                    'label' => "eCard Recipient Name",
                    'value' => $toNameField,
                );
                
                $additionalOptions[] = array(
                    'label' => "eCard Recipient Email",
                    'value' => $toEmailField,
                );
                
                $additionalOptions[] = array(
                    'label' => "eCard Text",
                    'value' => $cardText,
                );

                $additionalOptions[] = array(
                    'label' => "eCard Data",
                    'value' => $carddata,
                );
                
                $additionalOptions[] = array(
                    'label' => "eCard Number",
                    'value' => $cardNumber,
                );
                
                $additionalOptions[] = array(
                    'label' => "eCard Send Date",
                    'value' => $date,
                );
                
//                $additionalOptions[] = array(
//                    'label' => "eCard Is Sent",
//                    'value' => false,
//                );
                $books = array();
                foreach($cardBooks as $imagePath) {
                    $index = strpos($imagePath, 'book_' );
                    $book = str_replace("_","", substr($imagePath, $index, 6));
                    $books[] = $book;
                }
                
                $additionalOptions[] = array(
                    'label' => "eCard Associated Products",
                    'value' => implode(",", $books)
                );
                
                $additionalOptions[] = array(
                    'label' => "eCard Associated Items",
                    'value' => "",
                );

                // add the additional options array with the option code additional_options
                $item->addOption(new Varien_Object(
                    array(
                        'product'   => $item->getProduct(),
                        'code'      => 'additional_options',
                        'value'     => serialize($additionalOptions)
                    )
                ));

                $quote->collectTotals()->save();

                $listId = Mage::helper('monkey')->config('list');
                $email = $toEmailField;
                $mergeVars = array('FNAME' => $toNameField);
                $isConfirmNeed = FALSE;
                Mage::getSingleton('monkey/api')->listSubscribe($listId, $email, $mergeVars, 'html', $isConfirmNeed, TRUE);
            }
        }
        //echo $ecard->getId() . "," . Mage::getSingleton('customer/session')->getCustomer()->getEmail();
    }
    
    public function displaycardAction(){
        $ecardCollection = Mage::getModel('ecard/ecard')->getCollection();
        $uid = $this->getRequest()->getParam('uid');
        $ecardCollection->addFieldToFilter('unique_id', array('eq'=> $uid));
        $found = false;
        foreach($ecardCollection as $ecard) {
            $found = true;
            Mage::register('ecard-data', $ecard->getData('data'));
        
            Mage::register('ecard-object', $ecard);
        }
        
        if(!$found) {
            Mage::throwException("Cannot find an eCard with that Unique Id!");
        }
        
        
        
        $update = $this->getLayout()->getUpdate();
        $update->addHandle('default');
        $this->addActionLayoutHandles();
        $update->addHandle('ecard');
        $this->loadLayoutUpdates();
        $this->generateLayoutXml();
        $this->generateLayoutBlocks();
        $this->_isLayoutLoaded = true; 
        
	$this->getLayout()->getBlock("head")->setTitle($this->__("The Gathering Of Friends"));
	$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
                    "label" => $this->__("Home Page"),
                    "title" => $this->__("Home Page"),
                    "link"  => Mage::getBaseUrl()
                       ));

        $breadcrumbs->addCrumb("titlename", array(
                    "label" => $this->__("eCard"),
                    "title" => $this->__("eCard")
                       ));

        $this->renderLayout();
        
        
        
    }
    
    public function downloadgiftAction() {
        $this->displaycardAction();
    }
    
    public function getCartContentsAction(){
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $items = $quote->getItemsCollection();
        $data = array();
        $data['items'] = array();
        $data['ecards'] = array(
            'created' => array(),
            'empty' => 0
        );
        foreach($items as $item) {
            $data['items'][] = array(
                'sku' => $item->getSku(),
                'name' => $item->getName(),
                'qty' => $item->getQty(),
                'price' => $item->getPrice()
            );
            if($item->getSku() == 'ecard') {
                $additionalOptions = $item->getOptionByCode('additional_options');
                if(isset($additionalOptions)) {
                    $unserialized = unserialize($additionalOptions->getValue());
                    $options = array('name', 'email', 'text', 'data','sendDate', 'isSent', 'associatedProducts');
                    foreach($unserialized as $option) {
                        switch ($option['label']) {
                            case 'eCard Associated Products':
                                $data['ecards']['created'][] = $option['value'];
                                break;
                        }
                    }
                    
                } else {
                    $data['ecards']['empty'] ++;
                }
            }
        }
        //$quote->collectTotals()->save();
        $totals = $quote->getTotals();
        $data['totals'] = array(
            'grand_total' => $totals['grand_total']->getValue(),
            'subtotal' => $totals['subtotal']->getValue(),
        );
        echo json_encode($data);
        exit;
    }
    
    public function addSkuToCartAction() {
        $sku = isset($_POST['sku']) ? $_POST['sku']:null;
        if(is_null($sku)){
            echo json_encode(false);
            exit;
        }
        
        $shouldBeGrouped = false;
        $skusThatShouldBeGrouped = Mage::helper('ecard')->getSkusThatShouldBeGrouped();
        $actualSkusThatShouldBeGrouped = array_keys($skusThatShouldBeGrouped);
        $storeId = Mage::app()->getStore()->getId();
        if(in_array($sku, $actualSkusThatShouldBeGrouped)) {
            $shouldBeGrouped = true;
            $parentSku = $skusThatShouldBeGrouped[$sku];
            $parentId = Mage::getModel('catalog/product')->getIdBySku($parentSku);
            $childId = Mage::getModel('catalog/product')->getIdBySku($sku);
            $superGroup = array($childId => 1);
            $params = array('super_group' => $superGroup);
            
            $qty = $params;
            $product = Mage::getModel('catalog/product')->load($parentId)->setStoreId($storeId);
        } else {
            $product = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($sku))->setStoreId($storeId);
            $qty = 1;
        }
        
        $cart = Mage::getModel('checkout/cart');
        $cart->addProduct($product, $qty)->save();
        Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
        echo json_encode(true);
        exit;
    }

    private function updateCartRowTotal()
    {
        // KL: Fix for the wrong calculation
        // Update the items_qty in sales_flat_quote
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $_item_qty = 0;

        foreach ($quote->getItemsCollection() as $item) {
            $_item_qty = $_item_qty + $item->getData('qty');
            $item->calcRowTotal();
            $item->save();

            $tax_amount = $item->getData('tax_amount');
            $base_tax_amount = $item->getData('base_tax_amount');
            $row_total = $item->getData('row_total');
            $base_row_total = $item->getData('base_row_total');
            $item->setData('row_total_incl_tax', $tax_amount + $row_total);
            $item->setData('base_row_total_incl_tax', $base_tax_amount + $base_row_total);
            //$item->calcTaxAmount();
            $item->save();
        }

        $quote->setData('items_qty', $_item_qty);
        $quote->save();

        $quote->collectTotals();
        $quote->save();
    }

    public function addSkusToCartAction() {
        $skus = isset($_POST['skus']) ? $_POST['skus']:null;
        if(is_null($skus) || !is_array($skus)){
            $this->getCartContentsAction();
            //exit;
        }
        $results = array();
        $storeId = Mage::app()->getStore()->getId();

        $skusThatShouldBeGrouped = Mage::helper('ecard')->getSkusThatShouldBeGrouped();
        $actualSkusThatShouldBeGrouped = array_keys($skusThatShouldBeGrouped);

        foreach($skus as $sku) {
            $shouldBeGrouped = false;
            if(in_array($sku, $actualSkusThatShouldBeGrouped)) {
                $shouldBeGrouped = true;
                $parentSku = $skusThatShouldBeGrouped[$sku];
                $parentId = Mage::getModel('catalog/product')->getIdBySku($parentSku);
                $childId = Mage::getModel('catalog/product')->getIdBySku($sku);
                $superGroup = array($childId => 1);
                $params = array('super_group' => $superGroup);

                $qty = $params;
                $product = Mage::getModel('catalog/product')->load($parentId)->setStoreId($storeId);
                $product_id = $childId;
            } else {
                $product = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($sku))->setStoreId($storeId);
                $product_id = $product->getId();
                $qty = 1;
            }

            // We only do add if the product is not yet in the cart
            $quote = Mage::getSingleton('checkout/session')->getQuote();
            if (!$quote->hasProductId($product_id)) {
                $cart = Mage::getModel('checkout/cart');
                $cart->init();
                $cart->addProduct($product, $qty);
                $cart->save();
                unset($cart);
                Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
            } else {
                $cartItems = $quote->getItemsCollection();
                foreach ($cartItems as $item) {
                    if ($product_id == $item->getProductId()) {
                        $quote->updateItem($item->getId(), array('qty' => $item->getQty() + 1));
                        $quote->save();
                    }
                }
            }
        }


        $this->updateCartRowTotal();
        $this->getCartContentsAction();

    }
    
    public function _addSkusToCartAction() {
        $skus = isset($_POST['skus']) ? $_POST['skus']:null;
        if(is_null($skus) || !is_array($skus)){
            $this->getCartContentsAction();
            //exit;
        }
        $results = array();
        $storeId = Mage::app()->getStore()->getId();
        
        $skusThatShouldBeGrouped = Mage::helper('ecard')->getSkusThatShouldBeGrouped();
        $actualSkusThatShouldBeGrouped = array_keys($skusThatShouldBeGrouped);
        
        foreach($skus as $sku) {
            $shouldBeGrouped = false;
            if(in_array($sku, $actualSkusThatShouldBeGrouped)) {
                $shouldBeGrouped = true;
                $parentSku = $skusThatShouldBeGrouped[$sku];
                $parentId = Mage::getModel('catalog/product')->getIdBySku($parentSku);
                $childId = Mage::getModel('catalog/product')->getIdBySku($sku);
                $superGroup = array($childId => 1);
                $params = array('super_group' => $superGroup);

                $qty = $params;
                $product = Mage::getModel('catalog/product')->load($parentId)->setStoreId($storeId);
                $product_id = $childId;
            } else {
                $product = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($sku))->setStoreId($storeId);
                $product_id = $product->getId();
                $qty = 1;
            }
            $cart = Mage::getModel('checkout/cart');
            $cart->init();
            $cart->addProduct($product, $qty);
            $cart->save();
            unset($cart);
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
        }


        $this->updateCartRowTotal();
        $this->getCartContentsAction();

    }
    
    public function removeSkuFromCartAction() {
        $sku = isset($_POST['sku']) ? $_POST['sku']:null;
        if(is_null($sku)){
            echo json_encode(false);
            exit;
        }
        $product = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($sku));
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        
        foreach ($quote->getItemsCollection() as $item) {
            if ($item->getProductId() == $product->getId()) {
                $qty = $item->getQty();

                if($qty == 1) {
                    $quote->removeItem($item->getId());

                }
                else {
                    $item->setQty($qty - 1);
                }   
                $quote->save();
                echo json_encode(true);
                exit;
            }
        }
            echo json_encode(false);
        exit;
        
    }
    public function removeSkusFromCartAction() {
        $skus = isset($_POST['skus']) ? $_POST['skus']:null;
        if(is_null($skus) || !is_array($skus)){
            $this->getCartContentsAction();
            //exit;
        }
        $result = array();
        foreach($skus as $sku) {
            $foundSku = false;
            $product = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($sku));
            $quote = Mage::getSingleton('checkout/session')->getQuote();

            foreach ($quote->getItemsCollection() as $item) {
                if ($item->getProductId() == $product->getId()) {
                    $qty = $item->getQty();

                    if($qty == 1) {
                        $quote->removeItem($item->getId());

                    }
                    else {
                        $item->setQty($qty - 1);
                    }   
                    $quote->save();
                    $foundSku = true;
                    $result[] = array('sku' => $sku, 'success' => true);
                }
            }
            if(!$foundSku) {
                $result[] = array('sku' => $sku, 'success' => false);
            }
       }
       $this->updateCartRowTotal();
       $this->getCartContentsAction();
//       echo json_encode($result);
//       exit;
    }
}
