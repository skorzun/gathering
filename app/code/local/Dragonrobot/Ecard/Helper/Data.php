<?php
class Dragonrobot_Ecard_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isEbook($sku) {
        $skus = Mage::helper('ecard')->getEbookSkus();
        foreach($skus as $book => $bookskus) {
            if(in_array($sku, $bookskus)) {
                return $book;
            }
        }
        return false;
    }
    
    public function getSkusThatShouldBeGrouped() {
        $skusThatShouldBeGrouped = array(
            'vol-one-any' => 'vol-one-ebook', // sku of downloadable product => sku of grouped product it should be grouped to
            'vol-two-any' => 'vol-two-ebook',
            'vol-three-any' => 'vol-three-ebook',
            'vol-four-any' => 'vol-four-ebook',
            'vol-five-any' => 'vol-five-ebook',
            'vol-six-any' => 'vol-six-ebook',
            'vol-seven-any' => 'vol-seven-ebook',
            'all-ebooks-any' => 'all-ebooks',
        );
        
        return $skusThatShouldBeGrouped;
    }
    
    public function getEbookSkus() {
        $bookSkus = array(
            "book1" => array("vol-one-epub", "vol-one-mobi", "vol-one-any", "ebook-volume-1"),
            "book2" => array("vol-two-epub", "vol-two-mobi", "vol-two-any"),
            "book3" => array("vol-three-epub", "vol-three-mobi", "vol-three-any"),
            "book4" => array("vol-four-epub", "vol-four-mobi", "vol-four-any"),
            "book5" => array("vol-five-epub", "vol-five-mobi", "vol-five-any"),
            "book6" => array("vol-six-epub", "vol-six-mobi", "vol-six-any"),
            "book7" => array("vol-seven-epub", "vol-seven-mobi", "vol-seven-any"),
            "bookAll" => array("all-ebooks-epub", "all-ebooks-mobi", "all-ebooks-any"),
        );
        return $bookSkus;
    }
    
    public function getCartCount($items) {
        
        $ebooksLinked = 0;
        $ebooks = 0;
        foreach($items as $item) {
            $sku = $item->getSku();
            if($sku == "ecard") {
                $options = $item->getOptionsByCode();
                foreach(unserialize($options['additional_options']->getValue()) as $option) {
                    switch($option['label']) {
                        case 'eCard Associated Products':
                            $books = explode(",", $option['value']);
                            $ebooksLinked += count($books);
                            break;
                    }
                }
            } else if($this->isEbook($sku)){
                $ebooks += $item->getQty();
            }
        }
        
        return "<span>$ebooksLinked</span> OF <span>$ebooks</span>";
    }
    
    public function canMakeCard() {
        return true;
    }
    
    public function getCardNumber($item) {
        $options = $item->getOptionsByCode();
        foreach(unserialize($options['additional_options']->getValue()) as $option) {
            switch($option['label']) {
                case 'eCard Number':
                    return $option['value'];
            }
        }
    }
    
    public function makeAssociatedProductsPretty($optionValue){
        $products = explode(',', $optionValue);
        
        foreach($products as &$product) {
            switch($product) {
                case 'book1':
                    $product = str_replace("book1", "Volume One eBook", $product);
                    break;
                case 'book2':
                    $product = str_replace("book2", "Volume Two eBook", $product);
                    break;
                case 'book3':
                    $product = str_replace("book3", "Volume Three eBook", $product);
                    break;
                case 'book4':
                    $product = str_replace("book4", "Volume Four eBook", $product);
                    break;
                case 'book5':
                    $product = str_replace("book5", "Volume Five eBook", $product);
                    break;
                case 'book6':
                    $product = str_replace("book6", "Volume Six eBook", $product);
                    break;
                case 'book7':
                    $product = str_replace("book7", "Volume Seven eBook", $product);
                    break;
                case 'bookAll':
                    $product = str_replace("bookAll", "Volume 1-7 eBooks", $product);
                    break;
            }
            
        }
        return implode("<br/>", $products);
    }
    
    public function makeCountOfExpectedEbookLinks() {
        $ebooks = array(
            'book1' => array(
                'epub' => 0,
                'mobi' => 0
            ),
            'book2' => array(
                'epub' => 0,
                'mobi' => 0
            ),
            'book3' => array(
                'epub' => 0,
                'mobi' => 0
            ),
            'book4' => array(
                'epub' => 0,
                'mobi' => 0
            ),
            'book5' => array(
                'epub' => 0,
                'mobi' => 0
            ),
            'book6' => array(
                'epub' => 0,
                'mobi' => 0
            ),
            'book7' => array(
                'epub' => 0,
                'mobi' => 0
            ),
        );
        $cart = Mage::getModel('checkout/cart')->getQuote();
        foreach ($cart->getAllItems() as $item) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
            $type = $product->getTypeId();
            if($type == "ecard") {
                $additionalOptions = $item->getOptionByCode('additional_options');
                if(isset($additionalOptions)) {
                    $unserialized = unserialize($additionalOptions->getValue());
                    $itemIds = array();
                    foreach($unserialized as $option) {
                        switch ($option['label']) {
                            case 'eCard Associated Products':
                                $associatedProducts = explode(",", $option['value']);
                                foreach($associatedProducts as $book) {
                                    $ebooks[$book]['epub']++;
                                    $ebooks[$book]['mobi']++;
                                    
                                }
                                break;
                        }
                    }
                }
            }
        }
        return $ebooks;
    }
}
	 