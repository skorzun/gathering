<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
ALTER TABLE  `downloadable_link_purchased_item` ADD  `ecard_unique_id` VARCHAR( 255 ) NULL DEFAULT NULL AFTER  `link_title` ,
ADD INDEX (  `ecard_unique_id` );
ALTER TABLE  `downloadable_link_purchased_item` ADD  `is_for_ecard` TINYINT NOT NULL DEFAULT  '0' AFTER  `link_title` ,
ADD INDEX (  `is_for_ecard` );
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 