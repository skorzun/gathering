<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE IF NOT EXISTS `ecard` (
  `ecard_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created because created_at wasnt automatic',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `card_number` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `recipient_email` varchar(255) NOT NULL,
  `recipient_name` varchar(255) NOT NULL,
  `send_date` date NOT NULL,
  `name` varchar(255) NOT NULL,
  `associated_items` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `text` text NOT NULL,
  `is_sent` tinyint(1) NOT NULL,
  PRIMARY KEY (`ecard_id`),
  UNIQUE (`unique_id`),
  INDEX (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 