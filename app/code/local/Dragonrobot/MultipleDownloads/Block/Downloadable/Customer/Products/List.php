<?php

class Dragonrobot_MultipleDownloads_Block_Downloadable_Customer_Products_List
    extends Mage_Downloadable_Block_Customer_Products_List {
    private $_itemsIdArray = array();

    public function __construct() {
        parent::__construct();
        $session = Mage::getSingleton('customer/session');
        $purchased = Mage::getResourceModel('downloadable/link_purchased_collection')
            ->addFieldToFilter('customer_id', $session->getCustomerId())
            ->addOrder('created_at', 'desc');
        $this->setPurchased($purchased);
        $purchasedIds = array();

        foreach ($purchased as $_item) {
            $purchasedIds[] = $_item->getId();
        }

        if (empty($purchasedIds)) {
            $purchasedIds = array(null);
        }

        $purchasedItems = Mage::getResourceModel('downloadable/link_purchased_item_collection')
            ->addFieldToFilter('purchased_id', array('in' => $purchasedIds))
            ->addFieldToFilter('status',
                array(
                    'nin' => array(
                        Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING_PAYMENT,
                        Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PAYMENT_REVIEW
                    )
                )
            )
            ->setOrder('item_id', 'desc');

        $handles = Mage::app()->getLayout()->getUpdate()->getHandles();

        if (in_array('ecard', $handles)) {
            $uid = Mage::app()->getRequest()->getParam('uid');
            if (!$uid) {
                Mage::throwException('no uid set?  shouldnt happen');
            }

            $purchasedItems->addFieldToFilter('ecard_unique_id', array('eq' => $uid))
                ->addFieldToFilter('is_for_ecard', array('eq' => 1));
        } else {
            $purchasedItems->addFieldToFilter('is_for_ecard', array('eq' => 0));
        }

        $this->setItems($purchasedItems);
    }

    public function isLinked($item) {
        $linkId = $item->getLinkId();
        $linkCollection = Mage::getModel('downloadable/link')->getCollection();
        $linkCollection->addFieldToFilter('is_linked', array('eq' => $linkId));
        $linkCollection->load();

        if(count($linkCollection) > 0) {
            return true;
        }
    }

    public function getLinkedItems($item) {
        $linkId = $item->getLinkId();
        $orderItemId = $item->getOrderItemId();

        $linkCollection = Mage::getModel('downloadable/link')->getCollection();
        $linkCollection->addFieldToFilter('is_linked', array('eq' => $linkId));

        $itemsIdArray= array();
        $itemsIdArray[] = $linkId;

        foreach ($linkCollection as $link) {
            $itemsIdArray[] = $link->getLinkId();
        }

        $this->_itemsIdArray = array_merge($this->_itemsIdArray, $itemsIdArray);
        $itemsForReturn = array();

        $items = Mage::getResourceModel('downloadable/link_purchased_item_collection')
            ->addFieldToFilter('order_item_id', array('eq' => $orderItemId ))
            ->addFieldToFilter('status',
            array(
                'nin' => array(
                    Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING_PAYMENT,
                    Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PAYMENT_REVIEW
                )
            )
        )
        ->setOrder('item_id', 'asc');

        foreach ($items as $_item) {
            if (in_array($_item->getLinkId(), $itemsIdArray)) {
                if (strtotime($_item->getCreatedAt()) < strtotime(date('04/01/2015'))) {
                    if ($_item->getIsForEcard() == 1) {
                        $itemsForReturn[] = $_item;
                    }
                } else {
                    $itemsForReturn[] = $_item;
                }
            }
        }

        return $itemsForReturn;
    }

    public function alreadyLinked($item) {
        if (in_array($item->getLinkId(), $this->_itemsIdArray)) {
            return true;
        }

        return false;
    }

    public function getFullListItems() {
        $session = Mage::getSingleton('customer/session');
        $purchased = Mage::getResourceModel('downloadable/link_purchased_collection')
            ->addFieldToFilter('customer_id', $session->getCustomerId())
            ->addOrder('created_at', 'desc');
        $this->setPurchased($purchased);
        $purchasedIds = array();

        foreach ($purchased as $_item) {
            $purchasedIds[] = $_item->getId();
        }

        if (empty($purchasedIds)) {
            $purchasedIds = array(null);
        }

        $purchasedItems = Mage::getResourceModel('downloadable/link_purchased_item_collection')
            ->addFieldToFilter('purchased_id', array('in' => $purchasedIds))
            ->addFieldToFilter('status',
                array(
                    'nin' => array(
                        Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING_PAYMENT,
                        Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PAYMENT_REVIEW
                    )
                )
            )
            ->setOrder('item_id', 'desc');

        return $purchasedItems;
    }


    public function getLinkedItemsForLastDownload($item)
    {
        $linkId = $item->getLinkId();
        $orderItemId = $item->getOrderItemId();

        $linkCollection = Mage::getModel('downloadable/link')->getCollection();
        $linkCollection->addFieldToFilter('is_linked', array('eq' => $linkId));

        $itemsIdArray = array();
        $itemsIdArray[] = $linkId;

        foreach ($linkCollection as $link) {
            $itemsIdArray[] = $link->getLinkId();
        }

        $this->_itemsIdArray = array_merge($this->_itemsIdArray, $itemsIdArray);
        $itemsForReturn = array();

        $items = Mage::getResourceModel('downloadable/link_purchased_item_collection')
            ->addFieldToFilter('order_item_id', array('eq' => $orderItemId))
            ->addFieldToFilter('status',
                array(
                    'nin' => array(
                        Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING_PAYMENT,
                        Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PAYMENT_REVIEW
                    )
                )
            )
            ->setOrder('item_id', 'asc');

        foreach ($items as $_item) {
            if (in_array($_item->getLinkId(), $itemsIdArray)) {
                if ($_item->getIsForEcard() == 1) {
                    $itemsForReturn[] = $_item;
                }
            }
        }

        return $itemsForReturn;
    }
}
