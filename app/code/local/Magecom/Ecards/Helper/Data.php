<?php

class Magecom_Ecards_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_code = 'eCard';

    public function getRecipientsQty()
    {
        return Mage::getStoreConfig('ecards/recipients/ecards_recipients');
    }

    public function getIsSpecialPrice()
    {
        //return Mage::getStoreConfig('ecards/price/use_default_price');
        return false;
    }

    public function getSpecialEcardPrice()
    {
        $customEcardPrice = Mage::getStoreConfig('ecards/price/retail_price_bundle');
        $storeCode = Mage::app()->getStore()->getCode();
        if ($storeCode == 'wholesale') {
            $customEcardPrice = Mage::getStoreConfig('ecards/price/wholesale_price_bundle');
        }
        return $customEcardPrice;
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function getCodeId()
    {
        $ecardType = Mage::getSingleton('base/type')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('type_name', $this->getCode());
        return $ecardType->getFirstItem()->getEntityId();
    }

    public function isEcard($productId)
    {
        $_product = Mage::getModel('catalog/product')->load($productId);
        $type = strtolower($_product->getAttributeText('card_type'));
        if ($type == 'ecards') {
            return true;
        } else {
            return false;
        }
    }

	public function getAccountUrl() {
		return Mage::getUrl('ecards/customer');
	}

    public function getRemindDays()
    {
        return Mage::getStoreConfig('ecards/general/remind_day');
    }

    public function getSecondRemindDays()
    {
        return Mage::getStoreConfig('ecards/general/second_remind_day');
    }
}
