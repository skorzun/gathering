<?php

class Magecom_Ecards_ViewController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function downloadsAction() {
        $this->loadLayout();
        $this->renderLayout();
    }
}
