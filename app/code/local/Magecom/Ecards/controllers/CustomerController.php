<?php
class Magecom_Ecards_CustomerController extends Mage_Core_Controller_Front_Action
{
    protected function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function preDispatch()
    {
        parent::preDispatch();
        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
            Mage::getSingleton('core/session')
                ->addSuccess(Mage::helper('ecards')->__('Please sign in or create a new account'));
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('My eCards'));
        $this->renderLayout();
    }

    public function sentAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('My eCards'));
        $this->getLayout()->getBlock('customer_account_navigation')->setActive('ecards/customer/index');
        $this->renderLayout();
    }
}
