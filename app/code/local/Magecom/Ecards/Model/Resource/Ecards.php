<?php

class Magecom_Ecards_Model_Resource_Ecards extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct() {
        $this->_init('ecards/ecards', 'entity_id');
    }
}