<?php

class Magecom_Ecards_Model_Resource_Ecards_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct() {
        $this->_init('ecards/ecards');
    }
}