<?php

class Magecom_Ecards_Model_Observer
{
    public function setNewEcards(Varien_Event_Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            if ($order->getState() != Mage_Sales_Model_Order::STATE_COMPLETE ||
                ($order->getId() == Mage::getSingleton('core/session')->getEcardsVarible()) ) {
                return;
            }
            $order_no = (string)$order->getRealOrderId();
            $order->loadByIncrementId($order_no);

            $orderItems = $order->getAllItems();
            $bSms = false;
            foreach($orderItems as $key => $_virtualItem){
                if(Mage::helper('base')->checkIsSms($_virtualItem)){
                    $bSms = true;
                    unset($orderItems[$key]);
                    break;
                }
            }

            foreach($orderItems as $_virtualItem) {
                if ($_virtualItem->getProductType() == 'virtual') {
                    $prod = Mage::getSingleton('catalog/product')->load($_virtualItem->getProduct()->getId());
                    $type = strtolower($prod->getAttributeText('card_type'));
                    if ($type != 'ecards') {
                        continue;
                    }
                    $qty = $_virtualItem->getQtyOrdered() * Mage::helper('ecards')->getRecipientsQty();

                    $ecard = Mage::getSingleton('ecards/ecards')->getCollection()
                        ->addFieldToSelect('*')
                        ->addFieldToFilter('customer_id', $order->getCustomerId())
                        ->addFieldToFilter('ecard_id', $prod->getId())
                        ->addFieldToFilter('is_active', array('in' => 1));

                    //===============================================================

                    $itemOptions = $_virtualItem->getProductOptions();
                    if(isset($itemOptions['additional_options']['send_number'])) {
                        $ecardSendId = $itemOptions['additional_options']['send_number']['value'];
                        if (!empty($ecardSendId)) {
                            $ecardSend = Mage::getSingleton('base/send')->load($ecardSendId);
                        } else {
                            continue;
                        }

                        if($ecardSend->getStatus() == '2') {
                            $orderItemIds = array();
                            foreach ($orderItems as $_downloadableItem) {
                                if ($_downloadableItem->getProductType() == 'downloadable') {
                                    $orderItemIds[] = $_downloadableItem->getItemId();
                                }
                            }

                            if ($ecardSend->getId()) {
                                if(!empty($orderItemIds)){
                                    $ecardSend->setOrderItemIds(json_encode($orderItemIds));
                                }
                                $ecardSend->setIsActive(1);
                                $ecardSend->setStatus(0);
                                $ecardSend->setIsReminder(1);
                                $ecardSend->save();
                                Mage::helper('base')->sendEmail();
                            }
                        }
                    } else {
                        if($ecard->getSize() > 0) {
                            $ecard->getFirstItem()->setCustomerId($order->getCustomerId())
                                ->setEcardId($prod->getId())
                                ->setCount($ecard->getFirstItem()->getCount() + $qty)
                                ->setIsActive(1);
                            if($bSms){
                                $ecard->setSms($ecard->getFirstItem()->getSms() + $qty);
                            }
                        } else {
                            $ecard = Mage::getModel('ecards/ecards');
                            $ecard->setCustomerId($order->getCustomerId())
                                ->setEcardId($prod->getId())
                                ->setCount($qty)
                                ->setIsActive(1);
                            if($bSms){
                                $ecard->setSms($qty);
                            }
                        }
                        $ecard->save();
                    }

                    //===============================================================

                    Mage::getSingleton('core/session')->setEcardsVarible($order->getId());
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'ecards_observer.log', true);
        }
    }

    public function applyEcardPrice($observer)
    {
        if (Mage::helper('ecards')->getIsSpecialPrice() == 1) {
            $event = $observer->getEvent();
            $product = $event->getProduct();
            if ( ($product->getTypeId() == 'virtual')
                && (strtolower($product->getAttributeText('card_type')) == 'ecards') ) {
                if ($product->getSuperProduct() && $product->getSuperProduct()->isConfigurable()) {
                } else {
                    $product->setFinalPrice(Mage::helper('ecards')->getSpecialEcardPrice());
                }
            }
        }
        return $this;
    }

    public function applyEcardCollectionPrice($observer)
    {
        if (Mage::helper('ecards')->getIsSpecialPrice() == 1) {
            $myCustomPrice = Mage::helper('ecards')->getSpecialEcardPrice();
            $products = $observer->getCollection();
            foreach( $products as $product ) {
                $prod = Mage::getSingleton('catalog/product')->load($product->getId());
                if ( ($product->getTypeId() == 'virtual')
                    && (strtolower($prod->getAttributeText('card_type')) == 'ecards') ) {
                    $product->setPrice( $myCustomPrice );
                    $product->setFinalPrice( $myCustomPrice );
                }
            }
        }
        return $this;
    }
}
