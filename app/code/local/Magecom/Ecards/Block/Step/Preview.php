<?php

class Magecom_Ecards_Block_Step_Preview extends Mage_Core_Block_Template
{
    protected $_ecard;
    protected $_gifts;

    protected function _construct() {
        $this->setTemplate('magecom/ecards/steps/preview.phtml');
        if (!$this->_ecard = Mage::registry('ecard')) {
            $this->_ecard = Mage::getModel('catalog/product')->load($this->getData('ecard_id'));
        }
        $this->_gifts = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => $this->getData('ids')));
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getMessage() {
        $message = $this->getData('message');
        return (!empty($message)) ? $this->getData('message') : 'Message will be here';
    }

    public function getGiftsList() {
        return $this->_gifts;
    }

    public function getStyle() {
        return $this->getData('style');
    }

    public function getExampleName() {
        $aParam = Mage::app()->getRequest()->getPost();
        if(isset($aParam['ecard_to'][0]['name'])){
            $recipientName = $aParam['ecard_to'][0]['name'];
        }
        return (!empty($recipientName)) ?  $recipientName : '';
    }

    /**
     * Get card image
     *
     * @return mixed
     */
    public function getCardImage(){
        return $this->getData('card_image');
    }
}
