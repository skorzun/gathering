<?php

class Magecom_Ecards_Block_Step_Summary extends Mage_Core_Block_Template
{
    protected $_data;

    protected function _construct() {
        $this->setTemplate('magecom/ecards/steps/summary.phtml');
        $this->_data = $this->getData('summary');
    }

    public function getEcard() {
        return Mage::getModel('catalog/product')->load($this->_data['ecard_id']);
    }

    public function getItemQty() {
        return (!empty($this->_data['recipients_method'])) ? $this->_data['ecard_qty'] : !empty($this->_data['ecard_to']) ? count($this->_data['ecard_to']) : 0;
    }

    public function getRecipients() {
        $collection = new Varien_Data_Collection();

        if(!empty($this->_data['ecard_to'])) {
            foreach ($this->_data['ecard_to'] as $data) {
                $_recipient = new Varien_Object();
                $_recipient->setData($data);
                $collection->addItem($_recipient);
            }
        }
        return $collection;
    }

    public function getMethod() {
        return !empty($this->_data['recipients_method']) ? $this->_data['recipients_method'] : null;
    }

    public function getCardType() {
        return $this->getEcard()->getAttributeText('card_type');
    }

    public function getEcardsCount()
    {
        if(!empty($this->_data['ecard_id'])){
            $ecard = Mage::getSingleton('ecards/ecards')->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->addFieldToFilter('ecard_id', $this->_data['ecard_id'])
                ->addFieldToFilter('is_active', array('in' => 1));
            return $ecard->getFirstItem()->getCount();
        }
        return;
    }
}
