<?php

class Magecom_Ecards_Block_Step_Text extends Mage_Core_Block_Template
{
    protected $_ecard;

    protected function _construct() {
        if (!$this->_ecard = Mage::registry('ecard')) {
            $this->_ecard = Mage::getModel('catalog/product')->load($this->getData('ecard_id'));
        }
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getCardType() {
        return $this->getEcard()->getAttributeText('card_type');
    }
}
