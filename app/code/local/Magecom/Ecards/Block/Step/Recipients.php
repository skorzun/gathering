<?php

class Magecom_Ecards_Block_Step_Recipients extends Mage_Core_Block_Template
{
    protected $_ecard;

    protected function _construct() {
        $this->_ecard = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getEcardsCount()
    {
Mage::log(Mage::registry('ecard')->getId());
        $ecard = Mage::getSingleton('ecards/ecards')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('ecard_id', $this->_ecard->getId())
            ->addFieldToFilter('is_active', array('in' => 1));


Mage::log($ecard->getSelect()->__toString());
        return $ecard->getFirstItem()->getCount();
    }
}
