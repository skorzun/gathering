<?php

class Magecom_Ecards_Block_Index extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();

        $orders = Mage::getSingleton('ecards/ecards')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('is_active', array('in' => 1))
            ->addFieldToFilter('count', array('gt' => 0))
            ->setOrder('entity_id', 'desc');

        $this->setOrders($orders);

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('ecards')->__('My Ecards'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'index.ecards.pager')
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

    public function getOrderIncrementId($orderId)
    {
        $order = Mage::getSingleton('sales/order')->load($orderId);
        return $order->getIncrementId();
    }

    public function getProduct($productId)
    {
        return Mage::getSingleton('catalog/product')->load($productId);
    }
}
