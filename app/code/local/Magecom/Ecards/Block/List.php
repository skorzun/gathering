<?php

class Magecom_Ecards_Block_List extends Mage_Core_Block_Template
{
    protected $_parentCategory;

    protected function _getParentCategory() {
        return Mage::registry('current_category');
    }

    public function getChildCategories() {
        $categories = array();
        $category = Mage::getModel('catalog/category')->load($this->_getParentCategory()->getId());
        $children = $category->getChildren();
        if ($children == '') {
            $categories[] = $category;
        } else {
            foreach (explode(',', $children) as $childId) {
                $categories[] = Mage::getModel('catalog/category')->load($childId);
            }
        }
        return $categories;
    }
}
