<?php

class Magecom_Ecards_Block_Ecard extends Mage_Core_Block_Template
{
    protected $_ecard;

    protected function _construct() {
        $this->_ecard = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getCardType() {
        return $this->getEcard()->getAttributeText('card_type');
    }
}
