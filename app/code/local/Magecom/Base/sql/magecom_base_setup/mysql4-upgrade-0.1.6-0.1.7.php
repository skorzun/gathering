<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'is_reminder', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable'  => false,
            'default'   => 1,
            'comment'   => 'Is needed second reminder'
        )
    );

$installer->endSetup();
