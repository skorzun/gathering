<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable('magecom_ecard_type')
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('type_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
    ), 'eCard Type Name');


$installer->getConnection()->createTable($table);

$installer->endSetup();
