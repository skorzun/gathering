<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->modifyColumn($installer->getTable('base/send'), 'phone_number', array(
            'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'phone number',
        )
    );

$installer->endSetup();
