<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'image', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => false,
            'default'   => NULL,
            'comment'   => 'Left Side Card Image',
        )
    );

$installer->endSetup();
