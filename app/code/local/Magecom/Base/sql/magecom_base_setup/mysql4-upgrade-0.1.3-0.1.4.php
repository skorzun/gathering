<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'ecard_type_id', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable'  => false,
            'default'   => 1,
            'comment'   => 'Type Id',
        )
    );

$installer->getConnection()
    ->addForeignKey(
        $installer->getFkName(
            'base/send',  'ecard_type_id',
            'base/type',  'entity_id'
        ),
        $installer->getTable('base/send'),
        'ecard_type_id',
        $installer->getTable('base/type'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

$installer->endSetup();
