<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('ecards/ecards'), 'sms', array(
            'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'comment' => 'sms quantity',
            'nullable'  => false,
            'default'   => 0,
        )
    );

$installer->getConnection()
    ->addColumn($installer->getTable('evites/evites'), 'sms', array(
            'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'comment' => 'sms quantity',
            'nullable'  => false,
            'default'   => 0,
        )
    );

$installer->endSetup();

