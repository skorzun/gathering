<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'remind_at', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
            'nullable'  => true,
            'comment'   => 'Remind date',
        )
    );

$installer->endSetup();
