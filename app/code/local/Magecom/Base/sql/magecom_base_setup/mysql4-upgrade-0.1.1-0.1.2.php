<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'is_reply', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable'  => false,
            'default'   => 0,
            'comment'   => 'Is replay eVite',
        )
    );

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'reply_at', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
            'nullable'  => true,
            'comment'   => 'Reply date',
        )
    );

$installer->endSetup();
