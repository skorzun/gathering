<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'is_open', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable'  => false,
            'default'   => 0,
            'comment'   => 'Opening card count',
        )
    );

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'first_open_at', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
            'nullable'  => true,
            'comment'   => 'The first opening cards',
        )
    );

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'is_download', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'nullable'  => false,
            'default'   => 0,
            'comment'   => 'Is download',
        )
    );

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'download_at', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
            'nullable'  => true,
            'comment'   => 'Date of download',
        )
    );

$installer->endSetup();