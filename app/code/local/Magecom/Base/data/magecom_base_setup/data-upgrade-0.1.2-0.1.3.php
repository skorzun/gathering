<?php

$installer = $this;
$connection = $installer->getConnection();
$installer->startSetup();

$data = array(
    array('eGift'),
    array('eCard'),
    array('eVite')
);

$connection = $installer->getConnection()->insertArray(
    $installer->getTable('base/type'),
    array('type_name'),
    $data
);

$installer->endSetup();
