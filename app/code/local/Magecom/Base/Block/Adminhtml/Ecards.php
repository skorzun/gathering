<?php

class Magecom_Base_Block_Adminhtml_Ecards extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'base';
        $this->_controller = 'adminhtml_ecards';
        $this->_headerText = Mage::helper('base')->__('Report - eCards');

        parent::__construct();
        $this->_removeButton('add');
    }
}
