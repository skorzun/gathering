<?php

class Magecom_Base_Block_Adminhtml_Ecards_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('entity_id');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('ecards');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('base/send')->getCollection()
            ->addFieldToFilter('is_active', 1);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header'    => Mage::helper('base')->__('Entity ID'),
                'align'     => 'right',
                'width'     => '50px',
                'type'      => 'number',
                'index'     => 'entity_id',
            ));

        $this->addColumn('to_name',
            array(
                'header'    => Mage::helper('base')->__('eCard to name'),
                'align'     => 'left',
                'index'     => 'to_name',
            ));

        $this->addColumn('to_email',
            array(
                'header'    => Mage::helper('base')->__('eCard To Email'),
                'align'     => 'left',
                'index'     => 'to_email',
            ));

        $this->addColumn ('send_at',
            array(
                'header'    => Mage::helper('base')->__('Date sent'),
                'align'     => 'left',
                'format'    => Mage::app()->getLocale()
                        ->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
                'type'      => 'date',
                'width'     => '200px',
                'index'     => 'send_at'
            ));

        $this->addColumn('is_open',
            array(
                'header'    => Mage::helper('base')->__('Opening count'),
                'align'     => 'left',
                'index'     => 'is_open',
                'width'     => '80px',
                'type'      => 'number',
            ));

        $this->addColumn ('first_open_at',
            array(
                'header'    => Mage::helper('base')->__('First opening'),
                'align'     => 'left',
                'format'    => Mage::app()->getLocale()
                        ->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
                'type'      => 'date',
                'width'     => '200px',
                'index'     => 'first_open_at'
            ));

        $this->addColumn('is_download',
            array(
                'header'=> Mage::helper('base')->__('Downloaded or no?'),
                'index' => 'is_download',
                'type'  => 'options',
                'options' => array(
                    0 => 'No',
                    1 => 'Yes',
                )
            ));

        $this->addColumn ('download_at',
            array(
                'header'    => Mage::helper('base')->__('Date of download'),
                'align'     => 'left',
                'format'    => Mage::app()->getLocale()
                        ->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
                'type'      => 'date',
                'width'     => '200px',
                'index'     => 'download_at'
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('base')->__('CSV'));
        //$this->addExportType('*/*/exportExcel', Mage::helper('ecards')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            $field = ( $column->getFilterIndex() ) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                    $this->getCollection()->addFieldToFilter($field , $cond);
                }
            }
        }
        return $this;
    }
}
