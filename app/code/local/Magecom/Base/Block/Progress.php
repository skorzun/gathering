<?php

class Magecom_Base_Block_Progress extends Mage_Core_Block_Template
{
    protected $_steps;

    protected $_fields = array(
        1   => 'first',
        2   => 'second',
        3   => 'third',
        4   => 'fourth',
        5   => 'fifth',
        6   => 'sixth'
    );

    protected $_currentStep;

    protected function _construct() {
        $this->_currentStep = ($this->getData('step')) ? $this->getData('step') : 2;
        $this->_setSteps();
    }

    protected function _setSteps() {

        $collection = new Varien_Data_Collection();
        foreach ($this->_fields as $key=>$value) {
            $step = new Varien_Object();
            $step->setData(array(
                'name'     => Mage::getStoreConfig('base/steps/' . $value),
                'status'    => ($key == $this->_currentStep) ? 1 : 0
            ));
            $collection->addItem($step);
        }
        $this->_steps = $collection;
    }

    public function getSteps() {
        return $this->_steps;
    }

    public function getFirstStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('base/steps/first')
        ));

        return $step->getName();
    }

    public function getSecondStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('base/steps/second')
        ));

        return $step->getName();
    }

    public function getThirdStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('base/steps/third')
        ));

        return $step->getName();
    }

    public function getFourthStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('base/steps/fourth')
        ));

        return $step->getName();
    }

    public function getFifthStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('base/steps/fifth')
        ));

        return $step->getName();
    }

    public function getSixthStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('base/steps/sixth')
        ));

        return $step->getName();
    }

    public function getCardType() {
        $eCard = Mage::registry('egift');
        return $eCard->getAttributeText('card_type');
    }
}
