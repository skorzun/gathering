<?php

class Magecom_Base_Model_Resource_Send extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct() {
        $this->_init('base/send', 'entity_id');
    }
}