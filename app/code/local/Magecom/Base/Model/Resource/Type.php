<?php

class Magecom_Base_Model_Resource_Type extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct() {
        $this->_init('base/type', 'entity_id');
    }
}