<?php

class Magecom_Base_Model_Observer
{
    protected $_ecardTypes = array('eCards', 'eGifts', 'eVites');

    public function sendEmail()
    {
        Mage::helper('base')->sendEmail();
    }

    public function sendReminder()
    {
        Mage::helper('base')->sendReminder();
    }

    public function updatePrice(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();

            if (!$quote->getItemsCount()) {
                return;
            }

            foreach ($quote->getAllItemsCount() as $_item) {
                $itemOptions = $_item->getOptionsByCode();
                    if (array_key_exists('additional_options', $itemOptions)) {
                        $options = $itemOptions['additional_options'];
                        $data = unserialize($options->getValue());
                        if (array_key_exists('value', $data['send_number']) && Mage::helper('base')->checkIsBuySingle()) {
                            $ecardSending = Mage::getModel('base/send')->load($data['send_number']['value']);
                            if ($ecardSending->getId() && $ecardSending->getStatus() == 2) {
                                $prices = Mage::helper('base')->getProductPrice($ecardSending->getEcardId());
                                $_item->setOriginalCustomPrice($prices['single']);
                                $_item->save();
                            }
                        }
                    }
            }

            Mage::getModel('core/session')->unsBuySingle();
        }

        $this->_updateSmsQty();
    }

    protected function _updateSmsQty() {
        $qty = 0;
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        foreach ($quote->getAllVisibleItems() as $_item) {
            if (Mage::helper('base')->checkIsSms($_item)) {
                $smsItem = $_item;
                continue;
            }
            if ($_item->getProductType() !== Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL) {
                continue;
            }

            $itemOptions = $_item->getOptionsByCode();
            if (array_key_exists('additional_options', $itemOptions)) {
                $options = $itemOptions['additional_options'];
                $data = unserialize($options->getValue());
                if (array_key_exists('phone_number', $data)) {
                    $qty += $_item->getQty();
                }
            } else {
                $cardType = Mage::helper('base')->getCardType($_item->getProductId());
                $qty += $_item->getQty() * Mage::helper($cardType)->getRecipientsQty();
            }
        }

        if ($smsItem instanceof Mage_Sales_Model_Quote_Item) {
            if ($qty !== 0) {
                $smsItem->setQty($qty);
            } else {
                $quote->removeItem($smsItem->getItemId());
            }
        }
    }
}