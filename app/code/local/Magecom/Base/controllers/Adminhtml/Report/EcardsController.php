<?php

class Magecom_Base_Adminhtml_Report_EcardsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('report/ecards');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('base/adminhtml_ecards'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('base/adminhtml_ecards_grid')->toHtml()
        );
    }

    public function exportCsvAction()
    {
        $fileName = 'ecards.csv';
        $grid = $this->getLayout()->createBlock('base/adminhtml_ecards_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelAction()
    {
        $fileName = 'ecards.xml';
        $grid = $this->getLayout()->createBlock('base/adminhtml_ecards_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}
