<?php
/**
 * Class Magecom_Base_IndexController
 */
class Magecom_Base_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * add image action
     */
    public function addimageAction()
    {
        $type = 'cardimage';
        if(isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
            try {
                $prevUploadFile = Mage::getSingleton('core/session')->getFileImagePath();
                if(!empty($prevUploadFile)){
                    $prevFilePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'upload' . $prevUploadFile;
                    unlink($prevFilePath);
                }
                $uploader = new Varien_File_Uploader($type);
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $path     = Mage::getBaseDir('media') . DS . 'upload' . DS;
                $uploader->save($path, $_FILES[$type]['name'] );
                $filename = $uploader->getUploadedFileName();
                $file     = Mage::getBaseUrl('media') . 'upload' . $filename;
                Mage::getSingleton('core/session')->setFileImagePath($filename);
                echo json_encode($file);
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        return;
    }

    /**
     * Get cart type action
     */
    public function getcardtypeAction()
    {
        $cardType = Mage::getSingleton('core/session')->getCardType();
        Mage::getSingleton('core/session')->setIsSingle(true);
        Mage::getSingleton('core/session')->setBuySingle(true);
        echo json_encode($cardType);
    }

}
