<?php

require_once(Mage::getBaseDir('lib') . '/PHPExcel/IOFactory.php');

class Magecom_Base_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CARD_TYPE_EGIFT = 1;
    const CARD_TYPE_ECARD = 2;
    const CARD_TYPE_EVITE = 3;

    protected $skuSms = 'magesms23239512311';

    public function getFileRecipients() {
        $uploads = new Zend_File_Transfer_Adapter_Http();
        $_file = $uploads->getFileInfo();

        if (array_key_exists('csv_file', $_file)) {
            $inputFileName = $_file['csv_file']['tmp_name'];
            $info = new SplFileInfo($_file['csv_file']['name']);
        }
        if (array_key_exists('file', $_file)) {
            $info = new SplFileInfo($_file['file']['name']);
            $inputFileName = $_file['file']['tmp_name'];
        }

        $recipients = array();

        if ($info->getExtension() == 'csv') {
            $csv = new Varien_File_Csv();
            $csvData = $csv->getData($inputFileName);
            $_headersFile = array_shift($csvData);
            $_headers = array();
            foreach ($_headersFile as $row) {
                $_headers[] = strtolower(str_replace(' ', '', $row));
            }
            foreach ($csvData as $_row) {
                $recipients[] = array_combine($_headers, $_row);
            }
        }

        if ($info->getExtension() == 'xls') {
            $inputFileType = 'Excel5';
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);
            $dataCount = count($sheetData);
            if ($dataCount > 2) {
                $_headersFile = $sheetData[0];
                $_headers = array();
                foreach ($_headersFile as $row) {
                    $_headers[] = strtolower(str_replace(' ', '', $row));
                }
                for ($i=1; $i < $dataCount; $i++) {
                    $recipients[] = array_combine($_headers, $sheetData[$i]);
                }
            }
        }

        if ($info->getExtension() == 'xlsx') {
            $inputFileType = 'Excel2007';
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);
            $dataCount = count($sheetData);
            if ($dataCount > 2) {
                $_headersFile = $sheetData[0];
                $_headers = array();
                foreach ($_headersFile as $row) {
                    $_headers[] = strtolower(str_replace(' ', '', $row));
                }
                for ($i=1; $i < $dataCount; $i++) {
                    $recipients[] = array_combine($_headers, $sheetData[$i]);
                }
            }
        }

        return $this->_validateCsv($recipients);
    }

    protected function _validateCsv($csvData = array()) {
        $errors = 0;
        date_default_timezone_set(Mage::getStoreConfig('general/locale/timezone'));
        $result = array();
        foreach ($csvData as $_row) {
            if (!Zend_Validate::is($_row['name'], 'NotEmpty') ||
                !Zend_Validate::is($_row['email'], 'EmailAddress') ||
                (strtotime($_row['date']) < strtotime(date('m/d/Y')))) {
                $errors++;
                if (!Zend_Validate::is($_row['name'], 'NotEmpty')) {
                    Mage::log('name is not valid', null, 'validateRecipientErrors.log');
                }
                if (!Zend_Validate::is($_row['email'], 'EmailAddress')) {
                    Mage::log('email is not valid', null, 'validateRecipientErrors.log');
                }
                Mage::log($_row, null, 'validateRecipientErrors.log');
                Mage::log('Date from file - ' . $_row['date'] . ' strtotime - ' . strtotime($_row['date']), null, 'validateRecipientErrors.log');
                Mage::log('Current date - ' . date('m/d/Y') . ' strtotime - ' . strtotime(date('m/d/Y')), null, 'validateRecipientErrors.log');
            } else {
                $result[] = $_row;
            }
        }

        if(!$errors){
            Mage::getSingleton('core/session')->setRecipient($result);
        } else {
            Mage::getSingleton('core/session')->unsRecipient();
        }

        return $response = array(
            'errors' => $errors,
            'valid'  => count($result),
            'result' => $result
        );
    }

    public function getToken() {
        $generator = Mage::getModel('salesrule/coupon_codegenerator');
        $generator->setLength(24);
        $code = $generator->generateCode();

        return $code;
    }

    public function sendEmail() {
        try {
            $sheduleList = Mage::getModel('base/send')->getCollection()
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('status', 0)
                ->addFieldToFilter('send_at', array('lteq' => date('Y-m-d')));

            foreach ($sheduleList as $_shedule) {
                $storeId = Mage::app()->getStore()->getId();
//                $product = Mage::getModel('catalog/product')->load($_shedule->getEcardId());
//                $cardType = strtolower($product->getAttributeText('card_type'));
                switch ($_shedule->getEcardTypeId()) {
                    case self::CARD_TYPE_ECARD :
                        $templateId = 31;
                        break;
                    case self::CARD_TYPE_EVITE :
                        $templateId = 32;
                        break;
                    default:
                        $templateId = 30; // eGift
                }

                $customer = Mage::getModel('customer/customer')->load($_shedule->getCustomerId());
                $sender = array(
                    'name'  => $customer->getName(),
                    'email' => Mage::getStoreConfig('trans_email/ident_general/email')
                );

                $recipientName  = $_shedule->getToName();
                $recipientEmail = $_shedule->getToEmail();

                $listId         = Mage::helper('monkey')->config('list_ecard');
                $email          = $recipientEmail;
                $mergeVars      = array('FNAME' => $recipientName);
                $isConfirmNeed  = FALSE;
                Mage::getSingleton('monkey/api')->listSubscribe($listId, $email, $mergeVars, 'html', $isConfirmNeed, TRUE);

                $vars = array(
                    'senderName'    => $customer->getName(),
                    'recipientName' => $recipientName,
                    'uniqueId'      => $_shedule->getToken()
                );

                $translate = Mage::getSingleton('core/translate');
                Mage::getModel('core/email_template')
                    ->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $vars, $storeId);
                $translate->setTranslateInline(true);

                $_shedule->setStatus(1)
                    ->save();
                //=================================

                $phoneNumber = $_shedule->getPhoneNumber();
                if($phoneNumber){
                    $aCardType = array(
                        self::CARD_TYPE_EVITE => 'evite',
                        self::CARD_TYPE_ECARD => 'ecard',
                        self::CARD_TYPE_EGIFT => 'egift',
                    );
                    $eventData = array(
                        'sender'   => $vars['senderName'],
                        'link'     => $vars['uniqueId'],
                        'cardType' => $aCardType[$_shedule->getEcardTypeId()],
                        'name'     => $recipientName,
                        'number'   => $phoneNumber,
                    );

                    Mage::dispatchEvent('email_send', $eventData);
                }

                //=================================
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'ecards_send.log', true);
        }
    }

    public function sendReminder()
    {
        try {
            $sheduleList = Mage::getModel('base/send')->getCollection()
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('remind_at', array('lteq' => date('Y-m-d')));

            foreach ($sheduleList as $_shedule) {
                $storeId = Mage::app()->getStore()->getId();
                $product = Mage::getModel('catalog/product')->load($_shedule->getEcardId());
                $cardType = strtolower($product->getAttributeText('card_type'));
                $templateId = null;
                $secondReminder = NULL;
                switch ($cardType) {
                    case 'egifts':
                        $templateId = 34;
                        $secondReminder = Mage::helper('egifts')->getSecondRemindDays();
                        break;
                    case 'evites':
                        $templateId = 35;
                        break;
                    case 'ecards':
                        $templateId = 36;
                        $secondReminder = Mage::helper('ecards')->getSecondRemindDays();
                        break;
                }

                if ($templateId) {
                    $customer = Mage::getModel('customer/customer')->load($_shedule->getCustomerId());
                    $sender = array(
                        'name'      => $customer->getName(),
                        'email'     => Mage::getStoreConfig('trans_email/ident_general/email')
                    );

                    $recipientName  = $_shedule->getToName();
                    $recipientEmail = $_shedule->getToEmail();

                    $vars = array(
                        'senderName'    => $customer->getName(),
                        'recipientName' => $recipientName,
                        'uniqueId'      => $_shedule->getToken()
                    );

                    $translate = Mage::getSingleton('core/translate');
                    Mage::getModel('core/email_template')
                        ->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $vars, $storeId);
                    $translate->setTranslateInline(true);

                    if($_shedule->getIsReminder() == 0) {
                        $secondReminder = NULL;
                    }

                    if(!$secondReminder) {
                        $_shedule->setRemindAt(NULL);
                        $_shedule->setIsReminder(0);
                    } else {
                        $_shedule->setRemindAt(strtotime($_shedule->getRemindAt() . '+ ' . $secondReminder . 'days'));
                        $_shedule->setIsReminder(0);
                    }
                    $_shedule->save();
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'ecards_send.log', true);
        }
    }

    public function getAllRecipients()
    {
        $resipCollection = Mage::getSingleton('base/send')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->setOrder('entity_id', 'desc');

        $resipCollection->getSelect()->group(array('to_name', 'to_email'));
        $resipients = array();

        foreach($resipCollection as $resipient) {
            $resipients[$resipient->getToName()] = $resipient->getToEmail();
        }

        return $resipients;
    }

    /**
     * Get block image
     *
     * @return mixed
     */
    public function getBlockImage() {
        $block = Mage::app()->getLayout()->createBlock('base/cardimage', 'cardimage')
            ->setTemplate('magecom/cardimage.phtml');
        return $block->toHtml();
    }

    public function checkIsCardDouble($_product)
    {
        $attributeSetName = Mage::getModel('eav/entity_attribute_set')->load($_product->getAttributeSetId())->getAttributeSetName();
        Mage::getSingleton('core/session')->setIsCardDouble($attributeSetName =='eCard Double' ? true : false);
        return;
    }

    public function getCardType($productId = null)
    {
        if(empty($productId)){
            $id = Mage::registry('current_product')->getId();
            $productId = (!empty($id)) ? $id : Mage::app()->getRequest()->getParam('id');
        }
        $_product = Mage::getSingleton('catalog/product')->load($productId);
        $type = strtolower($_product->getAttributeText('card_type'));
        Mage::getSingleton('core/session')->setCardType($type);
        return $type;
    }

    public function checkIsBuySingle()
    {
        $bIsBuySingle = Mage::getSingleton('core/session')->getBuySingle();
        return !empty($bIsBuySingle) ? true : false;
    }

    public function getSinglePrice($productId)
    {
        $cardType    = $this->getCardType($productId);
        $singlePrice = Mage::getStoreConfig($cardType . '/price/retail_price_single');
        $storeCode   = Mage::app()->getStore()->getCode();
        if ($storeCode == 'wholesale') {
            $singlePrice = Mage::getStoreConfig($cardType . '/price/wholesale_price_single');
        }
        return $singlePrice;
    }

    public function getStoreCode()
    {
        return Mage::app()->getStore()->getName();
    }

    public function getProductPrice($productId)
    {
        if(Mage::helper('ecards')->isEcard($productId) || Mage::helper('evites')->isEcard($productId)){
            $this->checkIsLogIn();
        }
        $oProduct      = Mage::getModel('catalog/product')->load($productId);
        $aProductPrice = array();
        $aProductPrice['bundle']           = number_format($oProduct->getPrice(), 2, '.', '');
        $singlePrice                       = $oProduct->getData('single_price');
        $aProductPrice['single']           = !empty($singlePrice) ? $singlePrice : $aProductPrice['bundle'];
        $wholesaleSinglePrice              = $oProduct->getData('wholesale_single_price');
        $wholesaleBundlePrice              = $oProduct->getData('wholesale_bundle_price');
        $aProductPrice['wholesale_single'] = !empty($wholesaleSinglePrice) ? $wholesaleSinglePrice : $aProductPrice['single'];
        $aProductPrice['wholesale_bundle'] = !empty($wholesaleBundlePrice) ? $wholesaleBundlePrice : $aProductPrice['bundle'];

        if (Mage::helper('base')->getStoreCode() == 'wholesale'){
            $aProductPrice['single'] = $aProductPrice['wholesale_single'];
            $aProductPrice['bundle'] = $aProductPrice['wholesale_bundle'];
        }

        return $aProductPrice;
    }


    public function isBuyAsSingle()
    {
        $productId = Mage::registry('current_product')->getId();
        return Mage::getModel('catalog/product')->load($productId)->getData('is_buy_single');
    }

    public function checkIsLogIn()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $session = Mage::getSingleton('customer/session');
            $session->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
            Mage::getModel('core/session')->addNotice($this->__('You should log in or register.'));
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'))->sendResponse();
        }
        return;
    }

    public function checkIsSms($oProduct)
    {
        return $oProduct->getSku() == $this->skuSms ? true : false;
    }

    public function checkIsSingle($oProduct)
    {
        $aCustomOptions = $oProduct->getCustomOptions();
        if(isset($aCustomOptions['additional_options'])){
            $oAdditionalOptions = $aCustomOptions['additional_options'];
            $aValue = unserialize($oAdditionalOptions->getValue());
            if(isset($aValue['send_number'])){
                return true;
            }
        }
        return false;
    }

}
