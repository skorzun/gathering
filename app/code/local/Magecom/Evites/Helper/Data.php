<?php

class Magecom_Evites_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_code = 'eVite';

    public function getRecipientsQty()
    {
        return Mage::getStoreConfig('evites/recipients/evites_recipients');
    }

    public function getIsSpecialPrice()
    {
        return false;
    }

    public function getSpecialEcardPrice()
    {
        $customEcardPrice = Mage::getStoreConfig('evites/price/retail_price_bundle');
        $storeCode = Mage::app()->getStore()->getCode();
        if ($storeCode == 'wholesale') {
            $customEcardPrice = Mage::getStoreConfig('evites/price/wholesale_price_bundle');
        }
        return $customEcardPrice;
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function getCodeId()
    {
        $ecardType = Mage::getSingleton('base/type')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('type_name', $this->getCode());
        return $ecardType->getFirstItem()->getEntityId();
    }

    public function isEcard($productId)
    {
        $_product = Mage::getModel('catalog/product')->load($productId);
        $type = strtolower($_product->getAttributeText('card_type'));
        if ($type == 'evites') {
            return true;
        } else {
            return false;
        }
    }

    public function getRemindDays()
    {
        return Mage::getStoreConfig('evites/general/remind_day');
    }
}
