<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('base/send'), 'event', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => false,
            'default'   => NULL,
            'comment'   => 'Event name',
        )
    );

$installer->endSetup();
