<?php

class Magecom_Evites_Model_Observer
{
    public function setNewEvites(Varien_Event_Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            if ($order->getState() != Mage_Sales_Model_Order::STATE_COMPLETE ||
                ($order->getId() == Mage::getSingleton('core/session')->getEvitesVarible()) ) {
                return;
            }
            $order_no = (string)$order->getRealOrderId();
            $order->loadByIncrementId($order_no);

            $orderItems = $order->getAllItems();
            $bSms = false;
            foreach($orderItems as $key => $_virtualItem){
                if($_virtualItem->getSku() == 'magesms23239512311'){
                    $bSms = true;
                    unset($orderItems[$key]);
                    break;
                }
            }

            foreach($orderItems as $_virtualItem) {
                if ($_virtualItem->getProductType() == 'virtual') {
                    $prod = Mage::getSingleton('catalog/product')->load($_virtualItem->getProduct()->getId());
                    $type = strtolower($prod->getAttributeText('card_type'));
                    if ($type != 'evites') {
                        continue;
                    }
                    $qty = $_virtualItem->getQtyOrdered() * Mage::helper('evites')->getRecipientsQty();

                    $evite = Mage::getSingleton('evites/evites')->getCollection()
                        ->addFieldToSelect('*')
                        ->addFieldToFilter('customer_id', $order->getCustomerId())
                        ->addFieldToFilter('ecard_id', $prod->getId())
                        ->addFieldToFilter('is_active', array('in' => 1));

                    $itemOptions = $_virtualItem->getProductOptions();
                    if(isset($itemOptions['additional_options']['send_number'])) {
                        $ecardSendId = $itemOptions['additional_options']['send_number']['value'];
                        if (!empty($ecardSendId)) {
                            $ecardSend = Mage::getSingleton('base/send')->load($ecardSendId);
                        } else {
                            continue;
                        }

                        if($ecardSend->getStatus() == '2') {
                            $orderItemIds = array();
                            foreach ($orderItems as $_downloadableItem) {
                                if ($_downloadableItem->getProductType() == 'downloadable') {
                                    $orderItemIds[] = $_downloadableItem->getItemId();
                                }
                            }

                            if ($ecardSend->getId()) {
                                if(!empty($orderItemIds)){
                                    $ecardSend->setOrderItemIds(json_encode($orderItemIds));
                                }
                                $ecardSend->setIsActive(1);
                                $ecardSend->setStatus(0);
                                $ecardSend->setIsReminder(1);
                                $ecardSend->save();
                                Mage::helper('base')->sendEmail();
                            }
                        }
                    } else {
                        if($evite->getSize() > 0) {
                            $evite->getFirstItem()->setCustomerId($order->getCustomerId())
                                ->setEcardId($prod->getId())
                                ->setCount($evite->getFirstItem()->getCount() + $qty)
                                ->setIsActive(1);
                            if($bSms){
                                $evite->setSms($evite->getFirstItem()->getSms() + $qty);
                            }
                        } else {
                            $evite = Mage::getModel('evites/evites');
                            $evite->setCustomerId($order->getCustomerId())
                                ->setEcardId($prod->getId())
                                ->setCount($qty)
                                ->setIsActive(1);
                            if($bSms){
                                $evite->setSms($qty);
                            }
                        }
                        $evite->save();
                    }

                    Mage::getSingleton('core/session')->setEvitesVarible($order->getId());
                }
            }
            Mage::log('end');
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'ecards_observer.log', true);
        }
    }

    public function applyEvitePrice($observer)
    {
        if (Mage::helper('evites')->getIsSpecialPrice() == 1) {
            $event = $observer->getEvent();
            $product = $event->getProduct();
            if ( ($product->getTypeId() == 'virtual')
                && (strtolower($product->getAttributeText('card_type')) == 'evites') ) {
                if ($product->getSuperProduct() && $product->getSuperProduct()->isConfigurable()) {
                } else {
                    $product->setFinalPrice(Mage::helper('evites')->getSpecialEcardPrice());
                }
            }
        }
        return $this;
    }

    public function applyEviteCollectionPrice($observer)
    {
        if (Mage::helper('evites')->getIsSpecialPrice() == 1) {
            $myCustomPrice = Mage::helper('evites')->getSpecialEcardPrice();
            $products = $observer->getCollection();
            foreach( $products as $product ) {
                $prod = Mage::getSingleton('catalog/product')->load($product->getId());
                if ( ($product->getTypeId() == 'virtual')
                    && (strtolower($prod->getAttributeText('card_type')) == 'evites') ) {
                    $product->setPrice( $myCustomPrice );
                    $product->setFinalPrice( $myCustomPrice );
                }
            }
        }
        return $this;
    }
}
