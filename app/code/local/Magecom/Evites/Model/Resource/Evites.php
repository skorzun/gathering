<?php

class Magecom_Evites_Model_Resource_Evites extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct() {
        $this->_init('evites/evites', 'entity_id');
    }
}