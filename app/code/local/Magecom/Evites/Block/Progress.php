<?php

class Magecom_Evites_Block_Progress extends Magecom_Base_Block_Progress
{
    public function isBack() {
        return ($this->_currentStep == 1) ? false : true;
    }

    public function isNext() {
        return ($this->_currentStep == 6) ? false : true;
    }

    public function getReturnUrl() {
        return Mage::getUrl('evites/index/back');
    }

    public function getBackUrl() {
        return Mage::getUrl('evites/index/' . $this->_fields[$this->_currentStep - 1]);
    }

    public function getNextUrl() {
        return Mage::getUrl('evites/index/' . $this->_fields[$this->_currentStep + 1]);
    }

    public function getSecondStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('evites/steps/second')
        ));

        return $step->getName();
    }

    public function getThirdStep() {
        $step = new Varien_Object();
        $step->setData(array(
            'name'     => Mage::getStoreConfig('evites/steps/third')
        ));

        return $step->getName();
    }
}
