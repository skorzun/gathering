<?php
class Magecom_Evites_Block_Customer_Eventslist extends Mage_Core_Block_Template
{
    public function __construct()
    {
        $this->setTemplate('magecom/evites/customer/eventlist.phtml');
        $orders = Mage::getSingleton('base/send')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('ecard_type_id', array('in' => Mage::helper('evites')->getCodeId()))
            ->addFieldToFilter('is_active', array('in' => 1))
            ->addFieldToFilter('event', array('in' => $this->getRequest()->getPost('event')))
            ->setOrder('entity_id', 'desc');
        Mage::log($orders->getSelect()->__toString());

        $this->setOrders($orders);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'events.evites.pager')
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

    public function getOrderIncrementId($orderId)
    {
        $order = Mage::getSingleton('sales/order')->load($orderId);
        return $order->getIncrementId();
    }

    public function getProduct($productId)
    {
        return Mage::getSingleton('catalog/product')->load($productId);
    }
}
