<?php

class Magecom_Evites_Block_Step_Textback extends Mage_Core_Block_Template
{
    protected $_ecard;

    protected function _construct() {
        if (!$this->_ecard = Mage::registry('evite')) {
            $this->_ecard = Mage::getModel('catalog/product')->load($this->getData('evite_id'));
        }
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getCardType() {
        return $this->getEcard()->getAttributeText('card_type');
    }
}
