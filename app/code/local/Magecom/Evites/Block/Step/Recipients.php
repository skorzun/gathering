<?php

class Magecom_Evites_Block_Step_Recipients extends Mage_Core_Block_Template
{
    protected $_ecard;

    protected function _construct() {
        $this->_ecard = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getEvitesCount()
    {
        $ecard = Mage::getSingleton('evites/evites')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('ecard_id', $this->_ecard->getId())
            ->addFieldToFilter('is_active', array('in' => 1));
        return $ecard->getFirstItem()->getCount();
    }
}
