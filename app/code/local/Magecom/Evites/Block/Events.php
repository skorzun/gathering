<?php

class Magecom_Evites_Block_Events extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

    public function getEvents()
    {
        $events = Mage::getSingleton('base/send')->getCollection()
            ->addFieldToSelect('event')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('ecard_type_id', array('in' => Mage::helper('evites')->getCodeId()))
            ->addFieldToFilter('is_active', array('in' => 1))
            ->addFieldToFilter('event', array('neq' => ''))
            ->distinct(true)
            ->setOrder('entity_id', 'desc');

        return $events;
    }
}
