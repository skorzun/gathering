<?php

class Magecom_Evites_ViewController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function replyAction() {
        try {
            $storeId = Mage::app()->getStore()->getId();
            $id = $this->getRequest()->getPost('ecard_id');
            $_ecard = Mage::getModel('base/send')->load($id);

            $customer = Mage::getModel('customer/customer')->load($_ecard->getCustomerId());
            $sender = array(
                'name' => $_ecard->getToName(),
                'email' => Mage::getStoreConfig('trans_email/ident_general/email')
            );

            $templateId = 33;

            $recipientName = $customer->getName();
            $recipientEmail = $customer->getEmail();

            $vars = array(
                'recipientName' => $_ecard->getToName(),
//                'customer'      => $customer->getName(),
//                'id-invite'     => $_ecard->getEntityId(),
                'token'         => $_ecard->getToken(),
                'store'         => Mage::app()->getStore()->getCode(),
//                'attending'     => $this->getRequest()->getPost('attend'),
//                'evite-people'  => $this->getRequest()->getPost('how'),
            );

            $translate = Mage::getSingleton('core/translate');
            Mage::getModel('core/email_template')
                ->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $vars, $storeId);
            $translate->setTranslateInline(true);

            $_ecard->setIsReply(1)
                ->setReplyAt(date('Y-m-d H:i:s'))
                ->setReplyText(json_encode(
                    array(
                    'attend'    => $this->getRequest()->getPost('attend'),
                    'how'       => $this->getRequest()->getPost('how')
                    )
                ))
                ->setRemindAt(NULL)
                ->save();

        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'ecards_send.log', true);
            return false;
        }
        $this->_redirect('');
    }
}
