<?php
class Magecom_Evites_IndexController extends Mage_Core_Controller_Front_Action
{
    protected function _init() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $session = Mage::getSingleton('customer/session');
            $session->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
            Mage::getModel('core/session')->addNotice(
                $this->__('You should log in or register before creating an eCard.')
            );
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('customer/account/login'))->sendResponse();
        }
    }

    public function indexAction() {
        $this->_init();
        if(!Mage::getSingleton('core/session')->getBuySingle()){
            Mage::getModel('core/session')->unsBuySingle();
        }
        $id = (int) $this->getRequest()->getParam('id');
        if (!$id) {
            $this->_back();
            return;
        }
        $_product = Mage::getSingleton('catalog/product')->load($id);
        Mage::register('evite', $_product);
        Mage::register('evite_id', $id);
        Mage::helper('base')->checkIsCardDouble($_product);

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _back() {
        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('ecard-5.html'))->sendResponse();
    }

    public function previewAction()
    {
        $this->loadLayout();
        $postData = $this->getRequest()->getPost();
        try {
            if (!isset($postData['message'])) {
                $response['status'] = 'SUCCESS';
                $aData = array(
                    'message'   => json_encode($postData['ecard_text']),
                    'ecard_id'  => $postData['ecard_id'],
                    'style'     => $postData['ecard_style'],
                    'recipient' => $postData['ecard_to'][0]['name']
                );
                if(isset($postData['ecard_to'][0]['name'])){
                    $aData['recipient'] = $postData['ecard_to'][0]['name'];
                }
                if(isset($postData['card_image'])){
                    $aData['card_image'] = $postData['card_image'];
                }
                $block = $this->getLayout()->createBlock(
                    'evites/step_preview',
                    'preview',
                    $aData
                );
                if ($block) {
                    $response['status'] = 'SUCCESS';
                    $response['preview'] = $block->toHtml();
                }
            }
        } catch (Exception $e) {
            $response['status'] = 'ERROR';
            $response['message'] = $e->getMessage();
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

    public function summaryAction() {
        $postData = $this->getRequest()->getPost();
        $block = $this->getLayout()->createBlock(
            'evites/step_summary',
            'ecard_summary',
            array('summary' => $postData)
        );
        $response['status'] = 'SUCCESS';
        $response['summary'] = $block->toHtml();
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function uploadAction() {
        $data = Mage::helper('base')->getFileRecipients();
        $response['status'] = 'SUCCESS';
        $response['errors'] = $data['errors'];
        $response['qty']    = $data['valid'];
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function sendeviteAction()
    {
        try {
            $postData = $this->getRequest()->getPost();
            if ($postData['variant'] == 2) {
                $postData['ecard_text']['title'] = $postData['ecard_text']['title_own'];
            }
            if ($postData['recipients_method']) {
                $fileData = Mage::helper('base')->getFileRecipients();
                $data = array_filter($fileData['result']);
                if (empty($data)) {
                    Mage::getModel('core/session')->addError($this->__('No valid recipient data in uploaded file'));
                    $this->_redirect('checkout/cart/index');
                    return;
                }
                $postData['ecard_to'] = $fileData['result'];
            }

            $ecard = Mage::getSingleton('evites/evites')->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('customer_id', Mage::getModel('customer/session')->getCustomer()->getId())
                ->addFieldToFilter('ecard_id', $postData['ecard_id'])
                ->addFieldToFilter('is_active', array('in' => 1));
            $ecard->getFirstItem()->setCount($ecard->getFirstItem()->getCount() - count($postData['ecard_to']));
            $allowSms = $ecard->getFirstItem()->getSms();

            foreach ($postData['ecard_to'] as $_recipient) {
                $ecardSending = Mage::getModel('base/send');
                $reminder = null;
                if ($postData['ecard_text']['remider'] != '') {
                    $reminder = strtotime($_recipient['date'] . ' + ' . $postData['ecard_text']['remider'] . ' days');
                }
                $ecardSending->setCustomerId(Mage::getModel('customer/session')->getCustomer()->getId())
                    ->setEcardId($postData['ecard_id'])
                    ->setToName($_recipient['name'])
                    ->setToEmail($_recipient['email'])
                    ->setMessage(json_encode($postData['ecard_text']))
                    ->setMessageStyle(json_encode($postData['ecard_style']))
                    ->setEvent($postData['ecard_text']['title'])
                    ->setToken(
                        Mage::getUrl('evites/view/index',
                            array(
                                '_store'    => Mage::app()->getStore()->getCode(),
                                'key'       => Mage::helper('base')->getToken())
                        )
                    )
                    ->setCreatedAt(date('Y-m-d'))
                    ->setSendAt(strtotime($_recipient['date']))
                    ->setRemindAt($reminder)
                    ->setEcardTypeId(Mage::helper('evites')->getCodeId());
                $ecardSending->setIsActive(1);
                $ecardSending->setStatus(0);
                if(isset($_recipient['phone'])) {
                    $allowSms = $allowSms - 1;
                    $ecardSending->setPhoneNumber($_recipient['phone']);
                }
                if(isset($postData['card_image'])){
                    $ecardSending->setImage(substr_replace($postData['card_image'], '', 0, strlen(Mage::getBaseUrl('media'))));
                }
                $ecardSending->save();
                Mage::getModel('core/session')->unsFileImagePath();
            }

            $ecard->getFirstItem()->setSms($allowSms);
            $ecard->save();
            Mage::helper('base')->sendEmail();

        } catch (Exception $e) {
            Mage::getModel('core/session')->addError($e->getMessage());
        }
        $this->_redirect('evites/customer/sent/');
    }

    public function alleventsAction()
    {
        $block = $this->getLayout()->createBlock(
            'evites/customer_eventslist',
            'events.evite'
        );
        $response['status'] = 'SUCCESS';
        $response['event'] = $block->toHtml();
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function addtocartAction() {
        if(!Mage::helper('base')->checkIsBuySingle()){
            Mage::getSingleton('core/session')->setBuySingle(true);
        }
        try {
            $postData = $this->getRequest()->getPost();
            if ($postData['variant'] == 2) {
                $postData['ecard_text']['title'] = $postData['ecard_text']['title_own'];
            }
            $postData['ecard_qty'] = 1;
            if ($postData['recipients_method']) {
                $data = Mage::getSingleton('core/session')->getRecipient();
                if (empty($data)) {
                    Mage::getModel('core/session')->addError($this->__('No valid recipient data in uploaded file'));
                    $this->_redirect('checkout/cart/index');
                    return;
                }
                $postData['ecard_to'] = $data;
                Mage::getSingleton('core/session')->unsRecipient();
            }

            foreach ($postData['ecard_to'] as $_recipient) {
                $ecardSending = Mage::getModel('base/send');
                $reminder = null;
                if ($postData['ecard_text']['remider'] != '') {
                    $reminder = strtotime($_recipient['date'] . ' + ' . $postData['ecard_text']['remider'] . ' days');
                }
                $ecardSending->setCustomerId(Mage::getModel('customer/session')->getCustomer()->getId())
                    ->setEcardId($postData['ecard_id'])
                    ->setToName($_recipient['name'])
                    ->setToEmail($_recipient['email'])
                    ->setMessage(json_encode($postData['ecard_text']))
                    ->setMessageStyle(json_encode($postData['ecard_style']))
                    ->setEvent($postData['ecard_text']['title'])
                    ->setToken(
                        Mage::getUrl('evites/view/index',
                            array(
                                '_store' => Mage::app()->getStore()->getCode(),
                                'key'    => Mage::helper('base')->getToken())
                        )
                    )
                    ->setCreatedAt(date('Y-m-d'))
                    ->setSendAt(strtotime($_recipient['date']))
                    ->setRemindAt($reminder)
                    ->setEcardTypeId(Mage::helper('evites')->getCodeId());

                $ecardSending->setIsActive(0);
                $ecardSending->setStatus(2);
                if(isset($_recipient['phone'])) {
                    $ecardSending->setPhoneNumber($_recipient['phone']);
                }

                if(isset($postData['card_image'])){
                    $ecardSending->setImage(substr_replace($postData['card_image'], '', 0, strlen(Mage::getBaseUrl('media'))));
                }
                $ecardSending->save();
                Mage::getModel('core/session')->unsFileImagePath();

                $_ecard = Mage::getModel('catalog/product')->load($postData['ecard_id']);
                $quote = Mage::getSingleton('checkout/cart')->getQuote();
//                $_item = $quote->addProduct($_ecard, 1);
                $_item = Mage::getModel('sales/quote_item')->setProduct($_ecard);
                $_item->setQuote($quote);
                $_item->setQty(1);

                $additionalOptions = array();
                if ($additionalOption = $_ecard->getCustomOption('additional_options')) {
                    $additionalOptions = (array)unserialize($additionalOption->getValue());
                }

                $additionalOptions['recipient'] = array(
                    'label' => 'eVite Recipient',
                    'value' => $_recipient['name'] . ' \'' . $_recipient['email'] . '\'',
                );
                $additionalOptions['send_number'] = array(
                    'label' => 'eVite Send Number',
                    'value' => $ecardSending->getId(),
                );

                $phoneNumber = $ecardSending->getPhoneNumber();
                if(!empty($phoneNumber)){
                    $additionalOptions['phone_number'] = array(
                        'label' => 'eCard Phone Number',
                        'value' => $phoneNumber,
                    );
                }

                $_item->addOption(new Varien_Object(
                    array(
                        'product' => $_item->getProduct(),
                        'code'    => 'additional_options',
                        'value'   => serialize($additionalOptions)
                    )
                ));
                $quote->addItem($_item);
            }

            $quote      = Mage::getSingleton('checkout/cart')->getQuote();
            $customerId = Mage::getSingleton('customer/session')->getId();
            $customer   = Mage::getModel('customer/customer')->load($customerId);
            $quote->assignCustomer($customer);

            $quote->collectTotals()->save();
            if(!empty($phoneNumber)) {
                Mage::dispatchEvent('checkout_cart_add_singlecard');
            }
            Mage::getSingleton('core/session')->unsIsSingle();
        } catch (Exception $e) {
            Mage::getModel('core/session')->addError($e->getMessage());
        }
    }

}

