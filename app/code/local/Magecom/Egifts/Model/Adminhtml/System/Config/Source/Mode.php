<?php

class Magecom_Egifts_Model_Adminhtml_System_Config_Source_Mode
{
    public function toOptionArray() {
        return array(
            array('value'=>1, 'label'=>Mage::helper('egifts')->__('List')),
            array('value'=>0, 'label'=>Mage::helper('egifts')->__('Grid')),
        );
    }
}