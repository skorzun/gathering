<?php

class Magecom_Egifts_Model_Observer
{
    public function setActiveSending(Varien_Event_Observer $observer) {
        try {
            $order = $observer->getEvent()->getOrder();
            if ($order->getState() != Mage_Sales_Model_Order::STATE_COMPLETE ||
                ($order->getId() == Mage::getSingleton('core/session')->getEgiftsVarible()) ) {
                return;
            }

            $order_no = (string)$order->getRealOrderId();
            $order->loadByIncrementId($order_no);

            $orderItems = $order->getAllItems();
            foreach($orderItems as $_virtualItem) {
                if ($_virtualItem->getProductType() == 'virtual') {
                    $prod = Mage::getSingleton('catalog/product')->load($_virtualItem->getProduct()->getId());
                    $type = strtolower($prod->getAttributeText('card_type'));
                    if ($type != 'egifts') {
                        continue;
                    }
                    $itemOptions = $_virtualItem->getProductOptions();
                    $ecardSendId = $itemOptions['additional_options']['send_number']['value'];
                    if (isset($ecardSendId) && !empty($ecardSendId)) {
                        $giftIds = explode(',', $itemOptions['additional_options']['gift']['value']);
                        $orderItemIds = array();
                        foreach ($orderItems as $_downloadableItem) {
                            if ($_downloadableItem->getProductType() == 'downloadable' && in_array($_downloadableItem->getProductId(), $giftIds)) {
                                $orderItemIds[] = $_downloadableItem->getItemId();
                            }
                        }

                        $ecard = Mage::getSingleton('base/send')->load($ecardSendId);
                        if ($ecard->getId()) {
                            $ecard->setOrderItemIds(json_encode($orderItemIds));
                            $ecard->setIsActive(1);
                            $ecard->setIsReminder(1);
                            $ecard->save();

                            Mage::helper('base')->sendEmail();
                        }
                    }
                }
            }
            Mage::getSingleton('core/session')->setEgiftsVarible($order->getId());
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'ecards_observer.log', true);
        }
    }

    public function clearCart(Varien_Event_Observer $observer) {
        foreach( Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item ) {
            Mage::getSingleton('checkout/cart')->removeItem( $item->getId() )->save();
        }
    }

    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer) {
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $orderItem = $observer->getOrderItem();
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
    }
}
