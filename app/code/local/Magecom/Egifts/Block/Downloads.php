<?php

class Magecom_Egifts_Block_Downloads extends Mage_Core_Block_Template
{
    protected $_ecardId;

    protected function _construct() {
        $this->_ecardId = $this->getRequest()->getParam('id');
    }

    public function getId() {
        return $this->_ecardId;
    }

    public function getGiftIds() {
        $_ecard = Mage::getModel('base/send')->load($this->getId());
        return json_decode($_ecard->getGiftIds());
    }

    public function getGiftCollection() {
        return Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => $this->getGiftIds()));
    }
}
