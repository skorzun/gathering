<?php

class Magecom_Egifts_Block_Progress extends Magecom_Base_Block_Progress
{
    public function isBack() {
        return ($this->_currentStep == 1) ? false : true;
    }

    public function isNext() {
        return ($this->_currentStep == 6) ? false : true;
    }

    public function getReturnUrl() {
        return Mage::getUrl('egifts/index/back');
    }

    public function getBackUrl() {
        return Mage::getUrl('egifts/index/' . $this->_fields[$this->_currentStep - 1]);
    }

    public function getNextUrl() {
        return Mage::getUrl('egifts/index/' . $this->_fields[$this->_currentStep + 1]);
    }
}
