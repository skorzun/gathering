<?php

class Magecom_Egifts_Block_View extends Mage_Core_Block_Template
{
    protected $_ecard;
    protected $_ecardData;
    protected $_gifts;

    /**
     * @var string
     */
    protected $_leftSideCardImage;

    protected function _construct() {
        $token = $this->getRequest()->getParam('key');

        $this->_ecardData = Mage::getModel('base/send')->getCollection()
            ->addFieldToFilter('token', array('like' => '%' . $token . '%'))
            ->getFirstItem();

        $this->_leftSideCardImage = $this->_ecardData->getImage();

        $openingCount = $this->_ecardData->getIsOpen() + 1;
        $firstOpenAt = $this->_ecardData->getFirstOpenAt();
        $this->_ecardData->setIsOpen($openingCount);
        if (!$firstOpenAt) {
            $this->_ecardData->setFirstOpenAt(date('Y-m-d H:i:s'));
        }

        $this->_ecardData->save();


        $this->_ecard = Mage::getModel('catalog/product')->load($this->_ecardData->getEcardId());

        $this->_gifts = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => json_decode($this->_ecardData->getGiftIds())));
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getEcardData() {
        return $this->_ecardData;
    }

    public function getGifts() {
        return $this->_gifts;
    }

    /**
     * Get left side card image
     *
     * @return string
     */
    public function getLeftSideCardImage() {

        $file = Mage::getBaseUrl('media') . $this->_leftSideCardImage;
        return $file;
    }
}
