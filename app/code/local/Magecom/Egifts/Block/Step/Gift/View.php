<?php

class Magecom_Egifts_Block_Step_Gift_View extends Mage_Core_Block_Template
{
    protected $_ecard;
    protected $_giftIds;
    protected $_gifts;

    protected function _construct() {
        $this->setTemplate('magecom/egifts/steps/gift/view.phtml');
        if (!$this->_ecard = Mage::registry('egift')) {
            $this->_ecard = Mage::getModel('catalog/product')->load($this->getData('ecard_id'));
        }
        $this->_giftIds = $this->getData('ids');

    }

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getEcard() {
        return $this->_ecard;
    }

    public function getGifts() {
        if (is_array($this->_giftIds)) {
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', array('in' => $this->_giftIds));
            return $collection;
        }
    }
}
