<?php

class Magecom_Egifts_Block_Step_Summary extends Mage_Core_Block_Template
{
    protected $_data;

    protected function _construct() {
        $this->setTemplate('magecom/egifts/steps/summary.phtml');
        $this->_data = $this->getData('summary');
    }

    public function getEcard() {
        return Mage::getModel('catalog/product')->load($this->_data['ecard_id']);
    }

    public function getItems() {
        $giftIds = array();
        if (array_key_exists('gifts_ids', $this->_data)) {
            foreach ($this->_data['gifts_ids'] as $key=>$value) {
                $giftIds[] = $key;
            }
        }
        return Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => $giftIds));
    }

    public function getItemQty() {
        return ($this->_data['recipients_method']) ? $this->_data['ecard_qty'] : count($this->_data['ecard_to']);
    }

    public function getTotal($type = false) {
        $subTotal = 0;
        foreach ($this->getItems() as $_item) {
            $subTotal += $this->getPrice($_item) * $this->getItemQty();
        }
        if ($type) {
            $subTotal += $this->getEcard()->getPrice() * $this->getItemQty();
        }
        return Mage::helper('core')->currency($subTotal, true, false);
    }

    public function getRecipients() {
        $collection = new Varien_Data_Collection();

        foreach ($this->_data['ecard_to'] as $data) {
            $_recipient = new Varien_Object();
            $_recipient->setData($data);
            $collection->addItem($_recipient);
        }

        return $collection;
    }

    public function getMethod() {
        return $this->_data['recipients_method'];
    }

    public function getCardType() {
        return $this->getEcard()->getAttributeText('card_type');
    }

    public function getPrice($item) {
        $useDefaultPrice = Mage::getStoreConfig('egifts/ebooks/use_default_price');
        if ($useDefaultPrice == 1) {
            $customEbookPrice = Mage::getStoreConfig('egifts/ebooks/retail_price');
            $storeCode = Mage::app()->getStore()->getCode();
            if ($storeCode == 'wholesale') {
                $customEbookPrice = Mage::getStoreConfig('egifts/ebooks/wholesale_price');
            }
            return $customEbookPrice;
        }

        return $item->getPrice();
    }
}
