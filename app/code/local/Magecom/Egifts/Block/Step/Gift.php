<?php

class Magecom_Egifts_Block_Step_Gift extends Mage_Core_Block_Template
{
    protected $_gifts;

    public function __construct() {
        $_ecard = Mage::registry('egift');
        $this->_gifts = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => $_ecard->getRelatedProductIds()));
    }

    public function getGifts() {
        return $this->_gifts;
    }

    public function getMode() {
        return (!Mage::getStoreConfig('egifts/gifts/mode')) ? 'grid' : 'list';
    }

    public function getColumnSize() {
        return Mage::getStoreConfig('egifts/gifts/count');
    }
}
