<?php

class Magecom_Egifts_Helper_Downloads extends Mage_Core_Helper_Abstract
{
    public function getDownloadableUrl($ecardId, $giftId) {
        $_ecard = Mage::getModel('base/send')->load($ecardId);

        if ($_ecard->getId()) {
            return $urlCollection = Mage::getModel('downloadable/link_purchased_item')->getCollection()
                ->addFieldToFilter('product_id', $giftId)
                ->addFieldToFilter('order_item_id', array('in' => json_decode($_ecard->getOrderItemIds())))
                ->addFieldToFilter('status', array('eq' => 'available'));
        }
    }

    public function isDownloadable($orderItemId, $orderId) {
        $order = Mage::getModel('sales/order')->load($orderId);
        $orderItems = $order->getAllItems();
        foreach ($orderItems as $_item) {
            if ($_item->getItemId() == $orderItemId) {
                $itemOptions = $_item->getProductOptions();
                if (isset($itemOptions['additional_options'])) {
                    return false;
                }
            }
        }

        return true;
    }
}