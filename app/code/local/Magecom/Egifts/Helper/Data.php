<?php

require_once(Mage::getBaseDir('lib') . '/PHPExcel/IOFactory.php');

class Magecom_Egifts_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getFileRecipients() {
        $uploads = new Zend_File_Transfer_Adapter_Http();
        $_file = $uploads->getFileInfo();

        if (array_key_exists('csv_file', $_file)) {
            $inputFileName = $_file['csv_file']['tmp_name'];
            $info = new SplFileInfo($_file['csv_file']['name']);
        }
        if (array_key_exists('file', $_file)) {
            $info = new SplFileInfo($_file['file']['name']);
            $inputFileName = $_file['file']['tmp_name'];
        }

        $recipients = array();

        if ($info->getExtension() == 'csv') {
            $csv = new Varien_File_Csv();
            $csvData = $csv->getData($inputFileName);
            $_headersFile = array_shift($csvData);
            $_headers = array();
            foreach ($_headersFile as $row) {
                $_headers[] = strtolower(str_replace(' ', '', $row));
            }
            foreach ($csvData as $_row) {
                $recipients[] = array_combine($_headers, $_row);
            }
        }

        if ($info->getExtension() == 'xls') {
            $inputFileType = 'Excel5';
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);
            $dataCount = count($sheetData);
            if ($dataCount > 2) {
                $_headersFile = $sheetData[0];
                $_headers = array();
                foreach ($_headersFile as $row) {
                    $_headers[] = strtolower(str_replace(' ', '', $row));
                }
                for ($i=1; $i < $dataCount; $i++) {
                    $recipients[] = array_combine($_headers, $sheetData[$i]);
                }
            }
        }

        if ($info->getExtension() == 'xlsx') {
            $inputFileType = 'Excel2007';
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);
            $dataCount = count($sheetData);
            if ($dataCount > 2) {
                $_headersFile = $sheetData[0];
                $_headers = array();
                foreach ($_headersFile as $row) {
                    $_headers[] = strtolower(str_replace(' ', '', $row));
                }
                for ($i=1; $i < $dataCount; $i++) {
                    $recipients[] = array_combine($_headers, $sheetData[$i]);
                }
            }
        }

        return $this->_validateCsv($recipients);
    }

    protected function _validateCsv($csvData = array()) {
        $errors = 0;
        date_default_timezone_set(Mage::getStoreConfig('general/locale/timezone'));
        $result = array();
        foreach ($csvData as $_row) {
            if (!Zend_Validate::is($_row['name'], 'NotEmpty') ||
                !Zend_Validate::is($_row['email'], 'EmailAddress') ||
                (strtotime($_row['date']) < strtotime(date('m/d/Y')))) {
                $errors++;
                Mage::log($_row, null, 'validateRecipientErrors.log');
            } else {
                $result[] = $_row;
            }
        }

        return $response = array(
            'errors'    => $errors,
            'valid'     => count($result),
            'result'    => $result
        );
    }

    public function isModuleEnable()
    {
        if(Mage::getStoreConfig('egifts/general/enabled') == '1') {
            return true;
        } else {
            return false;
        }
    }

    public function getRemindDays()
    {
        return Mage::getStoreConfig('egifts/general/remind_day');
    }

    public function getSecondRemindDays()
    {
        return Mage::getStoreConfig('egifts/general/second_remind_day');
    }
}