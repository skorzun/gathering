<?php

class Magecom_Egifts_ViewController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function downloadsAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function reportAction() {
        $id = $this->getRequest()->getParam('id');
        $_ecard = Mage::getModel('base/send')->load($id);
        $_ecard->setIsDownload(1)
            ->setDownloadAt(date('Y-m-d H:i:s'))
            ->setRemindAt(NULL)
            ->setIsReminder(0)
            ->save();
    }
}
