<?php

class Magecom_Egifts_IndexController extends Mage_Core_Controller_Front_Action
{
    protected function _init() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $session = Mage::getSingleton('customer/session');
            $session->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
            Mage::getModel('core/session')->addNotice($this->__('You should log in or register before creating an eCard.'));
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'))->sendResponse();
        }
    }
    public function indexAction() {
        $this->_init();

        $id = (int) $this->getRequest()->getParam('id');

        if (!$id) {
            $this->_back();
            return;
        }

        $_product = Mage::getSingleton('catalog/product')->load($id);
        Mage::register('egift', $_product);
        Mage::register('egift_id', $id);

        Mage::helper('base')->checkIsCardDouble($_product);

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _back() {
        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('ecard-5.html'))->sendResponse();
    }

    public function firstAction() {
        $this->_redirect('egift/*/*', array('id' => Mage::registry('egift_id')));
    }

    public function previewAction() {
        $this->loadLayout();
        $postData = $this->getRequest()->getPost();
        try {
            if (!isset($postData['message'])) {
                $giftIds = array_keys($postData['gifts_ids']);
                $response['status'] = 'SUCCESS';
                $aData = array(
                    'ids'        => $giftIds,
                    'message'    => json_encode($postData['ecard_text']),
                    'style'      => $postData['ecard_style'],
                    'ecard_id'   => $postData['ecard_id'],
                    'recipient'  => $postData['ecard_to'][0]['name'],
                );

                if(isset($postData['ecard_to'][0]['name'])){
                    $aData['recipient'] = $postData['ecard_to'][0]['name'];
                }

                if(isset($postData['card_image'])){
                    $aData['card_image'] = $postData['card_image'];
                }

                $block = $this->getLayout()->createBlock(
                    'egifts/step_preview',
                    'preview',
                    $aData
                );

                if ($block) {
                    $response['status'] = 'SUCCESS';
                    $response['preview'] = $block->toHtml();
                }
            }
        } catch (Exception $e) {
                $response['status'] = 'ERROR';
                $response['message'] = $e->getMessage();
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
            return;

    }

    public function addgiftAction()
    {
        $postData = $this->getRequest()->getParams();
        try {
            $ecard = Mage::getModel('catalog/product')->load($postData['ecard_id']);
            $ecardId = $ecard->getId();

            $response = array();
            try {
                $giftIds = array_keys($postData['gifts_ids']);

                if (empty($giftIds)) {
                    $giftIds = null;
                }

                $this->loadLayout();
                $block = $this->getLayout()->createBlock(
                    'egifts/step_gift_view',
                    'gift_view',
                    array('ids' => $giftIds, 'ecard_id' => $ecardId)
                );

                if ($block) {
                    $response['status'] = 'SUCCESS';
                    $response['gift_view'] = $block->toHtml();
                }
            } catch (Exception $e) {
                $response['status'] = 'ERROR';
                $response['message'] = $e->getMessage();
            }
        } catch (Exception $e) {
            $response['status'] = 'ERROR';
            $response['message'] = $this->__('eGifts isn\'t defined');
        }
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function summaryAction() {
        $postData = $this->getRequest()->getPost();
        $block = $this->getLayout()->createBlock(
            'egifts/step_summary',
            'ecard_summary',
            array('summary' => $postData)
        );
        $response['status'] = 'SUCCESS';
        $response['summary'] = $block->toHtml();
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function addtocartAction() {
        try {
            $postData = $this->getRequest()->getPost();
            if ($postData['recipients_method']) {
                $fileData = Mage::helper('base')->getFileRecipients();
                $data = array_filter($fileData['result']);
                if (empty($data)) {
                    Mage::getModel('core/session')->addError($this->__('No valid recipient data in uploaded file'));
                    $this->_redirect('checkout/cart/index');
                    return;
                }
                $postData['ecard_to'] = $fileData['result'];
            }
            $qty = count($postData['ecard_to']);
            $giftIds = array_keys($postData['gifts_ids']);

            foreach ($postData['ecard_to'] as $_recipient) {
                $ecardSending = Mage::getModel('base/send');
                $ecardSending->setCustomerId(Mage::getModel('customer/session')->getCustomer()->getId())
                    ->setEcardId($postData['ecard_id'])
                    ->setToName($_recipient['name'])
                    ->setToEmail($_recipient['email'])
                    ->setMessage(json_encode($postData['ecard_text']))
                    ->setMessageStyle(json_encode($postData['ecard_style']))
                    ->setGiftIds(json_encode($giftIds))
                    ->setToken(
                        Mage::getUrl('egifts/view/index',
                            array(
                                '_store'    => Mage::app()->getStore()->getCode(),
                                'key'       => Mage::helper('base')->getToken())
                        )
                    )
                    ->setCreatedAt(date('Y-m-d'))
                    ->setSendAt(strtotime($_recipient['date']))
                    ->setRemindAt(strtotime($_recipient['date'] . '+ ' . Mage::helper('egifts')->getRemindDays() . 'days'))
                    ->setIsActive(0)
                    ->setStatus(0);
                if(isset($_recipient['phone'])) {
                    $ecardSending->setPhoneNumber($_recipient['phone']);
                }
                if(isset($postData['card_image'])){
                    $ecardSending->setImage(substr_replace($postData['card_image'], '', 0, strlen(Mage::getBaseUrl('media'))));
                }
                $ecardSending->save();
                Mage::getModel('core/session')->unsFileImagePath();

                $_ecard = Mage::getModel('catalog/product')->load($postData['ecard_id']);
                $quote = Mage::getSingleton('checkout/cart')->getQuote();
                $_item = $quote->addProduct($_ecard, 1);

                $additionalOptions = array();
                if ($additionalOption = $_ecard->getCustomOption('additional_options')) {
                    $additionalOptions = (array)unserialize($additionalOption->getValue());
                }

                $additionalOptions['recipient'] = array(
                    'label' => 'eCard Recipient',
                    'value' => $_recipient['name'] . ' \'' . $_recipient['email'] . '\'',
                );
                $additionalOptions['gift'] = array(
                    'label' => 'eCard Gifts',
                    'value' => implode(',', $giftIds),
                );
                $additionalOptions['send_number'] = array(
                    'label' => 'eCard Send Number',
                    'value' => $ecardSending->getId(),
                );

                $phoneNumber = $ecardSending->getPhoneNumber();
                if(!empty($phoneNumber)){
                    $additionalOptions['phone_number'] = array(
                        'label' => 'eCard Phone Number',
                        'value' => $phoneNumber,
                    );
                }

                $_item->addOption(new Varien_Object(
                    array(
                        'product' => $_item->getProduct(),
                        'code' => 'additional_options',
                        'value' => serialize($additionalOptions)
                    )
                ));

            }

            $useDefaultPrice = Mage::getStoreConfig('egifts/ebooks/use_default_price');

            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            foreach ($giftIds as $giftId) {
                $_ecard = Mage::getModel('catalog/product')->load($giftId);
                $_item = $quote->addProduct($_ecard, $qty);
//                $_item = Mage::getModel('sales/quote_item')->setProduct($_ecard);
//                $_item->setQuote($quote);
//                $_item->setQty(1);

                if ($useDefaultPrice == 1) {
                    $customEbookPrice = Mage::getStoreConfig('egifts/ebooks/retail_price');
                    $storeCode = Mage::app()->getStore()->getCode();
                    if ($storeCode == 'wholesale') {
                        $customEbookPrice = Mage::getStoreConfig('egifts/ebooks/wholesale_price');
                    }
                    $_item->setCustomPrice($customEbookPrice);
                    $_item->setOriginalCustomPrice($customEbookPrice);
                }

                $additionalOptions = array();
                if ($additionalOption = $_ecard->getCustomOption('additional_options')) {
                    $additionalOptions = (array)unserialize($additionalOption->getValue());
                }

                $additionalOptions['send_number'] = array(
                    'label' => 'eCard Number',
                    'value' => $postData['ecard_id'],
                );

                $_item->addOption(new Varien_Object(
                    array(
                        'product' => $_item->getProduct(),
                        'code' => 'additional_options',
                        'value' => serialize($additionalOptions)
                    )
                ));
//                $quote->addItem($_item);

            }
            $customerId = Mage::getSingleton('customer/session')->getId();
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $quote->assignCustomer($customer);

            $quote->collectTotals()->save();
            if(!empty($phoneNumber)) {
                Mage::dispatchEvent('checkout_cart_add_singlecard');
            }
        } catch (Exception $e) {
            Mage::getModel('core/session')->addError($e->getMessage());
        }

        $this->_redirect('checkout/cart/index');
    }

    public function uploadAction() {
        $data = Mage::helper('base')->getFileRecipients();
        $response['status'] = 'SUCCESS';
        $response['errors'] = $data['errors'];
        $response['qty']    = $data['valid'];
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8', true)->setBody(Mage::helper('core')->jsonEncode($response));
    }

}
