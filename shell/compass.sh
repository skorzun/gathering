#!/usr/bin/env bash
#/**
# * Llama Commerce Platform
# *
# * NOTICE OF LICENSE
# *
# * This source file is subject to the Llama Commerce Platform License
# * that is bundled with this package in the file LICENSE_LC.txt.
# * It is also available through the world-wide-web at this URL:
# * http://www.llamacommerce.com/license
# * If you did not receive a copy of the license and are unable to
# * obtain it through the world-wide-web, please send an email
# * to support@llamacommerce.com so we can send you a copy immediately.
# *
# * DISCLAIMER
# *
# * Do not edit or add to this file if you wish to upgrade Llama Commerce Platform
# * to newer versions in the future. If you wish to customize Llama Commerce
# * Platform for your needs please refer to http://www.llamacommerce.com
# * for more information.
# *
# * @category   CLS
# * @package    LlamaCommerceCore
# * @copyright  Copyright (c) 2014 Classy Llama Studios, LLC (http://www.classyllama.com)
# * @license    http://www.llamacommerce.com/license
# */

## Set bash mode to exit on error
set -e

## Output usage text if asked for
if [[ "$1" = "--help" ]]; then
    echo "usage: compass.sh [--clean]";
    exit;
fi

## Store cwd so we exit with no changes
_cwd="`pwd`"

## Retrieve our fully-qualified dir path
pushd "`dirname "$0"`" > /dev/null
script_dir="`pwd`"
popd > /dev/null

## Full path to design folder
source_dir="$(dirname "$script_dir")/skin/frontend"

# Execute the compass compiler on all themes that contain a config.rb file
# Unfortunately, need to check for a specifically-named directory to make sure we don't have an output error when there's nothing to compile
for dir in $(find $source_dir -mindepth 2 -maxdepth 2 -type d)
do
    if [[ -f "$dir"/config.rb ]] && [[ -d "$dir"/scss ]]; then
        cd "$dir"
        pretty_name=$(basename $(dirname $dir))/$(basename $dir)

        if [[ "$1" == "--clean" ]]; then
            echo " cleaning $pretty_name/.sass-cache/"
            rm -rf "$dir.sass-cache"
        fi

        echo " building $pretty_name"
        compass compile ./
    fi
done

## Reset cwd to stored path
cd "$_cwd"
