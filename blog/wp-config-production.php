<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thegathe_wp1');

/** MySQL database username */
define('DB_USER', 'thegathe_wp1');

/** MySQL database password */
define('DB_PASSWORD', 'F[HKpW3@7755(~5');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nIvkTm6eWtvrQVunCXZrE3TJ5T4Nn5fKmTTWmtssInMauwb0auSxcxovpZR9v19V');
define('SECURE_AUTH_KEY',  'yrUagWW8wGgSHtfhgafyUBzGDReoH21dfCKXf1dg3AIK858uHucHU1JdkT3d6H44');
define('LOGGED_IN_KEY',    'Rw3vRpK0nitQ5mD3Aw4ezXhkPSplV2C1tj35AgZIcZaliusvm9q7nDItUUii53Jf');
define('NONCE_KEY',        'RmTlNsM6VQWHd4ANtRSxPENGchePNIHHUO8YeG8ln49ZeEpaYp2s8hboxsxv5XEm');
define('AUTH_SALT',        'swVPsZLt8iJZhJgTcYd0ahBlpH9o17LaZrs5EdLcbiauTGho6TY3vrJ6bugrjuoH');
define('SECURE_AUTH_SALT', 'alfyX1jfiMcLl2wmdmx0fAwEYdpzGmp2IKL69lXSwiLsyoNBAjfKw2Of93YDfnt1');
define('LOGGED_IN_SALT',   'u0NQGWFEMv2jUZxj3gGi9W8IP7uQr1Kdq62zAGQmRQx2pnieCqJ5qvfS8LxpGBfc');
define('NONCE_SALT',       'b5GACARk8EEMI3XITh1pKHDmRgnLkTYYDpIvhzsX52zuuqV1SkrqUbZyOE3rTiCZ');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
